//
//  OOD_LR7Tests.swift
//  OOD_LR7Tests
//
//  Created by Marty on 13/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import XCTest
@testable import OOD_LR7

class OOD_LR7Tests: XCTestCase {

    func testSingleShapeInitFrameOfComposite() {
        let oval = Ellipse()
        oval.frame = CGRect(x: 100, y: 300, width: 200, height: 150)
        
        let comp = Composite(withComponent: Leaf(shape: oval))
        XCTAssertEqual(comp.frame, CGRect(x: 100, y: 300, width: 200, height: 150))
    }
    
    func testSingleShapeChangesFrameOfComposite() {
        let oval = Ellipse()
        oval.frame = CGRect(x: 100, y: 300, width: 200, height: 150)
        
        let comp = Composite(withComponent: Leaf(shape: oval))
        comp.frame = CGRect(x: 10, y: 20, width: 30, height: 100)
        XCTAssertEqual(comp.frame, CGRect(x: 10, y: 20, width: 30, height: 100))
    }

    func testSeconsShapeChangesCompositeFrame() {
        let oval = Ellipse()
        oval.frame = CGRect(x: 100, y: 300, width: 200, height: 150)
        
        let polygon = try! Polygon(points: [CGPoint(x: 100, y: 100), CGPoint(x: 200, y: 100), CGPoint(x: 100, y: 200)])
        polygon.frame = CGRect(x: 120, y: 130, width: 20, height: 500)
        
        
        let comp = Composite(withComponent: Leaf(shape: oval))
        comp.add(component: Leaf(shape: polygon))
        
        XCTAssertEqual(comp.frame, CGRect(x: 100, y: 130, width: 200, height: 500))
    }
    
    func testChangingCompositeFrameChangesShapes() {
        let oval = Ellipse()
        oval.frame = CGRect(x: 100, y: 100, width: 100, height: 100)
        
        let polygon = try! Polygon(points: [CGPoint(x: 100, y: 100), CGPoint(x: 200, y: 100), CGPoint(x: 100, y: 200)])
        polygon.frame = CGRect(x: 200, y: 200, width: 100, height: 100)
        
        let leafOval = Leaf(shape: oval)
        let leafPolygon = Leaf(shape: polygon)
        
        
        let comp = Composite(withComponent: leafOval)
        comp.add(component: leafPolygon)
        
        comp.frame = CGRect(x: 150, y: 150, width: 100, height: 100)
        XCTAssertEqual(leafOval.frame, CGRect(x: 150, y: 150, width: 50, height: 50))
        XCTAssertEqual(leafPolygon.frame, CGRect(x: 200, y: 200, width: 50, height: 50))
    }
    
    func testCompositeFrameChangesAfterRemovingShape() {
        let oval = Ellipse()
        oval.frame = CGRect(x: 100, y: 100, width: 100, height: 100)
        
        let polygon = try! Polygon(points: [CGPoint(x: 100, y: 100), CGPoint(x: 200, y: 100), CGPoint(x: 100, y: 200)])
        polygon.frame = CGRect(x: 200, y: 200, width: 100, height: 100)
        
        let leafOval = Leaf(shape: oval)
        let leafPolygon = Leaf(shape: polygon)
        
        
        let comp = Composite(withComponent: leafOval)
        comp.add(component: leafPolygon)
        XCTAssertEqual(comp.frame, CGRect(x: 100, y: 100, width: 200, height: 200))
        try! comp.removeComponent(atIndex: 0)
        XCTAssertEqual(comp.frame, CGRect(x: 200, y: 200, width: 100, height: 100))
        
        comp.add(component: leafOval)
        XCTAssertEqual(comp.frame, CGRect(x: 100, y: 100, width: 200, height: 200))
        try! comp.removeComponent(atIndex: 0)
        XCTAssertEqual(comp.frame, CGRect(x: 100, y: 100, width: 100, height: 100))
    }
    
    func testCantRemoveLastElementFromGroup() {
        let oval = Ellipse()
        oval.frame = CGRect(x: 100, y: 100, width: 100, height: 100)
        
        let polygon = try! Polygon(points: [CGPoint(x: 100, y: 100), CGPoint(x: 200, y: 100), CGPoint(x: 100, y: 200)])
        polygon.frame = CGRect(x: 200, y: 200, width: 100, height: 100)
        
        let leafOval = Leaf(shape: oval)
        let leafPolygon = Leaf(shape: polygon)
        
        
        let comp = Composite(withComponent: leafOval)
        comp.add(component: leafPolygon)
        
        XCTAssertNoThrow(try comp.removeComponent(atIndex: 0))
        XCTAssertThrowsError(try comp.removeComponent(atIndex: 0))
    }
    
    func testStyleChangesWithInitGroupByFirstShape() {
        let oval = Ellipse()
        oval.fillStyle.color = UIColor.green.cgColor
        oval.lineStyle.color = UIColor.red.cgColor
        oval.lineStyle.lineWidth = 27
        
        let comp = Composite(withComponent: Leaf(shape: oval))
        
        XCTAssertEqual(comp.fillStyle.color, UIColor.green.cgColor)
        XCTAssertEqual(comp.lineStyle.color, UIColor.red.cgColor)
        XCTAssertEqual(comp.lineStyle.lineWidth, 27)
    }
    
    func testStyleDoesntChangeWithAddingTheSameStyledShape() {
        let oval = Ellipse()
        oval.fillStyle.color = UIColor.green.cgColor
        oval.lineStyle.color = UIColor.red.cgColor
        oval.lineStyle.lineWidth = 27
        
        let comp = Composite(withComponent: Leaf(shape: oval))
        
        let polygon = try! Polygon(points: [CGPoint(x: 100, y: 100), CGPoint(x: 200, y: 100), CGPoint(x: 100, y: 200)])
        polygon.fillStyle.color = UIColor.green.cgColor
        polygon.lineStyle.color = UIColor.red.cgColor
        polygon.lineStyle.lineWidth = 27
        
        comp.add(component: Leaf(shape: polygon))
        
        XCTAssertEqual(comp.fillStyle.color, UIColor.green.cgColor)
        XCTAssertEqual(comp.lineStyle.color, UIColor.red.cgColor)
        XCTAssertEqual(comp.lineStyle.lineWidth, 27)
    }
    
    func testStyleChangesWithAddingDifferentShape() {
        let oval = Ellipse()
        oval.fillStyle.color = UIColor.green.cgColor
        oval.lineStyle.color = UIColor.red.cgColor
        oval.lineStyle.lineWidth = 27
        
        let comp = Composite(withComponent: Leaf(shape: oval))
        
        let polygon = try! Polygon(points: [CGPoint(x: 100, y: 100), CGPoint(x: 200, y: 100), CGPoint(x: 100, y: 200)])
        polygon.fillStyle.color = UIColor.yellow.cgColor
        polygon.lineStyle.color = UIColor.blue.cgColor
        polygon.lineStyle.lineWidth = 29
        
        comp.add(component: Leaf(shape: polygon))
        
        XCTAssertEqual(comp.fillStyle.color, nil)
        XCTAssertEqual(comp.lineStyle.color, nil)
        XCTAssertEqual(comp.lineStyle.lineWidth, nil)
    }
    
    func testStyleDefinesWhenAllTheGroupComponentsStylesAreTheSame() {
        let oval = Ellipse()
        oval.fillStyle.color = UIColor.green.cgColor
        oval.lineStyle.color = UIColor.red.cgColor
        oval.lineStyle.lineWidth = 27
        
        let comp = Composite(withComponent: Leaf(shape: oval))
        
        let polygon = try! Polygon(points: [CGPoint(x: 100, y: 100), CGPoint(x: 200, y: 100), CGPoint(x: 100, y: 200)])
        polygon.fillStyle.color = UIColor.yellow.cgColor
        polygon.lineStyle.color = UIColor.blue.cgColor
        polygon.lineStyle.lineWidth = 29
        
        let polygonLeaf = Leaf(shape: polygon)
        comp.add(component: polygonLeaf)
        
        polygonLeaf.fillStyle.color = UIColor.green.cgColor
        polygonLeaf.lineStyle.color = UIColor.red.cgColor
        polygonLeaf.lineStyle.lineWidth = 27
        
        XCTAssertEqual(comp.fillStyle.color, UIColor.green.cgColor)
        XCTAssertEqual(comp.lineStyle.color, UIColor.red.cgColor)
        XCTAssertEqual(comp.lineStyle.lineWidth, 27)
    }
    
    func testStyleIsSettedToNilForGroupWhenStyleOfOneComponentIsChanging() {
        let oval = Ellipse()
        oval.fillStyle.color = UIColor.green.cgColor
        oval.lineStyle.color = UIColor.red.cgColor
        oval.lineStyle.lineWidth = 27
        
        let comp = Composite(withComponent: Leaf(shape: oval))
        
        let polygon = try! Polygon(points: [CGPoint(x: 100, y: 100), CGPoint(x: 200, y: 100), CGPoint(x: 100, y: 200)])
        polygon.fillStyle.color = UIColor.yellow.cgColor
        polygon.lineStyle.color = UIColor.blue.cgColor
        polygon.lineStyle.lineWidth = 29
        
        let polygonLeaf = Leaf(shape: polygon)
        comp.add(component: polygonLeaf)
        
        polygonLeaf.fillStyle.color = UIColor.black.cgColor
        polygonLeaf.lineStyle.lineWidth = 1
        polygonLeaf.lineStyle.color = UIColor.clear.cgColor
        
        XCTAssertEqual(comp.fillStyle.color, nil)
        XCTAssertEqual(comp.lineStyle.color, nil)
        XCTAssertEqual(comp.lineStyle.lineWidth, nil)
    }
}
