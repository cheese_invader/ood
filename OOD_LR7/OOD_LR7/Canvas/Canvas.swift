//
//  Canvas.swift
//  OOD_LR7
//
//  Created by Marty on 13/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import CoreGraphics
import UIKit

class Canvas: UIView, CanvasProtocol {
    var lineWidth: CGFloat = 3
    
    var color: CGColor?
    
    var fillColor: CGColor?
    
    func setLineWidth(_ lineWidth: CGFloat) {
        self.lineWidth = lineWidth
    }
    
    func setColor(_ color: CGColor?) {
        self.color = color
    }
    
    func setFillColor(_ color: CGColor?) {
        self.fillColor = color
    }
    
    func drawLineFrom(_ from: CGPoint, to: CGPoint) {
        let line = CAShapeLayer()
        let linePath = UIBezierPath()
        
        linePath.move(to: from)
        linePath.addLine(to: to)
        line.path = linePath.cgPath
        
        line.strokeColor = color
        line.lineWidth = lineWidth
        
        self.layer.addSublayer(line)
    }
    
    func drawEllipse(x: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat) {
        drawEllipse(inRect: CGRect(x: x, y: y, width: width, height: height))
    }
    
    func drawEllipse(inRect rect: CGRect) {
        let ovalPath = UIBezierPath(ovalIn: rect)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = ovalPath.cgPath
        
        shapeLayer.fillColor = fillColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = lineWidth
        
        self.layer.addSublayer(shapeLayer)
    }
    
    func drawPolygonByPoints(_ points: [CGPoint]) {
        guard points.count > 2 else {
            return
        }
        let shapeLayer = CAShapeLayer()

        shapeLayer.fillColor = fillColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = lineWidth
        
        let path = UIBezierPath()
        path.move(to: points[0])
        
        for pointIndex in 1..<points.count {
            path.addLine(to: points[pointIndex])
        }
        path.addLine(to: points[0])
        path.close()
        
        shapeLayer.path = path.cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
}
