//
//  CanvasProtocol.swift
//  OOD_LR7
//
//  Created by Marty on 13/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import CoreGraphics

protocol CanvasProtocol {
    var lineWidth: CGFloat { get }
    var color    : CGColor? { get }
    var fillColor: CGColor? { get }
    
    func setLineWidth(_ lineWidth: CGFloat)
    func setColor(_ color: CGColor?)
    func setFillColor(_ color: CGColor?)
    
    func drawLineFrom(_ from: CGPoint, to: CGPoint)
    func drawEllipse(x: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat)
    func drawEllipse(inRect rect: CGRect)
    func drawPolygonByPoints(_ points: [CGPoint])
}
