//
//  LineStyle.swift
//  OOD_LR7
//
//  Created by Marty on 13/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import CoreGraphics
import UIKit

private let defaultColor = UIColor.black.cgColor
private let defaultLineWidth: CGFloat = 3

struct LineStyle {
    var lineWidth: CGFloat? = defaultLineWidth
    var color: CGColor?
    
    var isHidden: Bool {
        get {
            return color == nil
        }
        set {
            guard !newValue && color != nil else {
                return
            }
            color = newValue ? nil : defaultColor
        }
    }
}
