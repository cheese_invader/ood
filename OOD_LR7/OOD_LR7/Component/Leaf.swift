//
//  Leaf.swift
//  OOD_LR7
//
//  Created by Marty on 14/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import CoreGraphics

class Leaf: ComponentProtocol {
    func setParent(_ parent: ComponentProtocol) {
        let selfFrame = frame
        
        self.parent = parent
        
        frame = selfFrame
    }
    
    func removeParent() {
        self.parent = nil
    }
        
    private var shape: ShapeProtocol
    private weak var parent: ComponentProtocol?
    
    var frame: CGRect {
        get {
            guard let parent = parent else {
                return shape.frame
            }
            
            let parentFrame = parent.frame
            return CGRect(x: parentFrame.minX + parentFrame.width  * shape.frame.minX,
                          y: parentFrame.minY + parentFrame.height * shape.frame.minY,
                          width : parentFrame.width  * shape.frame.width,
                          height: parentFrame.height * shape.frame.height)
        }
        set {
            guard let parent = parent else {
                shape.frame = newValue
                return
            }
            
            let parentFrame = parent.frame
            shape.frame = CGRect(x: (newValue.minX - parent.frame.minX) / parent.frame.width,
                                 y: (newValue.minY - parent.frame.minY) / parent.frame.height,
                                 width : newValue.width  / parentFrame.width,
                                 height: newValue.height / parentFrame.height)
        }
    }
    
    var lineStyle: LineStyle {
        didSet {
            parent?.calculateStyle()
        }
    }
    
    var fillStyle: FillStyle {
        didSet {
            parent?.calculateStyle()
        }
    }
    
    init(shape: ShapeProtocol) {
        self.shape     = shape
        self.lineStyle = shape.lineStyle
        self.fillStyle = shape.fillStyle
    }
    
    func calculateStyle() {
        parent?.calculateStyle()
    }
    
    func calculateFrame() {}
    
    func add(component: ComponentProtocol) {}
    
    func removeComponent(atIndex index: Int) throws {}
    
    func drawInCanvas(_ canvas: CanvasProtocol) {
        shape.frame = frame
        shape.lineStyle = lineStyle
        shape.fillStyle = fillStyle
        shape.drawInCanvas(canvas)
    }
    
    private func getCoveringFrame(_ f1: CGRect, _ f2: CGRect) -> CGRect {
        let minX = min(f1.minX, f2.minX)
        let minY = min(f1.minY, f2.minY)
        let maxX = max(f1.maxX, f2.maxX)
        let maxY = max(f1.maxY, f2.maxY)
        
        return CGRect(x: minX, y: minY, width: maxX - minX, height: maxY - minY)
    }
}
