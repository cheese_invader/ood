//
//  ComponentProtocol.swift
//  OOD_LR7
//
//  Created by Marty on 14/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol ComponentProtocol: class, ShapeProtocol {    
    func setParent(_ parent: ComponentProtocol)
    func removeParent()
    func calculateStyle()
    func calculateFrame()
    func add(component: ComponentProtocol)
    func removeComponent(atIndex index: Int) throws
}
