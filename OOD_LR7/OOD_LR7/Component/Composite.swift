//
//  Composite.swift
//  OOD_LR7
//
//  Created by Marty on 14/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import CoreGraphics

class Composite: ComponentProtocol {
    private var descendants: [ComponentProtocol]
    private weak var parent: ComponentProtocol?
    
    
    private var fframe: CGRect
    private var flineStyle: LineStyle
    private var ffillStyle: FillStyle
    
    var lineStyle: LineStyle {
        get {
            return flineStyle
        }
        set {
            flineStyle = newValue
            parent?.calculateStyle()
            
            for i in 0..<descendants.count {
                var newLineStyle = descendants[i].lineStyle
                var changed = false
                
                if !newValue.isHidden && newValue.color != descendants[i].lineStyle.color {
                    newLineStyle.color = newValue.color
                    changed = true
                }
                if !newValue.isHidden && newValue.lineWidth != descendants[i].lineStyle.lineWidth {
                    newLineStyle.lineWidth = newValue.lineWidth
                    changed = true
                }
                
                if changed {
                    descendants[i].lineStyle = newLineStyle
                }
            }
        }
    }
    
    var fillStyle: FillStyle {
        get {
            return ffillStyle
        }
        set {
            ffillStyle = newValue
            parent?.calculateStyle()
            
            for i in 0..<descendants.count {
                var newFillStyle = descendants[i].fillStyle
                var changed = false
                
                if !newValue.isHidden && newValue.color != descendants[i].fillStyle.color {
                    newFillStyle.color = newValue.color
                    changed = true
                }

                if changed {
                    descendants[i].fillStyle = newFillStyle
                }
            }
        }
    }
    
    var frame: CGRect {
        get {
            guard let parent = parent else {
                return fframe
            }
            
            let parentFrame = parent.frame
            return CGRect(x: parentFrame.minX + parentFrame.width  * fframe.minX,
                          y: parentFrame.minY + parentFrame.height * fframe.minY,
                          width : parentFrame.width  * fframe.width,
                          height: parentFrame.height * fframe.height)
        }
        set {
            guard let parent = parent else {
                fframe = newValue
                return
            }
            
            let parentFrame = parent.frame
            fframe = CGRect(x: (newValue.minX - parentFrame.minX) / parentFrame.width,
                            y: (newValue.minY - parentFrame.minY) / parentFrame.height,
                            width : newValue.width / parentFrame.width,
                            height: newValue.height / parentFrame.height)
        }
    }
    
    init(withComponent component: ComponentProtocol) {
        self.descendants = [component]
        self.fframe      = component.frame
        self.flineStyle   = component.lineStyle
        self.ffillStyle   = component.fillStyle
        
        calculateStyle()
        
        component.setParent(self)
    }
    
    func setParent(_ parent: ComponentProtocol) {
        let selfFrame = frame
        
        self.parent = parent
        
        frame = selfFrame
    }
    
    func removeParent() {
        self.parent = nil
    }
    
    func calculateStyle() {
        var color    : CGColor? = descendants[0].lineStyle.color
        var fillColor: CGColor? = descendants[0].fillStyle.color
        var lineWidth: CGFloat? = descendants[0].lineStyle.lineWidth
        
        for descendant in descendants {
            if descendant.lineStyle.color != color {
                color = nil
            }
            if descendant.lineStyle.lineWidth != lineWidth {
                lineWidth = nil
            }
            if descendant.fillStyle.color != fillColor {
                fillColor = nil
            }
        }
        
        var isStyleChanged = false
        
        if self.lineStyle.lineWidth != lineWidth || self.fillStyle.color != fillColor {
            self.lineStyle = LineStyle(lineWidth: lineWidth, color: color)
            isStyleChanged = true
        }
        if self.fillStyle.color != fillColor {
            self.fillStyle.color = fillColor
            isStyleChanged = true
        }
        
        if isStyleChanged {
            parent?.calculateStyle()
        }
    }
    
    func calculateFrame() {
        var coveringFrame = descendants[0].frame

        for descendant in descendants {
            coveringFrame = getCoveringFrame(coveringFrame, descendant.frame)
        }

        if coveringFrame != frame {
            changeFrameForGroup(frame: coveringFrame)
            parent?.calculateFrame()
        }
    }
    
    func drawInCanvas(_ canvas: CanvasProtocol) {
        for descendant in descendants {
            descendant.drawInCanvas(canvas)
        }
    }
    
    func add(component: ComponentProtocol) {
        var comp = component
        
        changeFrameForGroup(frame: getCoveringFrame(frame, comp.frame))
        
        comp.setParent(self)
        descendants.append(comp)
        
        calculateStyle()
        calculateFrame()
    }
    
    private func changeFrameForGroup(frame: CGRect) {
        var rects = [CGRect]()
        for descendant in descendants {
            rects.append(descendant.frame)
        }
        self.frame = frame
        
        for i in 0..<descendants.count {
            descendants[i].frame = rects[i]
        }
    }
    
    func removeComponent(atIndex index: Int) throws {
        if descendants.count == 1 {
            throw ComponentError.componentHasOnlyOnedescendant
        }
        let removedFrame = descendants[index].frame
        descendants[index].removeParent()
        descendants[index].frame = removedFrame
        
        descendants.remove(at: index)
        
        calculateStyle()
        calculateFrame()
    }
    
    private func getCoveringFrame(_ f1: CGRect, _ f2: CGRect) -> CGRect {
        let minX = min(f1.minX, f2.minX)
        let minY = min(f1.minY, f2.minY)
        let maxX = max(f1.maxX, f2.maxX)
        let maxY = max(f1.maxY, f2.maxY)

        return CGRect(x: minX, y: minY, width: maxX - minX, height: maxY - minY)
    }
}
