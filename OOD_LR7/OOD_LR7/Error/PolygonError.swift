//
//  PolygonError.swift
//  OOD_LR7
//
//  Created by Marty on 13/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

enum PolygonError: Error {
    case incorrectArgumentsNumber
    case zeroWidth
    case zeroHeight
}
