//
//  ComponentError.swift
//  OOD_LR7
//
//  Created by Marty on 14/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

enum ComponentError: Error {
    case componentHasOnlyOnedescendant
}
