//
//  ViewController.swift
//  OOD_LR7
//
//  Created by Marty on 13/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    @IBOutlet var canvas: Canvas!
    
    func house() -> Composite {
        let house = try! Polygon(points: [CGPoint(x: 80, y: 260),
                                           CGPoint(x: 80, y: 130),
                                           CGPoint(x: 50, y: 130),
                                           CGPoint(x: 150, y: 80),
                                           CGPoint(x: 250, y: 130),
                                           CGPoint(x: 220, y: 130),
                                           CGPoint(x: 220, y: 260)])
        house.lineStyle.color = #colorLiteral(red: 0.8980392157, green: 0.470871871, blue: 0.145259691, alpha: 1)
        house.fillStyle.color = #colorLiteral(red: 0.8980392157, green: 0.07077920434, blue: 0.2087582859, alpha: 1)
        
        let doar = Rectangle()
        doar.fillStyle.color = #colorLiteral(red: 0.8980392157, green: 0.470871871, blue: 0.145259691, alpha: 1)
        doar.frame = CGRect(x: 130, y: 200, width: 40, height: 60)
        
        doar.lineStyle.color = UIColor.green.cgColor
        
        let composite = Composite(withComponent: Leaf(shape: house))
        
        composite.add(component: Leaf(shape: doar))
        
        return composite
    }
    
    
    func background() -> Composite {
        let ground = try! Polygon(points: [CGPoint(x: 0, y: 200),
                                           CGPoint(x: 1000, y: 200),
                                           CGPoint(x: 1000, y: 500),
                                           CGPoint(x: 0, y: 500)])
        ground.fillStyle.color = #colorLiteral(red: 1, green: 0.6977000025, blue: 0.1579711292, alpha: 1)
        
        
        let sky = try! Polygon(points: [CGPoint(x: 0, y: 0),
                                        CGPoint(x: 1000, y: 0),
                                        CGPoint(x: 1000, y: 200),
                                        CGPoint(x: 0, y: 200)])
        sky.fillStyle.color = #colorLiteral(red: 0.2282028665, green: 0.5420949771, blue: 1, alpha: 1)
        
        
        let sun = Ellipse()
        sun.frame = CGRect(x: 50, y: 50, width: 75, height: 75)
        sun.fillStyle.color = #colorLiteral(red: 0.9156283308, green: 1, blue: 0.1566543767, alpha: 1)
        sun.lineStyle.color = #colorLiteral(red: 1, green: 0.4678747823, blue: 0, alpha: 1)
        
        let lake = Ellipse()
        lake.frame = CGRect(x: 500, y: 250, width: 200, height: 100)
        lake.fillStyle.color = #colorLiteral(red: 0, green: 0.07706925133, blue: 1, alpha: 1)
        lake.lineStyle.color = #colorLiteral(red: 0.9578840462, green: 0.9911143893, blue: 1, alpha: 1)
        lake.lineStyle.lineWidth = 5
        
        let leafGround = Leaf(shape: ground)
        let leafSky = Leaf(shape: sky)
        let leafSun = Leaf(shape: sun)
        let leafLake = Leaf(shape: lake)
        
        let composite = Composite(withComponent: leafGround)
        composite.add(component: leafSky)
        composite.add(component: leafSun)
        composite.add(component: leafLake)
        
        
        return composite
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let bg = background()
        let composite = Composite(withComponent: bg)
        
//        composite.frame = CGRect(x: 100, y: 100, width: 200, height: 100)
//        bg.fillStyle.color = UIColor.black.cgColor
        let house1 = house()
        composite.add(component: house1)
        
        house1.frame = CGRect(x: 130, y: 200, width: 400, height: 60)
//        house1.lineStyle.color = UIColor.black.cgColor
        
        composite.drawInCanvas(canvas)
    }
}

