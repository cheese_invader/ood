//
//  BaseShape.swift
//  OOD_LR7
//
//  Created by Marty on 13/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import CoreGraphics

class BaseShape: BaseShapeProtocol {
    var frame: CGRect = CGRect(x: 0, y: 0, width: 100, height: 100)
    
    var lineStyle = LineStyle()
    
    var fillStyle = FillStyle()
    
    func prepeareStyleForCanvas(_ canvas: CanvasProtocol) {
        canvas.setColor(lineStyle.color)
        canvas.setFillColor(fillStyle.color)
        if let lineWidth = lineStyle.lineWidth {
            canvas.setLineWidth(lineWidth)
        }
    }
}
