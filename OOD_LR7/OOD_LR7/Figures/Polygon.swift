//
//  Polygon.swift
//  OOD_LR7
//
//  Created by Marty on 13/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import CoreGraphics

class Polygon: BaseShape, ShapeProtocol {
    let points: [CGPoint]
    
    init(points: [CGPoint]) throws {
        guard points.count > 2 else {
            throw PolygonError.incorrectArgumentsNumber
        }
        
        var x = points[0].x
        var y = points[0].y
        var width: CGFloat = 0
        var height: CGFloat = 0
        
        for point in points {
            if point.x < x {
                x = point.x
            }
            if point.x > x + width {
                width = point.x - x
            }
            if point.y < y {
                y = point.y
            }
            if point.y > y + height {
                height = point.y - y
            }
        }
        
        if abs(width) < 0.05 {
            throw PolygonError.zeroWidth
        }
        if abs(height) < 0.05 {
            throw PolygonError.zeroHeight
        }
        
        var proportionPoints = [CGPoint]()
        for point in points {
            proportionPoints.append(CGPoint(x: (point.x - x) / width, y: (point.y - y) / height))
        }
        self.points = proportionPoints
        
        super.init()
        self.frame = CGRect(x: x, y: y, width: width, height: height)
    }
    
    func drawInCanvas(_ canvas: CanvasProtocol) {
        prepeareStyleForCanvas(canvas)
        
        var finalPoints = [CGPoint]()
        
        for point in points {
            finalPoints.append(CGPoint(x: point.x * frame.width + frame.minX, y: point.y * frame.height + frame.minY))
        }
        
        canvas.drawPolygonByPoints(finalPoints)
    }
}
