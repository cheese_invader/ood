//
//  ShapeProtocol.swift
//  OOD_LR7
//
//  Created by Marty on 13/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import CoreGraphics

protocol BaseShapeProtocol {
    var frame: CGRect { get set }
    var lineStyle: LineStyle { get set }
    var fillStyle: FillStyle { get set }
}

protocol ShapeProtocol: BaseShapeProtocol {
    func drawInCanvas(_ canvas: CanvasProtocol)
}
