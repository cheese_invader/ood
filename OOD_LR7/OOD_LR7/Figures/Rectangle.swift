//
//  Rectangle.swift
//  OOD_LR7
//
//  Created by Marty on 13/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import CoreGraphics

class Rectangle: BaseShape, ShapeProtocol {    
    func drawInCanvas(_ canvas: CanvasProtocol) {
        prepeareStyleForCanvas(canvas)
        canvas.drawPolygonByPoints([CGPoint(x: frame.minX, y: frame.minY), CGPoint(x: frame.minX, y: frame.maxY), CGPoint(x: frame.maxX, y: frame.maxY), CGPoint(x: frame.maxX, y: frame.minY)])
    }
}
