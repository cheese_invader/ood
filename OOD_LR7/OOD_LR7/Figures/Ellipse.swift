//
//  Ellipse.swift
//  OOD_LR7
//
//  Created by Marty on 13/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class Ellipse: BaseShape, ShapeProtocol {    
    func drawInCanvas(_ canvas: CanvasProtocol) {
        prepeareStyleForCanvas(canvas)
        canvas.drawEllipse(inRect: frame)
    }
}
