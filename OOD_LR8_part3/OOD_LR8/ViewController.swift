//
//  ViewController.swift
//  OOD_LR8
//
//  Created by Marty on 22/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    private let menu = Menu()
    private var machine: GumballMachine!
    private let logger = Logger()
    
    @IBOutlet weak var commandTextField: UITextField!
    @IBOutlet weak var mainTextView: UITextView!
    
    @IBAction func helpButtonWasTapped(_ sender: UIButton) {
        mainTextView.text = menu.help
    }
    
    @IBAction func infoButtonWasTapped(_ sender: UIButton) {
        mainTextView.text = machine.toString()
    }
    
    @IBAction func mainTextFieldPrimaryActionTriggered(_ sender: UITextField) {
        menu.excecuteCommand(commandTextField.text ?? "")
        commandTextField.text = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initCommands()
        machine = GumballMachine(ballsCount: 10, logger: logger)
    }

    private func getConvenienceLog() -> String {
        var result = ""
        
        for log in logger.getLogs() {
            result.append("* " + log + "\n")
        }
        
        return result
    }
    
    
    private func initCommands() {
        menu.addShortcut("insertQuarter", withDescription: "You need to insert quarter to get your gumball") { [weak self] (_) in
            guard let self = self else {
                return
            }
            self.logger.clear()
            self.machine.insertQuarter()
            self.mainTextView.text = self.getConvenienceLog()
        }
        
        menu.addShortcut("ejectQuarter", withDescription: "You get back your quarter") { [weak self] (_) in
            guard let self = self else {
                return
            }
            self.logger.clear()
            self.machine.ejectQuarter()
            self.mainTextView.text = self.getConvenienceLog()
        }
        
        menu.addShortcut("turnCrank", withDescription: "After paying turn crank and you will get your quarter") { [weak self] (_) in
            guard let self = self else {
                return
            }
            self.logger.clear()
            self.machine.turnCrank()
            self.mainTextView.text = self.getConvenienceLog()
        }
        
        menu.addShortcut("fillWithGumball", withDescription: "Administrator can add gumballs to machine. You should print count of gumballs you want to add") { [weak self] (count) in
            guard let self = self, let countIncValue = Int(count) else {
                return
            }
            self.logger.clear()
            self.machine.fillWithGumballs(countIncValue)
            self.mainTextView.text = self.getConvenienceLog()
        }
    }
}

