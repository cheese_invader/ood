//
//  Logger.swift
//  OOD_LR8
//
//  Created by Marty on 22/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class Logger: LoggerProtocol {
    private var logs = [String]()
    
    func addLog(_ log: String) {
        logs.append(log)
    }
    
    func getLogs() -> [String] {
        return logs
    }
    
    var logsCount: Int {
        get {
            return logs.count
        }
    }
    
    func getLogAtIndex(_ index: Int) -> String {
        return logs[index]
    }
    
    func getLastLog() -> String? {
        return logs.last
    }
    
    func clear() {
        logs = []
    }
}
