//
//  LoggerProtocol.swift
//  OOD_LR8
//
//  Created by Marty on 22/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol LoggerProtocol {
    func addLog(_ log: String)
    func getLogs() -> [String]
    var logsCount: Int { get }
    func getLogAtIndex(_ index: Int) -> String
    func getLastLog() -> String?
    func clear()
}
