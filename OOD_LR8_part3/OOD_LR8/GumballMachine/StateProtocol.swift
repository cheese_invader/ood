//
//  StateProtocol.swift
//  OOD_LR8
//
//  Created by Marty on 22/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

public protocol StateProtocol {
    func insertQuarter()
    func ejectQuarters()
    func fillWithGumballs(_ count: Int)
    func turnCrank()
    func dispense()
    func toString() -> String
}

