//
//  HasQuarterState.swift
//  OOD_LR8
//
//  Created by Marty on 18/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class HasQuarterState: StateProtocol {
    init(gumballMachine: StateContextProtocol, logger: LoggerProtocol?) {
        self.gumballMachine = gumballMachine
        self.logger = logger
    }
    
    func insertQuarter() {
        guard let gumballMachine = gumballMachine else {
            return
        }
        
        var response = "You can't insert another quarter\n"
        if gumballMachine.canInsertOneMoreCoin {
            response = "You inserted a quarter\n"
            gumballMachine.addCoin()
        }
        
        logger?.addLog(response)
        print(response)
    }
    
    func ejectQuarters() {
        guard let gumballMachine = gumballMachine else {
            return
        }
        gumballMachine.setNoQuarterState()
        let response = "Get your \(gumballMachine.quartersCount) quarters\n"
        gumballMachine.releaseCoins()
        logger?.addLog(response)
        print(response)
    }
    
    func fillWithGumballs(_ count: Int) {
        guard let gumballMachine = gumballMachine else {
            return
        }
        guard count > 0 else {
            return
        }
        gumballMachine.fillWithGumballs(count)
        let response = "\(count) gumball\(count != 1 ? "s" : "") \(count == 1 ? "was" : "were") added\n"
        logger?.addLog(response)
        print(response)
    }
    
    func turnCrank() {
        guard let gumballMachine = gumballMachine else {
            return
        }
        gumballMachine.useCoin()
        gumballMachine.setSoldState()
        let response = "You turned...\n"
        logger?.addLog(response)
        print(response)
    }
    
    func dispense() {
        let response = "No gumball dispensed\n"
        logger?.addLog(response)
        print(response)
    }
    
    func toString() -> String {
        return "Waiting for turn of crank\n"
    }
    
    
    private weak var gumballMachine: StateContextProtocol?
    private var logger: LoggerProtocol?
}
