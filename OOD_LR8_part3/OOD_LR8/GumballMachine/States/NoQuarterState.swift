//
//  NoQuarterState.swift
//  OOD_LR8
//
//  Created by Marty on 18/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class NoQuarterState: StateProtocol {
    init(gumballMachine: StateContextProtocol, logger: LoggerProtocol?) {
        self.gumballMachine = gumballMachine
        self.logger = logger
    }
    
    func insertQuarter() {
        guard let gumballMachine = gumballMachine else {
            return
        }
        gumballMachine.setHasQuarterState()
        let response = "You inserted a quarter\n"
        gumballMachine.addCoin()
        logger?.addLog(response)
        print(response)
    }
    
    func ejectQuarters() {
        let response = "You haven't inserted a quarter\n"
        logger?.addLog(response)
        print(response)
    }
    
    func fillWithGumballs(_ count: Int) {
        guard let gumballMachine = gumballMachine else {
            return
        }
        guard count > 0 else {
            return
        }
        gumballMachine.fillWithGumballs(count)
        let response = "\(count) gumball\(count != 1 ? "s" : "") \(count == 1 ? "was" : "were") added\n"
        logger?.addLog(response)
        print(response)
    }
    
    func turnCrank() {
        let response = "You turned but there's no quarter\n"
        logger?.addLog(response)
        print(response)
    }
    
    func dispense() {
        let response = "You need to pay first\n"
        logger?.addLog(response)
        print(response)
    }
    
    func toString() -> String {
        return "Waiting for quarter\n"
    }
    
    
    private weak var gumballMachine: StateContextProtocol?
    private var logger: LoggerProtocol?
}
