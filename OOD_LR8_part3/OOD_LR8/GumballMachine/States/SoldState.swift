//
//  SoldState.swift
//  OOD_LR8
//
//  Created by Marty on 18/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class SoldState: StateProtocol {
    init(gumballMachine: StateContextProtocol, logger: LoggerProtocol?) {
        self.gumballMachine = gumballMachine
        self.logger = logger
    }
    
    func insertQuarter() {
        guard let gumballMachine = gumballMachine else {
            return
        }
        var response = "You cant insert one more quarter\n"
        
        if gumballMachine.canInsertOneMoreCoin {
            response = "Quarter inserted\n"
            gumballMachine.addCoin()
        }
        logger?.addLog(response)
        print(response)
    }
    
    func ejectQuarters() {
        guard let gumballMachine = gumballMachine else {
            return
        }
        var response = "No quarters\n"
        if gumballMachine.quartersCount > 0 {
            response = "Get your \(gumballMachine.quartersCount) quarter\(gumballMachine.quartersCount != 1 ? "s" : "")\n"
            gumballMachine.releaseCoins()
        }
        logger?.addLog(response)
        print(response)
    }
    
    func fillWithGumballs(_ count: Int) {
        let response = "Please, wait. Customer is waiting for his gumball now\n"
        logger?.addLog(response)
        print(response)
    }
    
    func turnCrank() {
        let response = "Turning twice doesn't get you another gumball\n"
        logger?.addLog(response)
        print(response)
    }
    
    func dispense() {
        guard let gumballMachine = gumballMachine else {
            return
        }
        gumballMachine.releaseBall()
        if gumballMachine.ballsCount == 0 {
            gumballMachine.setSoldOutState()
        } else if gumballMachine.quartersCount == 0 {
            gumballMachine.setNoQuarterState()
        } else {
            gumballMachine.setHasQuarterState()
        }
        let response = "Get your gumball\n"
        logger?.addLog(response)
        print(response)
    }
    
    func toString() -> String {
        return "Delivering a gumball\n"
    }
    
    
    private weak var gumballMachine: StateContextProtocol?
    private var logger: LoggerProtocol?
}
