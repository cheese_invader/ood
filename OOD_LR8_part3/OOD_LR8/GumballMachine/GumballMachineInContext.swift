//
//  GumballMachineInContext.swift
//  OOD_LR8
//
//  Created by Marty on 22/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

private let maxNumberOfCoint = 5

class GumballMachineInContext: StateContextProtocol {
    init(ballsCount: Int, logger: LoggerProtocol?) {
        self.logger = logger
        self.ballsCount = ballsCount >= 0 ? ballsCount : 0
        if ballsCount > 0 {
            setNoQuarterState()
        } else {
            setSoldOutState()
        }
    }
    
    func fillWithGumballs(_ count: Int) {
        ballsCount += count
    }
    
    func releaseBall() {
        if ballsCount != 0 {
            ballsCount -= 1
        }
    }
    
    func releaseCoins() {
        quartersCount = 0
    }
    
    func addCoin() {
        quartersCount += 1
    }
    
    func useCoin() {
        quartersCount -= 1
    }
    
    func setSoldOutState() {
        state = SoldOutState(gumballMachine: self, logger: logger)
    }
    
    func setNoQuarterState() {
        state = NoQuarterState(gumballMachine: self, logger: logger)
    }
    
    func setSoldState() {
        state = SoldState(gumballMachine: self, logger: logger)
    }
    
    func setHasQuarterState() {
        state = HasQuarterState(gumballMachine: self, logger: logger)
    }
    
    var ballsCount = 0
    var quartersCount = 0
    var state: StateProtocol!
    var logger: LoggerProtocol?
    var canInsertOneMoreCoin: Bool {
        get {
            return quartersCount != maxNumberOfCoint
        }
    }
}
