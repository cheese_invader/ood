//
//  Menu.swift
//  OOD_LR8
//
//  Created by Marty on 06/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//


typealias CommandHandler = (String) -> ()


class Menu {
    private var     handlerByShortcut = [String: CommandHandler]()
    private var descriptionByShortcut = [String: String]()

    
    func addShortcut(_ shortcut: String, withDescription description: String, andHandler handler: @escaping CommandHandler) {
        handlerByShortcut    [shortcut] = handler
        descriptionByShortcut[shortcut] = description
    }

    func excecuteCommand(_ command: String) {
        let firstWhitespace = command.firstIndex(of: " ") ?? command.endIndex
        
        let shortcut = String(command[..<firstWhitespace])
        let tail = firstWhitespace < command.endIndex ? String(command[command.index(after: firstWhitespace)...]) : ""
        
        handlerByShortcut[shortcut]?(tail)
    }
    
    var help: String {
        var result = ""
        
        for command in handlerByShortcut.keys {
            result.append("* " + command + "\n\t" + descriptionByShortcut[command]! + "\n\n")
        }
        return result
    }
}
