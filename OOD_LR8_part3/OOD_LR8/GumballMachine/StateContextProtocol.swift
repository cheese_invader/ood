//
//  StateContextProtocol.swift
//  OOD_LR8
//
//  Created by Marty on 22/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

public protocol StateContextProtocol: class {
    var ballsCount: Int { get }
    var quartersCount: Int { get }
    var canInsertOneMoreCoin: Bool { get }
    var state: StateProtocol! { get }
    
    func fillWithGumballs(_ count: Int)
    func releaseBall()
    func releaseCoins()
    func addCoin()
    func useCoin()
    func setSoldOutState()
    func setNoQuarterState()
    func setSoldState()
    func setHasQuarterState()
}
