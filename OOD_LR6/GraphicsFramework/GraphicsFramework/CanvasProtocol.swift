//
//  CanvasProtocol.swift
//  GraphicsFramework
//
//  Created by Marty on 30/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

public protocol CanvasProtocol {
    func setLogger(_ logger: LoggerProtocol)
    func setColor(_ color: Int)
    func moveTo(x: Int, y: Int)
    func lineTo(x: Int, y: Int)
}

