//
//  Canvas.swift
//  GraphicsFramework
//
//  Created by Marty on 30/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

open class Canvas: CanvasProtocol {
    public private(set) var logger: LoggerProtocol?
    
    public init() {
    }
    
    public func setLogger(_ logger: LoggerProtocol) {
        self.logger = logger
    }
    
    public func setColor(_ color: Int) {
        logger?.log("SetColor (#\(color))")
        print("SetColor (#\(String(color, radix: 16))")
    }
    
    public func moveTo(x: Int, y: Int) {
        logger?.log("Move to: (\(x), \(y))")
        print("Move to: (\(x), \(y))")
    }
    
    public func lineTo(x: Int, y: Int) {
        logger?.log("Line to: (\(x), \(y))")
        print("Line to: (\(x), \(y))")
    }
}
