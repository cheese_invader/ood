//
//  LoggerProtocol.swift
//  GraphicsFramework
//
//  Created by Marty on 30/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

public protocol LoggerProtocol {
    var count: Int { get }
    func getLogByIndex(_ index: Int) -> String
    func log(_ message: String)
}
