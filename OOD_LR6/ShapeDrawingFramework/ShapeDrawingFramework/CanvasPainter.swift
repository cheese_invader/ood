//
//  CanvasPainter.swift
//  ShapeDrawingFramework
//
//  Created by Marty on 30/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import GraphicsFramework

public class CanvasPainter {
    private let canvas: CanvasProtocol
    
    public init(canvas: CanvasProtocol) {
        self.canvas = canvas
    }
    
    public func draw(_ drawable: CanvasDrawableProtocol) {
        drawable.draw(graphicsLib: canvas)
    }
}
