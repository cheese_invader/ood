//
//  Triangle.swift
//  ShapeDrawingFramework
//
//  Created by Marty on 30/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import GraphicsFramework

public class Triangle: CanvasDrawableProtocol {
    private var vertex1: Point
    private var vertex2: Point
    private var vertex3: Point
    
    public init(vertex1: Point, vertex2: Point, vertex3: Point) {
        self.vertex1 = vertex1
        self.vertex2 = vertex2
        self.vertex3 = vertex3
    }
    
    public func draw(graphicsLib: CanvasProtocol) {
        graphicsLib.lineTo(x: vertex1.x, y: vertex1.y)
        graphicsLib.moveTo(x: vertex2.x, y: vertex2.y)
        
        graphicsLib.lineTo(x: vertex2.x, y: vertex2.y)
        graphicsLib.moveTo(x: vertex3.x, y: vertex3.y)
        
        graphicsLib.lineTo(x: vertex3.x, y: vertex3.y)
        graphicsLib.moveTo(x: vertex1.x, y: vertex1.y)
    }
}
