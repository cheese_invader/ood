//
//  CanvasDrawableProtocol.swift
//  ShapeDrawingFramework
//
//  Created by Marty on 30/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import GraphicsFramework

public protocol CanvasDrawableProtocol {
    func draw(graphicsLib: CanvasProtocol)
}
