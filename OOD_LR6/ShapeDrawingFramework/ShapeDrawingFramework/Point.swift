//
//  Point.swift
//  ShapeDrawingFramework
//
//  Created by Marty on 30/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

public struct Point {
    public init(y: Int, x: Int) {
        self.y = y
        self.x = x
    }
    
    public var x: Int
    public var y: Int
}
