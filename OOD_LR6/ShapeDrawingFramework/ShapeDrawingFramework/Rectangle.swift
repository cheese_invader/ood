//
//  Rectangle.swift
//  ShapeDrawingFramework
//
//  Created by Marty on 30/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import GraphicsFramework

public class Rectangle: CanvasDrawableProtocol {
    private var leftTop: Point
    private var rightBottom: Point
    
    public init(leftTop: Point, rightBottom: Point) {
        self.leftTop = leftTop
        self.rightBottom = rightBottom
    }
    
    public func draw(graphicsLib: CanvasProtocol) {
        graphicsLib.lineTo(x: leftTop.x, y: leftTop.y)
        graphicsLib.moveTo(x: rightBottom.x, y: leftTop.y)
        
        graphicsLib.lineTo(x: rightBottom.x, y: leftTop.y)
        graphicsLib.moveTo(x: rightBottom.x, y: rightBottom.y)
        
        graphicsLib.lineTo(x: rightBottom.x, y: rightBottom.y)
        graphicsLib.moveTo(x: leftTop.x, y: rightBottom.y)
        
        graphicsLib.lineTo(x: leftTop.x, y: rightBottom.y)
        graphicsLib.moveTo(x: leftTop.x, y: leftTop.y)
    }
}
