//
//  LoggerTests.swift
//  AdapterTests
//
//  Created by Marty on 30/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import GraphicsFramework
import ShapeDrawingFramework
import ModernGraphicsFramework

import XCTest
@testable import Adapter

class Logger: LoggerProtocol {
    private var elements = [String]()
    
    var count: Int {
        return elements.count
    }
    
    func getLogByIndex(_ index: Int) -> String {
        return elements[index]
    }
    
    func log(_ message: String) {
        elements.append(message)
    }
}

class ModernLogger: ModernLoggerProtocol {
    var elements = [String]()
    
    func addLog(_ message: String) {
        elements.append(message)
    }
}

class LoggerTests: XCTestCase {
    
    func testLogger() {
        let logger: LoggerProtocol = Logger()
        logger.log("test log")
        XCTAssertEqual(logger.count, 1)
        XCTAssertEqual(logger.getLogByIndex(0), "test log")
        
        logger.log("test log 2")
        XCTAssertEqual(logger.count, 2)
        XCTAssertEqual(logger.getLogByIndex(0), "test log")
        XCTAssertEqual(logger.getLogByIndex(1), "test log 2")
    }
    
    func testModernLoggerCanReturnValueAfterLog() {
        let logger: ModernLoggerProtocol = ModernLogger()
        logger.addLog("test log")
        XCTAssertEqual(logger.elements.count, 1)
        XCTAssertEqual(logger.elements[0], "test log")
        
        logger.addLog("test log 2")
        XCTAssertEqual(logger.elements.count, 2)
        XCTAssertEqual(logger.elements[0], "test log")
        XCTAssertEqual(logger.elements[1], "test log 2")
    }
    
    func testLoggerByAdapterInteraction() {
        let logger: LoggerProtocol = Logger()
        let adapter = LoggerAdapter(logger: logger)
        
        adapter.addLog("test log")
        XCTAssertEqual(logger.count, 1)
        XCTAssertEqual(logger.getLogByIndex(0), "test log")
        
        XCTAssertEqual(adapter.elements[0], "test log")
        
        adapter.addLog("test log 2")
        XCTAssertEqual(logger.count, 2)
        XCTAssertEqual(logger.getLogByIndex(0), "test log")
        XCTAssertEqual(logger.getLogByIndex(1), "test log 2")
        
        XCTAssertEqual(adapter.elements[0], "test log")
        XCTAssertEqual(adapter.elements[1], "test log 2")
    }
    
    func testLoggerByLoggerInteraction() {
        let logger: LoggerProtocol = Logger()
        let adapter = LoggerAdapter(logger: logger)
        
        logger.log("test log")
        XCTAssertEqual(logger.count, 1)
        XCTAssertEqual(logger.getLogByIndex(0), "test log")
        
        XCTAssertEqual(adapter.elements[0], "test log")
        
        logger.log("test log 2")
        XCTAssertEqual(logger.count, 2)
        XCTAssertEqual(logger.getLogByIndex(0), "test log")
        XCTAssertEqual(logger.getLogByIndex(1), "test log 2")
        
        XCTAssertEqual(adapter.elements[0], "test log")
        XCTAssertEqual(adapter.elements[1], "test log 2")
    }
    
    func testLoggerByMultipleInteraction() {
        let logger: LoggerProtocol = Logger()
        let adapter = LoggerAdapter(logger: logger)
        
        logger.log("test log")
        XCTAssertEqual(logger.count, 1)
        XCTAssertEqual(logger.getLogByIndex(0), "test log")
        
        XCTAssertEqual(adapter.elements[0], "test log")
        
        adapter.addLog("test log 2")
        XCTAssertEqual(logger.count, 2)
        XCTAssertEqual(logger.getLogByIndex(0), "test log")
        XCTAssertEqual(logger.getLogByIndex(1), "test log 2")
        
        XCTAssertEqual(adapter.elements[0], "test log")
        XCTAssertEqual(adapter.elements[1], "test log 2")
    }
}
