//
//  CanvasTest.swift
//  AdapterTests
//
//  Created by Marty on 30/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import GraphicsFramework
import ShapeDrawingFramework
import ModernGraphicsFramework

import XCTest
@testable import Adapter

class CanvasTests: XCTestCase {
    func testDrawRectangleInCanvas() {
        let rectangle = Rectangle(leftTop: Point(y: 30, x: 40), rightBottom: Point(y: 18, x: 24))
        
        let logger = Logger()
        let canvas: CanvasProtocol = Canvas()
        canvas.setLogger(logger)
        
        let painter = CanvasPainter(canvas: canvas)
        
        painter.draw(rectangle)
        
        XCTAssertEqual(logger.count, 8)
        XCTAssertEqual(logger.getLogByIndex(0), "Line to: (40, 30)")
        XCTAssertEqual(logger.getLogByIndex(1), "Move to: (24, 30)")
        XCTAssertEqual(logger.getLogByIndex(2), "Line to: (24, 30)")
        XCTAssertEqual(logger.getLogByIndex(3), "Move to: (24, 18)")
        XCTAssertEqual(logger.getLogByIndex(4), "Line to: (24, 18)")
        XCTAssertEqual(logger.getLogByIndex(5), "Move to: (40, 18)")
        XCTAssertEqual(logger.getLogByIndex(6), "Line to: (40, 18)")
        XCTAssertEqual(logger.getLogByIndex(7), "Move to: (40, 30)")
    }
    
    func testDrawTriangleInCanvas() {
        let triangle = Triangle(vertex1: Point(y: 10, x: 15),
                                vertex2: Point(y: 100, x: 200),
                                vertex3: Point(y: 10, x: 15))
        
        let logger = Logger()
        let canvas: CanvasProtocol = Canvas()
        canvas.setLogger(logger)
        
        let painter = CanvasPainter(canvas: canvas)
        
        painter.draw(triangle)
        
        XCTAssertEqual(logger.count, 6)
        XCTAssertEqual(logger.getLogByIndex(0), "Line to: (15, 10)")
        XCTAssertEqual(logger.getLogByIndex(1), "Move to: (200, 100)")
        XCTAssertEqual(logger.getLogByIndex(2), "Line to: (200, 100)")
        XCTAssertEqual(logger.getLogByIndex(3), "Move to: (15, 10)")
        XCTAssertEqual(logger.getLogByIndex(4), "Line to: (15, 10)")
        XCTAssertEqual(logger.getLogByIndex(5), "Move to: (15, 10)")
    }
    
    func testDrawingRectangleInModernRenderer() {
        let rectangle = Rectangle(leftTop: Point(y: 30, x: 40), rightBottom: Point(y: 18, x: 24))
        
        let logger = ModernLogger()
        let canvas: ModernGraphicsRenderer = ModernGraphicsRenderer(logger: logger)
        canvas.setLogger(logger)
        
        let adapter = GraphicsAdapter(renderer: canvas)
        
        let painter = CanvasPainter(canvas: adapter)

        
        painter.draw(rectangle)
        XCTAssertEqual(logger.elements.count, 12)
        XCTAssertEqual(logger.elements[0], "<draw>\n")
        XCTAssertEqual(logger.elements[1], "(<line fromX=\"40\" fromY=\"30\" toX=\"24\" toY=\"30\" r=\"0.0\" g=\"0.0\" b=\"0.0\" a=\"0.0\"/>)\n")
        XCTAssertEqual(logger.elements[2], "</draw>\n")
        XCTAssertEqual(logger.elements[3], "<draw>\n")
        XCTAssertEqual(logger.elements[4], "(<line fromX=\"24\" fromY=\"30\" toX=\"24\" toY=\"18\" r=\"0.0\" g=\"0.0\" b=\"0.0\" a=\"0.0\"/>)\n")
        XCTAssertEqual(logger.elements[5], "</draw>\n")
        XCTAssertEqual(logger.elements[6], "<draw>\n")
        XCTAssertEqual(logger.elements[7], "(<line fromX=\"24\" fromY=\"18\" toX=\"40\" toY=\"18\" r=\"0.0\" g=\"0.0\" b=\"0.0\" a=\"0.0\"/>)\n")
        XCTAssertEqual(logger.elements[8], "</draw>\n")
        XCTAssertEqual(logger.elements[9], "<draw>\n")
        XCTAssertEqual(logger.elements[10], "(<line fromX=\"40\" fromY=\"18\" toX=\"40\" toY=\"30\" r=\"0.0\" g=\"0.0\" b=\"0.0\" a=\"0.0\"/>)\n")
        XCTAssertEqual(logger.elements[11], "</draw>\n")
        
    }
}
