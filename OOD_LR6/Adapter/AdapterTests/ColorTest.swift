//
//  ColorTest.swift
//  AdapterTests
//
//  Created by Marty on 31/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import GraphicsFramework
import ShapeDrawingFramework
import ModernGraphicsFramework

import XCTest
@testable import Adapter

class ColorTests: XCTestCase {
    func testDrawRactangle() {
        let rectangle = Rectangle(leftTop: Point(y: 30, x: 40), rightBottom: Point(y: 18, x: 24))
        let logger = Logger()
        let canvas: CanvasProtocol = Canvas()
        canvas.setLogger(logger)
        canvas.setColor(6)
        
        let painter = CanvasPainter(canvas: canvas)
        
        painter.draw(rectangle)
        
        XCTAssertEqual(logger.count, 9)
        XCTAssertEqual(logger.getLogByIndex(0), "SetColor (#6)")
        XCTAssertEqual(logger.getLogByIndex(1), "Line to: (40, 30)")
        XCTAssertEqual(logger.getLogByIndex(2), "Move to: (24, 30)")
        XCTAssertEqual(logger.getLogByIndex(3), "Line to: (24, 30)")
        XCTAssertEqual(logger.getLogByIndex(4), "Move to: (24, 18)")
        XCTAssertEqual(logger.getLogByIndex(5), "Line to: (24, 18)")
        XCTAssertEqual(logger.getLogByIndex(6), "Move to: (40, 18)")
        XCTAssertEqual(logger.getLogByIndex(7), "Line to: (40, 18)")
        XCTAssertEqual(logger.getLogByIndex(8), "Move to: (40, 30)")
    }
    
    func testDrawRectWithAdapterAndDefaultColor() {
        let rectangle = Rectangle(leftTop: Point(y: 30, x: 40), rightBottom: Point(y: 18, x: 24))
        let logger = Logger()
        let loggerAdapter = LoggerAdapter(logger: logger)
        
        let renderer = ModernGraphicsRenderer(logger: loggerAdapter)
        
        let adapter = GraphicsAdapter(renderer: renderer)
        
        let painter = CanvasPainter(canvas: adapter)
        
        painter.draw(rectangle)
        
        XCTAssertEqual(loggerAdapter.elements.count, 12)
        XCTAssertEqual(loggerAdapter.elements[0], "<draw>\n")
        XCTAssertEqual(loggerAdapter.elements[1], "(<line fromX=\"40\" fromY=\"30\" toX=\"24\" toY=\"30\" r=\"0.0\" g=\"0.0\" b=\"0.0\" a=\"0.0\"/>)\n")
        XCTAssertEqual(loggerAdapter.elements[2], "</draw>\n")
        XCTAssertEqual(loggerAdapter.elements[3], "<draw>\n")
        XCTAssertEqual(loggerAdapter.elements[4], "(<line fromX=\"24\" fromY=\"30\" toX=\"24\" toY=\"18\" r=\"0.0\" g=\"0.0\" b=\"0.0\" a=\"0.0\"/>)\n")
        XCTAssertEqual(loggerAdapter.elements[5], "</draw>\n")
        XCTAssertEqual(loggerAdapter.elements[6], "<draw>\n")
        XCTAssertEqual(loggerAdapter.elements[7], "(<line fromX=\"24\" fromY=\"18\" toX=\"40\" toY=\"18\" r=\"0.0\" g=\"0.0\" b=\"0.0\" a=\"0.0\"/>)\n")
        XCTAssertEqual(loggerAdapter.elements[8], "</draw>\n")
        XCTAssertEqual(loggerAdapter.elements[9], "<draw>\n")
        XCTAssertEqual(loggerAdapter.elements[10], "(<line fromX=\"40\" fromY=\"18\" toX=\"40\" toY=\"30\" r=\"0.0\" g=\"0.0\" b=\"0.0\" a=\"0.0\"/>)\n")
        XCTAssertEqual(loggerAdapter.elements[11], "</draw>\n")
    }
    
    func testDrawRectWithAdapter() {
        let rectangle = Rectangle(leftTop: Point(y: 30, x: 40), rightBottom: Point(y: 18, x: 24))
        let logger = Logger()
        let loggerAdapter = LoggerAdapter(logger: logger)
        
        let renderer = ModernGraphicsRenderer(logger: loggerAdapter)
        
        let adapter = GraphicsAdapter(renderer: renderer)
        adapter.setColor(0x010203)
        
        let painter = CanvasPainter(canvas: adapter)
        
        painter.draw(rectangle)
        
        XCTAssertEqual(loggerAdapter.elements.count, 12)
        XCTAssertEqual(loggerAdapter.elements[0], "<draw>\n")
        XCTAssertEqual(loggerAdapter.elements[1], "(<line fromX=\"40\" fromY=\"30\" toX=\"24\" toY=\"30\" r=\"1.0\" g=\"2.0\" b=\"3.0\" a=\"1.0\"/>)\n")
        XCTAssertEqual(loggerAdapter.elements[2], "</draw>\n")
        XCTAssertEqual(loggerAdapter.elements[3], "<draw>\n")
        XCTAssertEqual(loggerAdapter.elements[4], "(<line fromX=\"24\" fromY=\"30\" toX=\"24\" toY=\"18\" r=\"1.0\" g=\"2.0\" b=\"3.0\" a=\"1.0\"/>)\n")
        XCTAssertEqual(loggerAdapter.elements[5], "</draw>\n")
        XCTAssertEqual(loggerAdapter.elements[6], "<draw>\n")
        XCTAssertEqual(loggerAdapter.elements[7], "(<line fromX=\"24\" fromY=\"18\" toX=\"40\" toY=\"18\" r=\"1.0\" g=\"2.0\" b=\"3.0\" a=\"1.0\"/>)\n")
        XCTAssertEqual(loggerAdapter.elements[8], "</draw>\n")
        XCTAssertEqual(loggerAdapter.elements[9], "<draw>\n")
        XCTAssertEqual(loggerAdapter.elements[10], "(<line fromX=\"40\" fromY=\"18\" toX=\"40\" toY=\"30\" r=\"1.0\" g=\"2.0\" b=\"3.0\" a=\"1.0\"/>)\n")
        XCTAssertEqual(loggerAdapter.elements[11], "</draw>\n")
    }
    
    func testDrawTriangleWithAdapter() {
        let triangle = Triangle(vertex1: Point(y: 10, x: 15),
                                vertex2: Point(y: 100, x: 200),
                                vertex3: Point(y: 10, x: 15))
        let logger = Logger()
        let loggerAdapter = LoggerAdapter(logger: logger)
        
        let renderer = ModernGraphicsRenderer(logger: loggerAdapter)
        
        let adapter = GraphicsAdapter(renderer: renderer)
        adapter.setColor(0x010203)
        
        let painter = CanvasPainter(canvas: adapter)
        
        painter.draw(triangle)
        
        XCTAssertEqual(loggerAdapter.elements.count, 9)
        XCTAssertEqual(loggerAdapter.elements[0], "<draw>\n")
        XCTAssertEqual(loggerAdapter.elements[1], "(<line fromX=\"15\" fromY=\"10\" toX=\"200\" toY=\"100\" r=\"1.0\" g=\"2.0\" b=\"3.0\" a=\"1.0\"/>)\n")
        XCTAssertEqual(loggerAdapter.elements[2], "</draw>\n")
        XCTAssertEqual(loggerAdapter.elements[3], "<draw>\n")
            XCTAssertEqual(loggerAdapter.elements[4], "(<line fromX=\"200\" fromY=\"100\" toX=\"15\" toY=\"10\" r=\"1.0\" g=\"2.0\" b=\"3.0\" a=\"1.0\"/>)\n")
        XCTAssertEqual(loggerAdapter.elements[5], "</draw>\n")
        XCTAssertEqual(loggerAdapter.elements[6], "<draw>\n")
            XCTAssertEqual(loggerAdapter.elements[7], "(<line fromX=\"15\" fromY=\"10\" toX=\"15\" toY=\"10\" r=\"1.0\" g=\"2.0\" b=\"3.0\" a=\"1.0\"/>)\n")
        XCTAssertEqual(loggerAdapter.elements[8], "</draw>\n")
    }
}
