//
//  ViewController.swift
//  Adapter
//
//  Created by Marty on 30/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

import GraphicsFramework
import ShapeDrawingFramework
import ModernGraphicsFramework


class ViewController: UIViewController {
    
    @IBOutlet weak var newApiUsingSwitcher: UISwitch!
    
    @IBAction func PaintButtonWasTapped(_ sender: UIButton) {
        if newApiUsingSwitcher.isOn {
            paintPictureOnModernGraphicsRenderer()
        } else {
            paintPictureOnCanvas()
        }
    }
    
    func paintPicture(painter: CanvasPainter) {
        let triangle = Triangle(vertex1: Point(y: 10, x: 15),
                                vertex2: Point(y: 100, x: 200),
                                vertex3: Point(y: 10, x: 15))
        let rectangle = Rectangle(leftTop: Point(y: 30, x: 40), rightBottom: Point(y: 18, x: 24))
        
        painter.draw(triangle)
        painter.draw(rectangle)
    }
    
    func paintPictureOnModernGraphicsRenderer() {
        let renderer = ModernGraphicsRenderer(logger: nil)
        let adaptedCanvas = GraphicsAdapter(renderer: renderer)
        let painter = CanvasPainter(canvas: adaptedCanvas)
        paintPicture(painter: painter)
    }
    
    func paintPictureOnCanvas() {
        let canvas = Canvas()
        let painter = CanvasPainter(canvas: canvas)
        paintPicture(painter: painter)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}

