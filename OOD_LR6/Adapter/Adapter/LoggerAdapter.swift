//
//  LoggerAdapter.swift
//  Adapter
//
//  Created by Marty on 30/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import GraphicsFramework
import ModernGraphicsFramework

class LoggerAdapter: ModernLoggerProtocol {
    private let logger: LoggerProtocol
    
    init(logger: LoggerProtocol) {
        self.logger = logger
    }
    
    var elements: [String] {
        var result = [String]()
        
        for i in 0..<logger.count {
            result.append(logger.getLogByIndex(i))
        }
        return result
    }
    
    func addLog(_ message: String) {
        logger.log(message)
    }
}
