//
//  GraphicsAdapter.swift
//  Adapter
//
//  Created by Marty on 30/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import GraphicsFramework
import ModernGraphicsFramework

class GraphicsAdapter: CanvasProtocol {
    private let renderer: ModernGraphicsRenderer
    private var startPoint: Point?
    private var color = RGBAColor(r: 0, g: 0, b: 0, a: 0)
    
    init(renderer: ModernGraphicsRenderer) {
        self.renderer = renderer
    }
    
    func setLogger(_ logger: LoggerProtocol) {
        let adaptedLogger = LoggerAdapter(logger: logger)
        renderer.setLogger(adaptedLogger)
    }
    
    func setColor(_ color: Int) {
        self.color = RGBAColor(r: Double((color & 0xff0000) >> 16), g:  Double((color & 0x00ff00) >> 8), b:  Double(color & 0x0000ff), a: 1)
    }
    
    func lineTo(x: Int, y: Int) {
        startPoint = Point(y: y, x: x)
    }
    
    func moveTo(x: Int, y: Int) {
        guard let startPoint = startPoint else {
            renderer.logger?.addLog("LineTo method wasn't called")
            return
        }
        
        do {
            try renderer.beginDraw()
            try renderer.drawLine(start: startPoint, end: Point(y: y, x: x), color: color)
            try renderer.endDraw()
        } catch {
            renderer.logger?.addLog(error.localizedDescription)
        }
        self.startPoint = nil
    }
}
