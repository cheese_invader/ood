//
//  RGBAColor.swift
//  ModernGraphicsFramework
//
//  Created by Marty on 31/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

public struct RGBAColor {
    public var r: Double
    public var g: Double
    public var b: Double
    public var a: Double
    
    public init(r: Double, g: Double, b: Double, a: Double) {
        self.r = r
        self.g = g
        self.b = b
        self.a = a
    }
}
