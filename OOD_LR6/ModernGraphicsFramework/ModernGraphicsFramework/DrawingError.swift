//
//  DrawingError.swift
//  ModernGraphicsFramework
//
//  Created by Marty on 30/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

enum DrawingError: Error {
    case drawingHasAlreadyBegun
    case drawingBegunHasntCalled
    case drawingHasntBeenStarted
}
