//
//  ModernGraphicsRenderer.swift
//  ModernGraphicsFramework
//
//  Created by Marty on 30/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

open class ModernGraphicsRenderer {
    private var isDrawwing = false
    public private(set) var logger: ModernLoggerProtocol?
    
    public init(logger: ModernLoggerProtocol?) {
        self.logger = logger
    }
    
    deinit {
        if isDrawwing {
            try? endDraw()
        }
    }
    
    public func setLogger(_ logger: ModernLoggerProtocol) {
        self.logger = logger
    }
    
    public func beginDraw() throws {
        if isDrawwing {
            throw DrawingError.drawingHasAlreadyBegun
        }
        logger?.addLog("<draw>\n")
        isDrawwing = true
        print("<draw>\n")
    }
    
    public func drawLine(start: Point, end: Point, color: RGBAColor = RGBAColor(r: 0, g: 0, b: 0, a: 0)) throws {
        if !isDrawwing {
            throw DrawingError.drawingBegunHasntCalled
        }
        logger?.addLog("(<line fromX=\"\(start.x)\" fromY=\"\(start.y)\" toX=\"\(end.x)\" toY=\"\(end.y)\" r=\"\(color.r)\" g=\"\(color.g)\" b=\"\(color.b)\" a=\"\(color.a)\"/>)\n")
        print("(<line fromX=\"\(start.x)\" fromY=\"\(start.y)\" toX=\"\(end.x)\" toY=\"\(end.y)\" r=\"\(color.r)\" g=\"\(color.g)\" b=\"\(color.b)\" a=\"\(color.a)\"/>)\n")
    }

    public func endDraw() throws {
        if !isDrawwing {
            throw DrawingError.drawingHasntBeenStarted
        }
        isDrawwing = false
        logger?.addLog("</draw>\n")
        print("</draw>\n")
    }
    
}
