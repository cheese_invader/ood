//
//  Point.swift
//  ModernGraphicsFramework
//
//  Created by Marty on 30/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

public struct Point {
    public var y: Int
    public var x: Int
    
    public init(y: Int, x: Int) {
        self.y = y
        self.x = x
    }
}
