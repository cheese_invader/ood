//
//  LoggerProtocol.swift
//  ModernGraphicsFramework
//
//  Created by Marty on 30/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

public protocol ModernLoggerProtocol {
    var elements: [String] { get }
    func addLog(_ message: String)
}
