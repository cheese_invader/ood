//
//  StateContextProtocol.swift
//  OOD_LR8
//
//  Created by Marty on 22/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

public protocol StateContextProtocol {
    var ballsCount: Int { get }
    var quartersCount: Int { get }
    var canInsertOneMoreCoin: Bool { get }
    var state: StateProtocol! { get }
    
    func releaseBall()
    func releaseCoins()
    func addCoin()
    func setSoldOutState()
    func setNoQuarterState()
    func setSoldState()
    func setHasQuarterState()
}
