//
//  GumballMachine.swift
//  OOD_LR8
//
//  Created by Marty on 22/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class GumballMachine {
    private let context: StateContextProtocol
    
    init(ballsCount: Int, logger: LoggerProtocol) {
        context = GumballMachineInContext(ballsCount: ballsCount, logger: logger)
    }
    
    init(ballsCount: Int) {
        context = GumballMachineInContext(ballsCount: ballsCount, logger: nil)
    }
    
    func insertQuarter() {
        context.state.insertQuarter()
    }
    
    func ejectQuarter() {
        context.state.ejectQuarters()
    }
    
    func turnCrank() {
        context.state.turnCrank()
        context.state.dispense()
    }
    
    func toString() -> String {
        return """
        GumballMachine, Inc.
        Copyright © 2019 Marty. All rights reserved.
        If you have any problems see you in court
        
        Inventory: \(context.ballsCount != 0 ? String(context.ballsCount) : "no") gumball\(context.ballsCount != 1 ? "s" : "")
        Machine is \(context.state.toString())
        """
    }
}
