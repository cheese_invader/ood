//
//  HasQuarterState.swift
//  OOD_LR8
//
//  Created by Marty on 18/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class HasQuarterState: StateProtocol {
    init(gumballMachine: StateContextProtocol, logger: LoggerProtocol?) {
        self.gumballMachine = gumballMachine
        self.logger = logger
    }
    
    func insertQuarter() {
        var response = "You can't insert another quarter\n"
        if gumballMachine.canInsertOneMoreCoin {
            response = "You inserted a quarter\n"
            gumballMachine.addCoin()
        }
        
        logger?.addLog(response)
        print(response)
    }
    
    func ejectQuarters() {
        gumballMachine.setNoQuarterState()
        let response = "Get your \(gumballMachine.quartersCount) quarters\n"
        gumballMachine.releaseCoins()
        logger?.addLog(response)
        print(response)
    }
    
    func turnCrank() {
        gumballMachine.setSoldState()
        let response = "You turned...\n"
        logger?.addLog(response)
        print(response)
    }
    
    func dispense() {
        let response = "No gumball dispensed\n"
        logger?.addLog(response)
        print(response)
    }
    
    func toString() -> String {
        return "Waiting for turn of crank\n"
    }
    
    
    private var gumballMachine: StateContextProtocol
    private var logger: LoggerProtocol?
}
