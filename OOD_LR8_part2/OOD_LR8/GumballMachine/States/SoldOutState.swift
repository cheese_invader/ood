//
//  SoldOutState.swift
//  OOD_LR8
//
//  Created by Marty on 18/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class SoldOutState: StateProtocol {
    init(gumballMachine: StateContextProtocol, logger: LoggerProtocol?) {
        self.gumballMachine = gumballMachine
        self.logger = logger
    }
    
    func insertQuarter() {
        let response = "You can't insert a quarter, the machine is sold out\n"
        logger?.addLog(response)
        print(response)
    }
    
    func ejectQuarters() {
        var response = "You can't eject, you haven't inserted a quarter yet\n"
        if gumballMachine.quartersCount != 0 {
            response = "Get your \(gumballMachine.quartersCount) quarters\n"
            gumballMachine.releaseCoins()
        }
        logger?.addLog(response)
        print(response)
    }
    
    func turnCrank() {
        let response = "You turned but there's no gumballs\n"
        logger?.addLog(response)
        print(response)
    }
    
    func dispense() {
        let response = "No gumball dispensed\n"
        logger?.addLog(response)
        print(response)
    }
    
    func toString() -> String {
        return "Sold out\n"
    }
    
    
    private var gumballMachine: StateContextProtocol
    private var logger: LoggerProtocol?
}
