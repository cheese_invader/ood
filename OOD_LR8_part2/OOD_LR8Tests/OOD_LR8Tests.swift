//
//  OOD_LR8Tests.swift
//  OOD_LR8Tests
//
//  Created by Marty on 22/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import XCTest
@testable import OOD_LR8

private class MockStateContext: StateContextProtocol {
    init(logger: LoggerProtocol) {
        self.ballsCount = 0
        self.logger = logger
        self.state = SoldOutState(gumballMachine: self, logger: logger)
    }
    
    var ballsCount: Int
    
    var quartersCount: Int = 0
    
    var canInsertOneMoreCoin: Bool {
        return quartersCount != 5
    }
    
    var state: StateProtocol!
    
    let logger: LoggerProtocol
    
    
    func releaseBall() {
        logger.addLog("Releasing ball")
    }
    
    func releaseCoins() {
        logger.addLog("Released \(quartersCount) coins")
        quartersCount = 0
    }
    
    func addCoin() {
        logger.addLog("Coin was added")
        quartersCount += 1
    }
    
    func setSoldOutState() {
        logger.addLog("-> Sold Out")
    }
    
    func setNoQuarterState() {
        logger.addLog("-> No Quarter")
    }
    
    func setSoldState() {
        logger.addLog("-> Sold")
    }
    
    func setHasQuarterState() {
        logger.addLog("-> Has Quarter")
    }
}


private class MockGumballMachine {    
    var ballsCount: Int {
        get {
            return context.ballsCount
        }
    }
    
    private let context: MockStateContext
    
    init(context: MockStateContext) {
        self.context = context
    }
    
    func insertQuarter() {
        context.state.insertQuarter()
    }
    
    func ejectQuarter() {
        context.state.ejectQuarters()
    }
    
    func turnCrank() {
        context.state.turnCrank()
        context.state.dispense()
    }
    
    func toString() -> String {
        return """
        GumballMachine, Inc.
        Copyright © 2019 Marty. All rights reserved.
        If you have any problems see you in court
        
        Inventory: \(context.ballsCount != 0 ? String(context.ballsCount) : "no") gumball\(context.ballsCount != 1 ? "s" : "")
        Machine is \(context.state.toString())
        """
    }
}


class OOD_LR8Tests: XCTestCase {
    // MARK: - Sold out state -
    
    func testSoldOutState_UserCantInsertQuarter() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        let machine = MockGumballMachine(context: context)
        
        machine.insertQuarter()
        XCTAssertEqual(logger.getLastLog()!, "You can't insert a quarter, the machine is sold out\n")
    }
    
    func testSoldOutState_MachineDoesntTransitInNewStateWhileInsertingQuarter() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        let machine = MockGumballMachine(context: context)
        
        machine.insertQuarter()
        XCTAssertEqual(logger.logsCount, 1)
    }
    
    func testSoldOutState_UserCAntEjectQuarter() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        let machine = MockGumballMachine(context: context)
        
        machine.ejectQuarter()
        XCTAssertEqual(logger.getLastLog(), "You can't eject, you haven't inserted a quarter yet\n")
    }
    
    func testSoldOutState_MachineDoesntTransferInNewStateWhileEjectionQuarter() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        let machine = MockGumballMachine(context: context)
        
        machine.ejectQuarter()
        XCTAssertEqual(logger.getLastLog(), "You can't eject, you haven't inserted a quarter yet\n")
        XCTAssertEqual(logger.logsCount, 1)
    }
    
    func testSoldOutState_TurningCrankDoesntBringAnyChanges() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        let machine = MockGumballMachine(context: context)
        
        machine.turnCrank()
        XCTAssertEqual(logger.getLogAtIndex(0), "You turned but there's no gumballs\n")
        XCTAssertEqual(logger.getLastLog(), "No gumball dispensed\n")
    }

    func testSoldOutState_TurningCrankDoesntTransferInNewState() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        let machine = MockGumballMachine(context: context)
        
        machine.turnCrank()
        XCTAssertEqual(logger.getLogAtIndex(0), "You turned but there's no gumballs\n")
        XCTAssertEqual(logger.getLastLog(), "No gumball dispensed\n")
    }
    
    func testSoldOutState_UserCanReturnExtraCoins() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        let machine = MockGumballMachine(context: context)
        context.quartersCount = 3
        
        machine.ejectQuarter()
        XCTAssertEqual(logger.getLastLog(), "Get your 3 quarters\n")
    }
    
    
    // MARK: - No quarter state -
    
    func testNoQuarterState_UserCanInsertQuarter() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        context.ballsCount = 3
        
        let machine = MockGumballMachine(context: context)
        context.state = NoQuarterState(gumballMachine: context, logger: logger)
        
        machine.insertQuarter()
        
        XCTAssertEqual(logger.getLastLog(), "You inserted a quarter\n")
    }
    
    func testNoQuarterState_MachineGetsHasQuarterStateAfterInsertionQuarter() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        context.ballsCount = 1
        context.state = NoQuarterState(gumballMachine: context, logger: logger)
        
        let machine = MockGumballMachine(context: context)
        
        machine.insertQuarter()

        
        XCTAssertEqual(logger.getLogAtIndex(0), "-> Has Quarter")
    }
    
    func testNoQuarterState_UserCantGetGumballTurningCrank() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        
        let machine = MockGumballMachine(context: context)
        context.ballsCount = 3
        context.state = NoQuarterState(gumballMachine: context, logger: logger)
        
        machine.turnCrank()
        
        XCTAssertEqual(logger.getLastLog(), "You need to pay first\n")
    }
    
    func testNoQuarterState_UserCantEjectQuarter() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        
        let machine = MockGumballMachine(context: context)
        context.ballsCount = 3
        context.state = NoQuarterState(gumballMachine: context, logger: logger)
        
        machine.ejectQuarter()
        XCTAssertEqual(logger.getLastLog(), "You haven't inserted a quarter\n")
    }
    
    func testNoQuarterState_TurninkCrankDoesntBringAnyChanges() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        
        let machine = MockGumballMachine(context: context)
        context.ballsCount = 3
        context.state = NoQuarterState(gumballMachine: context, logger: logger)
        
        machine.turnCrank()
        XCTAssertEqual(logger.getLogAtIndex(0), "You turned but there's no quarter\n")
    }
    
    
    // MARK: - Has quarter state -
    
    func testHasQuarterState_UserCanInsertOneMoreQuarter() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        context.ballsCount = 3
        
        let machine = MockGumballMachine(context: context)
        context.state = HasQuarterState(gumballMachine: context, logger: logger)
        
        machine.insertQuarter()
        
        XCTAssertEqual(logger.getLastLog(), "You inserted a quarter\n")
    }
    
    func testHasQuarterState_UserCanInsertOnlyFiveCoins() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        context.ballsCount = 3
        
        let machine = MockGumballMachine(context: context)
        context.state = HasQuarterState(gumballMachine: context, logger: logger)
        
        for _ in 0 ..< 5 {
            machine.insertQuarter()
            XCTAssertEqual(logger.getLastLog(), "You inserted a quarter\n")
        }
        machine.insertQuarter()
        XCTAssertEqual(logger.getLastLog(), "You can\'t insert another quarter\n")
    }
    
    func testHasQuarterState_UserCanEjectQuarter() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        context.ballsCount = 3
        
        let machine = MockGumballMachine(context: context)
        context.state = HasQuarterState(gumballMachine: context, logger: logger)
        
        machine.ejectQuarter()
        
        XCTAssertEqual(logger.getLastLog(), "Get your 0 quarters\n")
    }
    
    func testHasQuarterState_UserCanEjectQuarters() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        context.ballsCount = 3
        context.quartersCount = 5
        
        let machine = MockGumballMachine(context: context)
        context.state = HasQuarterState(gumballMachine: context, logger: logger)
        
        machine.ejectQuarter()
        
        XCTAssertEqual(logger.getLastLog(), "Get your 5 quarters\n")
    }
    
    func testHasQuarterState_TurningCrankBringSoldState() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        context.ballsCount = 3
        
        let machine = MockGumballMachine(context: context)
        context.state = HasQuarterState(gumballMachine: context, logger: logger)
        
        machine.turnCrank()
        
        XCTAssertEqual(logger.getLogAtIndex(0), "-> Sold")
    }
    
    
    // MARK: - Sold state -
    
    func testSoldState_UserCanInsertQuarter() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        context.ballsCount = 3
        
        let machine = MockGumballMachine(context: context)
        context.state = SoldState(gumballMachine: context, logger: logger)
        
        machine.insertQuarter()
        
        XCTAssertEqual(logger.getLastLog(), "Quarter inserted\n")
    }
    
    func testSoldState_UserCanInsertOnlyFiveQuarters() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        context.ballsCount = 3
        
        let machine = MockGumballMachine(context: context)
        context.state = SoldState(gumballMachine: context, logger: logger)
        
        for _ in 0 ..< 5 {
            machine.insertQuarter()
            XCTAssertEqual(logger.getLastLog(), "Quarter inserted\n")
        }
        
        machine.insertQuarter()
        XCTAssertEqual(logger.getLastLog(), "You cant insert one more quarter\n")
    }
    
    func testSoldState_UserCantEjectQuarter() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        context.ballsCount = 3
        
        let machine = MockGumballMachine(context: context)
        context.state = SoldState(gumballMachine: context, logger: logger)
        
        machine.ejectQuarter()
        
        XCTAssertEqual(logger.getLastLog(), "No quarters\n")
    }
    
    func testSoldState_UserCanEjectQuarterWhenItExists() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        context.ballsCount = 3
        context.quartersCount = 1
        
        let machine = MockGumballMachine(context: context)
        context.state = SoldState(gumballMachine: context, logger: logger)
        
        machine.ejectQuarter()
        
        XCTAssertEqual(logger.getLastLog(), "Get your 1 quarters\n")
    }
    
    func testSoldState_TurningCrankDoesntBringAnyChanges() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        context.ballsCount = 3
        
        let machine = MockGumballMachine(context: context)
        context.state = SoldState(gumballMachine: context, logger: logger)
        
        machine.turnCrank()
        
        XCTAssertEqual(logger.getLogAtIndex(0), "Turning twice doesn't get you another gumball\n")
    }
    
    // Dispense check MARK: - Issue 2 -
    func testSoldState_TurningCrankGetYouSoldOutStateWhenThereIsNoGumbal() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        context.ballsCount = 0
        
        let machine = MockGumballMachine(context: context)
        context.state = SoldState(gumballMachine: context, logger: logger)
        
        machine.turnCrank()
        
        XCTAssertEqual(logger.getLogAtIndex(2), "-> Sold Out")
    }
    
    func testSoldState_TurningCrankGetYouNoQuarterStateWhenThereAreGumbals() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        context.ballsCount = 1
        
        let machine = MockGumballMachine(context: context)
        context.state = SoldState(gumballMachine: context, logger: logger)
        
        machine.turnCrank()
        
        XCTAssertEqual(logger.getLogAtIndex(2), "-> No Quarter")
    }
    
    func testSoldState_TurningCrankGetYouHasQuarterStateWhenThereAreGumbalsAndThereAreQuarters() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        context.ballsCount = 1
        context.quartersCount = 3
        
        let machine = MockGumballMachine(context: context)
        context.state = SoldState(gumballMachine: context, logger: logger)
        
        machine.turnCrank()
        
        XCTAssertEqual(logger.getLogAtIndex(2), "-> Has Quarter")
    }
    
    // MARK: - Global machine testing -
    
    func testMachine_CantInsertQuarterInEmptyMachine() {
        let logger = Logger()
        let machine = GumballMachine(ballsCount: 0, logger: logger)
        
        machine.insertQuarter()
        
        XCTAssertEqual(logger.getLastLog(), "You can't insert a quarter, the machine is sold out\n")
    }
    
    func testMachine_CantEjectQuarterInEmptyMachine() {
        let logger = Logger()
        let machine = GumballMachine(ballsCount: 0, logger: logger)
        
        machine.ejectQuarter()
        
        XCTAssertEqual(logger.getLastLog(), "You can't eject, you haven't inserted a quarter yet\n")
    }
    
    func testMachine_CantTurnCrankInEmptyMachine() {
        let logger = Logger()
        let machine = GumballMachine(ballsCount: 0, logger: logger)
        
        machine.turnCrank()
        
        XCTAssertEqual(logger.getLogAtIndex(0), "You turned but there\'s no gumballs\n")
        XCTAssertEqual(logger.getLastLog(), "No gumball dispensed\n")
    }
    
    func testMachine_CanInsertQuarterInMachine() {
        let logger = Logger()
        let machine = GumballMachine(ballsCount: 1, logger: logger)
        
        machine.insertQuarter()
        
        XCTAssertEqual(logger.getLastLog(), "You inserted a quarter\n")
    }
    
    func testMachine_CantEjectQuarterWithoutInsertion() {
        let logger = Logger()
        let machine = GumballMachine(ballsCount: 1, logger: logger)
        
        machine.ejectQuarter()
        
        XCTAssertEqual(logger.getLastLog(), "You haven't inserted a quarter\n")
    }
    
    func testMachine_CantTurnCrankWithoutQuarter() {
        let logger = Logger()
        let machine = GumballMachine(ballsCount: 1, logger: logger)
        
        machine.turnCrank()
        
        XCTAssertEqual(logger.getLogAtIndex(0), "You turned but there's no quarter\n")
        XCTAssertEqual(logger.getLastLog(), "You need to pay first\n")
    }
    
    func testMachine_CanTurnCrankWithQuarter() {
        let logger = Logger()
        let machine = GumballMachine(ballsCount: 1, logger: logger)
        
        machine.insertQuarter()
        machine.turnCrank()
        
        XCTAssertEqual(logger.getLogAtIndex(1), "You turned...\n")
        XCTAssertEqual(logger.getLastLog(), "Get your gumball\n")
    }
    
    func testMachine_UserCanBuyGumballsUntilMachineIsNotEmpty() {
        let logger = Logger()
        let machine = GumballMachine(ballsCount: 1, logger: logger)
        
        machine.insertQuarter()
        machine.turnCrank()
        machine.insertQuarter()
        
        XCTAssertEqual(logger.getLogAtIndex(0), "You inserted a quarter\n")
        XCTAssertEqual(logger.getLogAtIndex(1), "You turned...\n")
        XCTAssertEqual(logger.getLogAtIndex(2), "Get your gumball\n")
        XCTAssertEqual(logger.getLogAtIndex(3), "You can't insert a quarter, the machine is sold out\n")
    }
}
