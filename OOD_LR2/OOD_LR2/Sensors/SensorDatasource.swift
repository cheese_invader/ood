//
//  SensorDatasource.swift
//  OOD_LR2
//
//  Created by Marty on 09/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol SensorDatasource {
    var sensorsCount: Int { get set }
    func nameForSensorAtIndex(_ index: Int) -> String
}

