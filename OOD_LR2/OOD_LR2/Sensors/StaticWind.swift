//
//  StaticWind.swift
//  OOD_LR2
//
//  Created by Marty on 09/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class StaticWind: SensorCalculator, SensorDatasource {
    let sensorNames = ["Speed", "DirX", "DirY"]
    
    override init() {
        super.init()
        sensorsCount = sensorNames.count
        setDelegate(self)
    }
    
    var sensorsCount = 0
    
    func nameForSensorAtIndex(_ index: Int) -> String {
        return sensorNames[index]
    }
}
