//
//  StaticDisplayObserver.swift
//  OOD_LR2
//
//  Created by Marty on 09/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class StaticDisplayObserver: Observer<WeatherInfo> {
    let staticDisplay = StaticDisplay()
    
    override func update<T>(_ data: WeatherInfo, initiator: ObservableImpl<T>) {
        print("Station \"\(initiator.identifier)\" has registred data: ")
        if let temperature = data.temperature {
            staticDisplay.setNewValue(temperature, forSensorAtIndex: 0)
        }
        if let humidity = data.humidity {
            staticDisplay.setNewValue(humidity, forSensorAtIndex: 1)
        }
        if let pressure = data.pressure {
            staticDisplay.setNewValue(pressure, forSensorAtIndex: 2)
        }
        
        staticDisplay.printInfo()
        print("---------------------------------")
    }
}
