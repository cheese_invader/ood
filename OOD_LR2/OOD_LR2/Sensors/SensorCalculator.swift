//
//  SensorCalculator.swift
//  OOD_LR2
//
//  Created by Marty on 09/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class SensorCalculator {
    private var delegate: SensorDatasource?
    
    
    private var sensorsMinValues   = [Int: Double]()
    private var sensorsMaxValues   = [Int: Double]()
    private var sensorsSumValues   = [Int: Double]()
    private var sensorsValuesCount = [Int: Int]()
    
    private var sensorCounts = 0
    
    
    // Mark: - Getter -
    func minValue(atIndex index: Int) -> Double {
        return sensorsMinValues[index]!
    }
    
    func maxValue(atIndex index: Int) -> Double {
        return sensorsMaxValues[index]!
    }
    
    func avgValue(atIndex index: Int) -> Double {
        return sensorsSumValues[index]! / Double(sensorsValuesCount[index]!)
    }
    
    func setDelegate(_ delegate: SensorDatasource) {
        self.delegate = delegate
    }
    
    
    // MARK: - Setter -
    func setNewValue(_ value: Double, forSensorAtIndex index: Int) {
        guard let delegate = delegate else {
            return
        }
        
        if sensorCounts != delegate.sensorsCount {
            sensorCounts = delegate.sensorsCount
            resetSensors()
        }
        
        if value < sensorsMinValues[index]! {
            sensorsMinValues[index] = value
        }
        
        if value > sensorsMaxValues[index]! {
            sensorsMaxValues[index] = value
        }
        
        sensorsSumValues[index]! += value
        sensorsValuesCount[index]! += 1
    }
    
    
    // MARK: - Helpersb -
    func printInfo() {
        guard let delegate = delegate else {
            return
        }
        if sensorCounts != delegate.sensorsCount {
            sensorCounts = delegate.sensorsCount
            resetSensors()
        }
        
        for i in 0..<sensorCounts {
            if sensorsValuesCount[i] ?? 0 == 0 {
                continue
            }
            let name = delegate.nameForSensorAtIndex(i)
            print("Min \(name): \(sensorsMinValues[i]!)")
            print("Max \(name): \(sensorsMaxValues[i]!)")
            print("Avg \(name): \(sensorsSumValues[i]! / Double(sensorsValuesCount[i]!))")
            print("---------------------------------")
        }
    }
    
    private func resetSensors() {
        for i in 0..<sensorCounts {
            sensorsSumValues[i] = 0
            sensorsMinValues[i] =  Double.infinity
            sensorsMaxValues[i] = -Double.infinity
            sensorsValuesCount[i] = 0
        }
    }
}
