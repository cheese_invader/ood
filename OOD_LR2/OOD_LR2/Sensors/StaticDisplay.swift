//
//  StaticDisplay.swift
//  OOD_LR2
//
//  Created by Marty on 09/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class StaticDisplay: SensorCalculator, SensorDatasource {
    let sensorNames = ["Temp", "Hum ", "Pres"]
    var sensorsCount = 0
    
    override init() {
        super.init()
        sensorsCount = sensorNames.count
        setDelegate(self)
    }
    
    func nameForSensorAtIndex(_ index: Int) -> String {
        return sensorNames[index]
    }
    
}
