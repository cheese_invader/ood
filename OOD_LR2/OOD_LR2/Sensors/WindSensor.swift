//
//  WindSensor.swift
//  OOD_LR2
//
//  Created by Marty on 09/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Cocoa

class WindSensor: Observer<WeatherInfo> {
    let staticWind = StaticWind()
    
    override func update<T>(_ data: WeatherInfo, initiator: ObservableImpl<T>) {
        print("Station \"\(initiator.identifier)\" has registred data: ")
        if let speed = data.speed {
            staticWind.setNewValue(speed, forSensorAtIndex: 0)
        }
        if let vector = data.vector {
            staticWind.setNewValue(vector.x, forSensorAtIndex: 1)
            staticWind.setNewValue(vector.y, forSensorAtIndex: 2)
        }
        staticWind.printInfo()
        
        var maxAngle = WeatherInfo()
        maxAngle.vector = (staticWind.maxValue(atIndex: 1), staticWind.maxValue(atIndex: 2))
        
        var minAngle = WeatherInfo()
        minAngle.vector = (staticWind.minValue(atIndex: 1), staticWind.minValue(atIndex: 2))
        
        
        print("Max Angle: \(maxAngle.direction!)")
        print("Min Angle: \(minAngle.direction!)")
        var avgAngle = WeatherInfo()
        if abs(staticWind.avgValue(atIndex: 1)) < 0.01 && abs(staticWind.avgValue(atIndex: 2)) < 0.01 {
            print("Avg Angle: nil")
        } else {
            avgAngle.vector = (staticWind.avgValue(atIndex: 1), staticWind.avgValue(atIndex: 2))
            
            print("Avg Angle: \(avgAngle.direction!)")
        }
        
        print("---------------------------------")
        print("---------------------------------")
    }
}
