//
//  DisplaySensor.swift
//  OOD_LR2
//
//  Created by Marty on 09/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class DisplaySensor: Observer<WeatherInfo> {
    override func update<T>(_ data: WeatherInfo, initiator: ObservableImpl<T>) {
        print("Station \"\(initiator.identifier)\" has registred data: ")
        
        if let temperature = data.temperature {
            print("Current Temp: \(temperature)")
        }
        if let humidity = data.humidity {
            print("Current Hum: \(humidity)")
        }
        if let pressure = data.pressure {
            print("Current Pres: \(pressure)")
        }
        print("---------------------------------")
    }
}
