//
//  WeatherData.swift
//  OOD_LR2
//
//  Created by Marty on 09/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class WeatherData: ObservableImpl<WeatherInfo> {
    private var weatherInfo = WeatherInfo()
    
    override var data: WeatherInfo {
        get {
            return weatherInfo
        }
        set {
            weatherInfo = newValue
            notifyObservers()
        }
    }
}
