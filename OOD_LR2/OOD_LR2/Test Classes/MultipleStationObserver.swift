//
//  MultipleStationObserver.swift
//  OOD_LR2
//
//  Created by Marty on 09/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class MultipleStationObserver: Observer<WeatherInfo> {
    var lastStationCall = ""
    
    override func update<T>(_ data: WeatherInfo, initiator: ObservableImpl<T>) {
        lastStationCall = initiator.identifier
    }

}
