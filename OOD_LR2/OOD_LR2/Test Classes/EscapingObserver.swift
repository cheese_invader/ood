//
//  EscapingObserver.swift
//  OOD_LR2
//
//  Created by Marty on 09/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class EscapingObserver: Observer<WeatherInfo> {
    var wd: WeatherData?
    
    override func update<T>(_ data: WeatherInfo, initiator: ObservableImpl<T>) {
        wd?.removeObserver(self)
    }
}
