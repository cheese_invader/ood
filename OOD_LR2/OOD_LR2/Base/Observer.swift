//
//  Observer.swift
//  OOD_LR2
//
//  Created by Marty on 09/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class Observer<ArgType> {
    func update<T>(_ data: ArgType, initiator: ObservableImpl<T>) {
        fatalError("Observer<ArgType> - func update<T>(_ data: ArgType, initiator: ObservableImpl<T>) - Should be overriden")
    }
}

extension Observer: Hashable {
    static func == (lhs: Observer<ArgType>, rhs: Observer<ArgType>) -> Bool {
        return ObjectIdentifier(lhs) == ObjectIdentifier(rhs)
    }
    
    var hashValue: Int {
        return ObjectIdentifier(self).hashValue
    }
}
