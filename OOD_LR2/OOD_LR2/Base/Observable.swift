//
//  Observable.swift
//  OOD_LR2
//
//  Created by Marty on 09/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//


protocol Observable {
    associatedtype ObservableArgType
    var identifier: String { get }
    
    func registerObserver(_ observer: Observer<ObservableArgType>, withPriority priority: Int)
    func notifyObservers()
    func removeObserver(_ observer: Observer<ObservableArgType>)
}
