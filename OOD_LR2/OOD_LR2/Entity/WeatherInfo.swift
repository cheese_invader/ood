//
//  WeatherInfo.swift
//  OOD_LR2
//
//  Created by Marty on 09/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation


struct WeatherInfo {
    var temperature: Double?
    var humidity   : Double?
    var pressure   : Double?
    
    var speed    : Double?
    var direction: Double?
    
    var vector: (x: Double, y: Double)? {
        get {
            guard let direction = direction else {
                return nil
            }
            let x = cos(direction * Double.pi / 180)
            let y = sin(direction * Double.pi / 180)
            return (x, y)
        }
        set {
            guard let newValue = newValue else {
                speed     = nil
                direction = nil
                return
            }
            let hypotenuse = sqrt(newValue.x * newValue.x + newValue.y * newValue.y)
            let multiplier = 1 / hypotenuse
            
            direction = acos(newValue.x * multiplier) * 180 / Double.pi
            
            if asin(newValue.y) < 0 {
                direction = 360 - direction!
            }
        }
    }
    
    init() {}
    
    init(temperature: Double, humidity: Double, pressure: Double) {
        self.temperature = temperature
        self.humidity    = humidity
        self.pressure    = pressure
    }
    
    init(speed: Double, direction: Double) {
        self.speed = speed
        self.direction = direction
    }
}
