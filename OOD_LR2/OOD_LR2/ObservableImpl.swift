//
//  ObservableImpl.swift
//  OOD_LR2
//
//  Created by Marty on 09/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class ObservableImpl<T> {
    private var observers = [Observer<T>: Int]()
    
    var identifier: String
    
    init(identifier: String) {
        self.identifier = identifier
    }
    
    var data: T {
        get {
            fatalError("ObservableImpl<T> - var data: T { get } - Should be overriden")
        }
        set {
            fatalError("ObservableImpl<T> - var data: T { set } - Should be overriden")
        }
    }
}

extension ObservableImpl: Observable {
    typealias ObservableArgType = T
    
    
    func registerObserver(_ observer: Observer<T>, withPriority priority: Int = 10) {
        observers[observer] = priority
    }
    
    func notifyObservers() {
        for observer in observers.keys.sorted(by: { observers[$0]! > observers[$1]! }) {
            observer.update(data, initiator: self)
        }
    }
    
    func removeObserver(_ observer: Observer<T>) {
        observers.removeValue(forKey: observer)
    }
}
