//
//  main.swift
//  OOD_LR2
//
//  Created by Marty on 09/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//


// MARK: - TASK 1 -

//let wd  = WeatherData(identifier: "wd")
//let display = DisplaySensor()
//let statsDisplay = StaticDisplayObserver()
//wd.registerObserver(display)
//wd.registerObserver(statsDisplay)
//wd.data = WeatherInfo(temperature: 10, humidity: 0.8, pressure: 761)
//wd.data = WeatherInfo(temperature: 2, humidity: 0.2, pressure: 759)
//wd.data = WeatherInfo(temperature: 4, humidity: 1.0, pressure: 759)




// MARK: - TASK 3 -

//let wd  = WeatherData(identifier: "wd")
//let display = DisplaySensor()
//let statsDisplay = StaticDisplayObserver()
//wd.registerObserver(display, withPriority: 10)
//wd.registerObserver(statsDisplay, withPriority: 100)
//
//wd.data = WeatherInfo(temperature: 10, humidity: 0.8, pressure: 761)




// MARK: - TASK 4 -

//let wdIn  = WeatherData(identifier: "in")
//let wdOut = WeatherData(identifier: "out")
//
//let display = DisplaySensor()
//wdIn.registerObserver(display)
//wdOut.registerObserver(display)
//
//let statsDisplay = StaticDisplayObserver()
//wdIn.registerObserver(statsDisplay)
//wdOut.registerObserver(statsDisplay)
//
//wdIn.data = WeatherInfo(temperature: 3, humidity: 0.7, pressure: 760)
//wdOut.data = WeatherInfo(temperature: 4, humidity: 0.8, pressure: 761)
//
//wdIn.removeObserver(statsDisplay)
//
//wdIn.data = WeatherInfo(temperature: 10, humidity: 0.8, pressure: 761)
//wdIn.data = WeatherInfo(temperature: -10, humidity: 0.8, pressure: 761)




// MARK: - TASK 5 -

//let wind = WeatherData(identifier: "wind")
//
//let windElement = WindSensor()
//wind.registerObserver(windElement)
//
//wind.data = WeatherInfo(speed: 10, direction: 10)
//wind.data = WeatherInfo(speed: 16, direction: 200)




// MARK: - TASK 6 -

let wdIn    = WeatherData(identifier: "in")
let wdOut   = WeatherData(identifier: "out")

let display = DisplaySensor()
wdIn.registerObserver(display)
wdOut.registerObserver(display)

let windElement = WindSensor()
wdOut.registerObserver(windElement)

let statsDisplay = StaticDisplayObserver()
wdIn.registerObserver(statsDisplay)
wdOut.registerObserver(statsDisplay)

wdIn.data = WeatherInfo(speed: 10, direction: 5)

wdIn.data  = WeatherInfo(temperature: 3, humidity: 0.7, pressure: 760)
wdOut.data = WeatherInfo(temperature: 4, humidity: 0.8, pressure: 761)

wdOut.data = WeatherInfo(speed: 10, direction: 5)
