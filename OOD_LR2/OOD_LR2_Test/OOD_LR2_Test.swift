//
//  OOD_LR2_Test.swift
//  OOD_LR2_Test
//
//  Created by Marty on 09/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import XCTest

class OOD_LR2_Test: XCTestCase {

    func testEscapingObserver() {
        let wd = WeatherData(identifier: "in")
        let a = EscapingObserver()
        a.wd = wd
        wd.notifyObservers()
        XCTAssertTrue(true)
    }
    
    func testDifferentStations() {
        let wdIn    = WeatherData(identifier: "in")
        let wdOut   = WeatherData(identifier: "out")
        let wdThird = WeatherData(identifier: "third")
        
        let mto = MultipleStationObserver()
        
        wdIn.registerObserver(mto)
        wdOut.registerObserver(mto)
        
        wdIn.data = WeatherInfo(temperature: 3, humidity: 0.7, pressure: 760)
        XCTAssertEqual(mto.lastStationCall, "in")
        
        wdOut.data = WeatherInfo(temperature: 4, humidity: 0.8, pressure: 761)
        XCTAssertEqual(mto.lastStationCall, "out")
        
        wdIn.data = WeatherInfo(temperature: 3, humidity: 0.7, pressure: 760)
        XCTAssertEqual(mto.lastStationCall, "in")
        
        // wdThird isn't subscribed
        wdThird.data = WeatherInfo(temperature: 3, humidity: 0.7, pressure: 760)
        XCTAssertEqual(mto.lastStationCall, "in")
    }

    func testWindInfo() {
        var wi = WeatherInfo(speed: 10, direction: 30)
        var a = wi.vector!
        XCTAssertEqual(a.x, 0.866, accuracy: 0.001)
        XCTAssertEqual(a.y, 0.5, accuracy: 0.001)
        wi.vector = a
        XCTAssertEqual(wi.direction!, 30, accuracy: 0.001)
        
        wi = WeatherInfo(speed: 10, direction: 95)
        a = wi.vector!
        wi.vector = a
        XCTAssertEqual(wi.direction!, 95, accuracy: 0.001)
        
        wi = WeatherInfo(speed: 10, direction: 190)
        a = wi.vector!
        wi.vector = a
        XCTAssertEqual(wi.direction!, 190, accuracy: 0.001)
        
        wi = WeatherInfo(speed: 10, direction: 290)
        a = wi.vector!
        wi.vector = a
        XCTAssertEqual(wi.direction!, 290, accuracy: 0.001)
    }
    
    func testAvgAngel() {
        let wind = WeatherData(identifier: "first")
        
        let windElement = WindSensor()
        wind.registerObserver(windElement)
        
        wind.data = WeatherInfo(speed: 10, direction: 20)
        wind.data = WeatherInfo(speed: 16, direction: 180)
        
        var avgAngle = WeatherInfo()
        avgAngle.vector = (windElement.staticWind.avgValue(atIndex: 1), windElement.staticWind.avgValue(atIndex: 2))
        
        XCTAssertEqual(avgAngle.direction!, 100, accuracy: 1)
    }
}
