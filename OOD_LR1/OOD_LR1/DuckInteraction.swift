//
//  DuckInteraction.swift
//  OOD_LR1
//
//  Created by Marty on 04/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

func drawDuck(_ duck: Duck) {
    duck.display()
}

func playWithDuck(_ duck: Duck) {
    drawDuck(duck)
    duck.quack()
    duck.fly()
    duck.dance()
    print()
}
