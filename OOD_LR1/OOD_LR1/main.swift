//
//  main.swift
//  OOD_LR1
//
//  Created by Marty on 04/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

let mallardDuck = MallardDuck()
playWithDuck(mallardDuck)
playWithDuck(mallardDuck)
playWithDuck(mallardDuck)

let redheadDuck = RedheadDuck()
playWithDuck(redheadDuck)

let rubberDuck = RubberDuck()
playWithDuck(rubberDuck)

let decoyDuck = DecoyDuck()
playWithDuck(decoyDuck)

let modelDuck = ModelDuck()
playWithDuck(modelDuck)
playWithDuck(modelDuck)

