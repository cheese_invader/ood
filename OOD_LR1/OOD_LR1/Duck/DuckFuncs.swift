//
//  DuckFuncs.swift
//  OOD_LR1
//
//  Created by Marty on 04/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

func danceWaltz() {
    print("I'm dancing waltz")
}

func danceMinuet() {
    print("I'm dancing minuet")
}

func quackQuack() {
    print("Quack Quack!!!")
}

func squeek() {
    print("Squeek!!!")
}

func makeFlyWithWings() -> (() -> ()) {
    var flyCount = 0
    return {
        flyCount += 1
        print("It's my \(flyCount) flying")
    }
}
