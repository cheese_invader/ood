//
//  DecoyDuck.swift
//  OOD_LR1
//
//  Created by Marty on 04/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class DecoyDuck: Duck {
    init() {
        super.init(quackHandler: quackQuack)
    }
    
    override func display() {
        print("I'm decoy duck")
    }
}
