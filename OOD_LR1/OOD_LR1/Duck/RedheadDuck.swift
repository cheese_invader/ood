//
//  RedheadDuck.swift
//  OOD_LR1
//
//  Created by Marty on 04/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class RedheadDuck: Duck {
    private var flyCount = 0
    
    init() {
        super.init(quackHandler: quackQuack, flyHandler: makeFlyWithWings(), danceHandler: danceMinuet)
    }
    
    override func display() {
        print("I'm redhead duck")
    }
}
