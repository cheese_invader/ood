//
//  Duck.swift
//  OOD_LR1
//
//  Created by Marty on 04/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class Duck {
    private var danceHandler: (() -> ())?
    private var quackHandler: (() -> ())?
    private var flyHandler  : (() -> ())?
    
    
    init(quackHandler: (() -> ())? = nil, flyHandler: (() -> ())? = nil, danceHandler: (() -> ())? = nil) {
        self.quackHandler = quackHandler
        self.danceHandler = danceHandler
        self.flyHandler   = flyHandler
    }
    
    
    // MARK: - Action -
    func quack() {
        quackHandler?()
    }
    
    func fly() {
        flyHandler?()
    }
    
    func dance() {
        danceHandler?()
    }
    
    func display() {
        fatalError("Duck - display() - should be overriden")
    }
    
    func swim() {
        print("I'm swimming")
    }
}
