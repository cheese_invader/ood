//
//  ModelDuck.swift
//  OOD_LR1
//
//  Created by Marty on 04/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class ModelDuck: Duck {
    init() {
        super.init()
    }
    
    override func display() {
        print("I'm model duck")
    }
}
