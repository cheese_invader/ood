//
//  RubberDuck.swift
//  OOD_LR1
//
//  Created by Marty on 04/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class RubberDuck: Duck {
    init() {
        super.init(quackHandler: squeek)
    }
    
    override func display() {
        print("I'm rubber duck")
    }
}
