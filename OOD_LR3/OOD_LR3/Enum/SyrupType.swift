//
//  SyrupType.swift
//  OOD_LR3
//
//  Created by Marty on 22/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

enum SyrupType: String {
    case Chocolate
    case Maple
}
