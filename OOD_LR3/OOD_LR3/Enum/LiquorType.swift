//
//  LiquorType.swift
//  OOD_LR3
//
//  Created by Marty on 24/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

enum LiquorType: String {
    case Chocolate
    case Nut
}
