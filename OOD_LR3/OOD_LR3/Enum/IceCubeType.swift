//
//  IceCubeType.swift
//  OOD_LR3
//
//  Created by Marty on 22/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

private let waterIceCubeCost = 5.0
private let   dryIceCubeCost = 10.0


enum IceCubeType: String {
    case Dry
    case Water
    
    var cost: Double {
        switch self {
        case .Dry:
            return dryIceCubeCost
        case .Water:
            return waterIceCubeCost
        }
    }
}
