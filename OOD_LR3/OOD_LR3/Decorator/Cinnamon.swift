//
//  Cinnamon.swift
//  OOD_LR3
//
//  Created by Marty on 22/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

private let productCost = 20.0


class Cinnamon: CondimentDecorator {
    override var cost: Double {
        return super.cost + productCost
    }
    
    override var description: String {
        return super.description + ", Cinnamon"
    }
}
