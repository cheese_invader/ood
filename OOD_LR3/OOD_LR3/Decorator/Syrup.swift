//
//  Syrup.swift
//  OOD_LR3
//
//  Created by Marty on 22/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

private let productCost = 15.0


class Syrup: CondimentDecorator {
    private let type: SyrupType
    
    init(beverage: BeverageProtocol, type: SyrupType = .Chocolate) {
        self.type = type
        super.init(beverage: beverage)
    }
    
    override var cost: Double {
        return super.cost + productCost
    }
    
    override var description: String {
        return super.description + ", \(type.rawValue) syrup"
    }
}
