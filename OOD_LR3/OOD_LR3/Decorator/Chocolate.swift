//
//  Chocolate.swift
//  OOD_LR3
//
//  Created by Marty on 24/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

private let defaultSlices = 1
private let minSlices     = 1
private let maxSlices     = 5

private let productCost = 10.0


class Chocolate: CondimentDecorator {
    private let slices: Int
    
    init(beverage: BeverageProtocol, slices: Int = defaultSlices) {
        self.slices = (slices >= minSlices && slices <= maxSlices) ? slices : defaultSlices
        super.init(beverage: beverage)
    }
    
    override var cost: Double {
        return super.cost + Double(slices) * productCost
    }
    
    override var description: String {
        return super.description + ", Chocolate x \(slices) slices"
    }
}
