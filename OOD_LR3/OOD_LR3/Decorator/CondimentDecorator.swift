//
//  CondimentDecorator.swift
//  OOD_LR3
//
//  Created by Marty on 22/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class CondimentDecorator: BeverageProtocol {
    private var beverage: BeverageProtocol
    
    init(beverage: BeverageProtocol) {
        self.beverage = beverage
    }
    
    
    // MARK: - BeverageProtocol -
    var description: String {
        return beverage.description
    }
    
    var cost: Double {
        return beverage.cost
    }
}

