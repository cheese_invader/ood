//
//  Liquor.swift
//  OOD_LR3
//
//  Created by Marty on 24/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

private let productCost = 50.0


class Liquor: CondimentDecorator {
    private let type: LiquorType
    
    init(beverage: BeverageProtocol, type: LiquorType = .Chocolate) {
        self.type = type
        super.init(beverage: beverage)
    }
    
    override var cost: Double {
        return super.cost + productCost
    }
    
    override var description: String {
        return super.description + ", \(type.rawValue) liquor"
    }
}
