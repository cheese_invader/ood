//
//  Lemon.swift
//  OOD_LR3
//
//  Created by Marty on 22/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

private let defaultQuantity = 1
private let minQuantity     = 1
private let maxQuantity     = 20

private let productCost = 10.0


class Lemon: CondimentDecorator {
    private let quantity: Int
    
    init(beverage: BeverageProtocol, quantity: Int = defaultQuantity) {
        self.quantity = (quantity >= minQuantity && quantity <= maxQuantity) ? quantity : defaultQuantity
        super.init(beverage: beverage)
    }
    
    override var cost: Double {
        return super.cost + productCost * Double(quantity)
    }
    
    override var description: String {
        return super.description + ", Lemon x \(quantity)"
    }
}
