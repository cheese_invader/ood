//
//  Cream.swift
//  OOD_LR3
//
//  Created by Marty on 24/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

private let productCost = 25.0


class Cream: CondimentDecorator {
    override var cost: Double {
        return super.cost + productCost
    }
    
    override var description: String {
        return super.description + ", Cream"
    }
}
