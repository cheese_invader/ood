//
//  CoconutFlakes.swift
//  OOD_LR3
//
//  Created by Marty on 22/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

private let defaultMass = 10
private let minMass     = 1
private let maxMass     = 1000

private let productCost = 1.0


class CoconutFlakes: CondimentDecorator {
    private let mass: Int
    
    init(beverage: BeverageProtocol, mass: Int = defaultMass) {
        self.mass = (mass >= minMass && mass <= maxMass) ? mass : defaultMass
        super.init(beverage: beverage)
    }
    
    override var cost: Double {
        return super.cost + Double(mass) * productCost
    }
    
    override var description: String {
        return super.description + ", Coconut flakes \(mass) g"
    }
}
