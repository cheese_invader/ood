//
//  IceCubes.swift
//  OOD_LR3
//
//  Created by Marty on 22/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

private let defaultQuantity = 3
private let minQuantity     = 1
private let maxQuantity     = 20


class IceCubes: CondimentDecorator {
    private let quantity: Int
    private let type    : IceCubeType
    
    init(beverage: BeverageProtocol, quantity: Int = defaultQuantity, type: IceCubeType = .Water) {
        self.quantity = (quantity >= minQuantity && quantity <= maxQuantity) ? quantity : defaultQuantity
        self.type     = type
        super.init(beverage: beverage)
    }
    
    override var cost: Double {
        return super.cost + type.cost * Double(quantity)
    }
    
    override var description: String {
        return super.description + ", \(type.rawValue) ice cubes x \(quantity)"
    }
}
