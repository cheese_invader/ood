//
//  SmallMilkshake.swift
//  OOD_LR3
//
//  Created by Marty on 24/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

private let productCost = 50.0


class SmallMilkshake: Milkshake {
    override var description: String {
        return "Small milkshake"
    }
    
    override var cost: Double {
        return productCost
    }
}
