//
//  LargeMilkshake.swift
//  OOD_LR3
//
//  Created by Marty on 24/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

private let productCost = 80.0


class LargeMilkshake: Milkshake {
    override var description: String {
        return "Large milkshake"
    }
    
    override var cost: Double {
        return productCost
    }
}
