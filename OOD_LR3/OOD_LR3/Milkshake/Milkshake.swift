//
//  Milkshake.swift
//  OOD_LR3
//
//  Created by Marty on 22/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

private let productCost = 80.0


class Milkshake: BeverageProtocol {
    var description: String {
        return "Milkshake"
    }
    
    var cost: Double {
        return productCost
    }
}
