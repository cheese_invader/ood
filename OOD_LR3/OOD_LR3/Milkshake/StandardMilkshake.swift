//
//  StandardMilkshake.swift
//  OOD_LR3
//
//  Created by Marty on 24/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

private let productCost = 60.0


class StandardMilkshake: Milkshake {
    override var description: String {
        return "Standard milkshake"
    }
    
    override var cost: Double {
        return productCost
    }
}
