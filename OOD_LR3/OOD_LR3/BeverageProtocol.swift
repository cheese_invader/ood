//
//  BeverageProtocol.swift
//  OOD_LR3
//
//  Created by Marty on 22/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol BeverageProtocol {
    var description: String { get }
    var cost       : Double { get }
}
