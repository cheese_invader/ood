//
//  Capuccino.swift
//  OOD_LR3
//
//  Created by Marty on 22/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

private let productCost = 80.0


class Capuccino: Coffee {
    override var description: String {
        return "Capuccino"
    }
    
    override var cost: Double {
        return productCost
    }
}
