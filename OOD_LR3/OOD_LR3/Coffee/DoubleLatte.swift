//
//  DoubleLatte.swift
//  OOD_LR3
//
//  Created by Marty on 24/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

private let productCost = 130.0


class DoubleLatte: Coffee {
    override var description: String {
        return "Double latte"
    }
    
    override var cost: Double {
        return productCost
    }
}
