//
//  Coffee.swift
//  OOD_LR3
//
//  Created by Marty on 22/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

private let productCost = 60.0


class Coffee: BeverageProtocol {
    var description: String {
        return "Coffee"
    }
    
    var cost: Double {
        return productCost
    }
}
