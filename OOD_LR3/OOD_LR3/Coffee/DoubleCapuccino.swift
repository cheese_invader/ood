//
//  DoubleCapuccino.swift
//  OOD_LR3
//
//  Created by Marty on 24/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

private let productCost = 120.0


class DoubleCapuccino: Coffee {
    override var description: String {
        return "Double capuccino"
    }
    
    override var cost: Double {
        return productCost
    }
}
