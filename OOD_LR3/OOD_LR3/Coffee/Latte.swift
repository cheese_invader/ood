//
//  Latte.swift
//  OOD_LR3
//
//  Created by Marty on 22/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

private let productCost = 90.0


class Latte: Coffee {
    override var description: String {
        return "Latte"
    }
    
    override var cost: Double {
        return productCost
    }
}
