//
//  Tea.swift
//  OOD_LR3
//
//  Created by Marty on 22/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

private let productCost = 30.0


class Tea: BeverageProtocol {
    var description: String {
        return "Tea"
    }
    
    var cost: Double {
        return productCost
    }
}
