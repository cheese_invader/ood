//
//  WhiteTea.swift
//  OOD_LR3
//
//  Created by Marty on 24/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class WhiteTea: Tea {
    override var description: String {
        return "White tea"
    }
}
