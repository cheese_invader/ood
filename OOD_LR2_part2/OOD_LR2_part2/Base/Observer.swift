//
//  Observer.swift
//  OOD_LR2_part2
//
//  Created by Marty on 17/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

@objc protocol Observer {
    func update(_ notification: Notification)
}
