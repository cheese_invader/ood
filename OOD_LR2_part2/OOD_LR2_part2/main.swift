//
//  main.swift
//  OOD_LR2_part2
//
//  Created by Marty on 17/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

let displaySensor = DisplaySensor()

let wdIn = WeatherData(forEvent: Notification.Name.gotDataFromObserverIn)

wdIn.registerObserver(displaySensor)

wdIn.data = WeatherInfo(temperature: 10, humidity: 20, pressure: 30)

