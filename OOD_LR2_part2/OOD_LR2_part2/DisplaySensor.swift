//
//  DisplaySensor.swift
//  OOD_LR2_part2
//
//  Created by Marty on 17/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

class DisplaySensor: Observer {
    
    func update(_ notification: Notification) {
        guard
            let info       = notification.userInfo?["info"] as? WeatherInfo,
            let observerId = notification.userInfo?["id"]   as? String
        else {
            return
        }
        
        
        printInfo(info, forDisplayName: observerId)
        // Should be like this, but it doesn't work
//        DispatchQueue.main.async { [weak self] in
//            guard let self = self else {
//                return
//            }
//            self.printInfo(info, forDisplayName: observerId)
//        }
    }
    
    deinit {
        print("Nooo")
    }
    
    private func printInfo(_ info: WeatherInfo, forDisplayName name: String) {
        print("Station \"\(name)\" has registred data: ")
        
        if let temperature = info.temperature {
            print("Current Temp: \(temperature)")
        }
        if let humidity = info.humidity {
            print("Current Hum : \(humidity)")
        }
        if let pressure = info.pressure {
            print("Current Pres: \(pressure)")
        }
        print("---------------------------------")
    }
}
