//
//  NotificationName.swift
//  OOD_LR2_part2
//
//  Created by Marty on 17/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let gotDataFromObserverIn  = Notification.Name("OOD_LR2_part2.gotDataFromObserverIn")
    static let gotDataFromObserverOut = Notification.Name("OOD_LR2_part2.gotDataFromObserverOut")
}
