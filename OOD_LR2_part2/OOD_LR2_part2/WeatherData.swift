//
//  WeatherData.swift
//  OOD_LR2_part2
//
//  Created by Marty on 17/02/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation

class WeatherData {
    private let event: Notification.Name
    var data = WeatherInfo() {
        didSet {
            self.notifyObservers()
        }
    }
    
    init(forEvent event: Notification.Name) {
        self.event = event
    }
    
    func registerObserver(_ observer: Observer) {
        NotificationCenter.default.addObserver(observer, selector: #selector(observer.update(_:)), name: event, object: nil)
    }
    
    func removeObserver(_ observer: Observer) {
        NotificationCenter.default.removeObserver(self, name: event, object: nil)
    }
    
    func notifyObservers() {
        NotificationCenter.default.post(name: event, object: nil, userInfo: ["info": data, "id": event.rawValue])
    }
}
