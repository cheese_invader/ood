//
//  OOD_LR5_Command.swift
//  OOD_LR5Tests
//
//  Created by Marty on 24/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import XCTest
@testable import OOD_LR5

class OOD_LR5_Command: XCTestCase {
    // MARK: - InsertParagraph -
    func testInsertParagraphCommand() {
        let document = Document()
        let paragraph = Paragraph()
        paragraph.text = "Paragraph"
        
        let insertParagraphCommand: CommandProtocol = InsertParagraphCommand(withParagraph: paragraph, atPosition: 0, inDocument: document)
        
        XCTAssertNoThrow(try insertParagraphCommand.execute())
        XCTAssertEqual(document.itemsCount, 1)
        XCTAssertEqual(document.getItemAtIndex(0).paragraph!.text, "Paragraph")
    }
    
    
    func testInsertAtTheEndOfDocument() {
        let document = Document()
        let paragraph = Paragraph()
        paragraph.text = "Paragraph"
        
        try! InsertParagraphCommand(withParagraph: paragraph, atPosition: 0, inDocument: document).execute()
        
        let secondParagraph = Paragraph()
        secondParagraph.text = "SecondParagraph"
        
        let insertParagraphCommand: CommandProtocol = InsertParagraphCommand(withParagraph: secondParagraph, inDocument: document)
        
        XCTAssertNoThrow(try insertParagraphCommand.execute())
        XCTAssertEqual(document.itemsCount, 2)
        XCTAssertEqual(document.getItemAtIndex(1).paragraph!.text, "SecondParagraph")
    }
    
    
    func testCantInsertParagraphInWrongPosition() {
        let document = Document()
        let paragraph = Paragraph()
        paragraph.text = "Paragraph"
        
        try! InsertParagraphCommand(withParagraph: paragraph, atPosition: 0, inDocument: document).execute()
        
        let secondParagraph = Paragraph()
        secondParagraph.text = "SecondParagraph"
        
        var insertParagraphCommand: CommandProtocol = InsertParagraphCommand(withParagraph: secondParagraph, atPosition: 2, inDocument: document)
        XCTAssertThrowsError(try insertParagraphCommand.execute())
        
        insertParagraphCommand = InsertParagraphCommand(withParagraph: secondParagraph, atPosition: -1, inDocument: document)
        XCTAssertThrowsError(try insertParagraphCommand.execute())
    }
    
    
    // MARK: - InsertImage -
    func testInsertImage() {
        let document = Document()
        let image = Image(withImage: UIImage(named: "Notes")!, name: "Notes")
        image.width = 300
        image.height = 200
        
        let insertImageCommand: CommandProtocol = InsertImageCommand(withImage: image, andPosition: 0, inDocument: document)
        
        XCTAssertNoThrow(try insertImageCommand.execute())
        XCTAssertEqual(document.itemsCount, 1)
        XCTAssertEqual(document.getItemAtIndex(0).image!.name, "Notes")
        XCTAssertEqual(document.getItemAtIndex(0).image!.image, UIImage(named: "Notes")!)
    }
    
    
    func testInsertImageAtTheEndOfDocument() {
        let document = Document()
        let image = Image(withImage: UIImage(named: "Notes")!, name: "Notes")
        image.width = 300
        image.height = 200
        
        try! InsertImageCommand(withImage: image, andPosition: 0, inDocument: document).execute()
        
        let insertImageCommand: CommandProtocol = InsertImageCommand(withImage: image, inDocument: document)
        
        XCTAssertNoThrow(try insertImageCommand.execute())
        XCTAssertEqual(document.itemsCount, 2)
        XCTAssertEqual(document.getItemAtIndex(1).image!.name, "Notes")
        XCTAssertEqual(document.getItemAtIndex(1).image!.image, UIImage(named: "Notes")!)
    }
    
    
    func testCantInsertImageInWrongPosition() {
        let document = Document()
        let image = Image(withImage: UIImage(named: "Notes")!, name: "Notes")
        image.width = 300
        image.height = 200
        
        try! InsertImageCommand(withImage: image, andPosition: 0, inDocument: document).execute()
        
        var insertImageCommand: CommandProtocol = InsertImageCommand(withImage: image, andPosition: 2, inDocument: document)
        XCTAssertThrowsError(try insertImageCommand.execute())
        
        insertImageCommand = InsertImageCommand(withImage: image, andPosition: -1, inDocument: document)
        XCTAssertThrowsError(try insertImageCommand.execute())
    }
    
    
    // MARK: - DeleteItemCommand -
    func testCanDeleteParagraph() {
        let document = Document()
        var paragraph = Paragraph()
        paragraph.text = "Paragraph"
        try! InsertParagraphCommand(withParagraph: paragraph, atPosition: 0, inDocument: document).execute()
        
        paragraph = Paragraph()
        paragraph.text = "SecondParagraph"
        try! InsertParagraphCommand(withParagraph: paragraph, atPosition: 1, inDocument: document).execute()
        
 
        var deleteItemCommand: CommandProtocol = DeleteItemCommand(withPosition: 0, inDocument: document)
        XCTAssertNoThrow(try deleteItemCommand.execute())
        XCTAssertEqual(document.itemsCount, 1)
        XCTAssertEqual(document.getItemAtIndex(0).paragraph!.text, "SecondParagraph")
        XCTAssertNoThrow(try deleteItemCommand.unexecute())
        XCTAssertEqual(document.itemsCount, 2)
        XCTAssertEqual(document.getItemAtIndex(0).paragraph!.text, "Paragraph")
        XCTAssertEqual(document.getItemAtIndex(1).paragraph!.text, "SecondParagraph")
        
        deleteItemCommand = DeleteItemCommand(withPosition: 1, inDocument: document)
        XCTAssertNoThrow(try deleteItemCommand.execute())
        XCTAssertEqual(document.itemsCount, 1)
        XCTAssertEqual(document.getItemAtIndex(0).paragraph!.text, "Paragraph")
        
        deleteItemCommand = DeleteItemCommand(withPosition: 0, inDocument: document)
        XCTAssertNoThrow(try deleteItemCommand.execute())
        XCTAssertEqual(document.itemsCount, 0)
    }
    
    
    func testCanDeleteImage() {
        let document = Document()
        let paragraph = Paragraph()
        paragraph.text = "Paragraph"
        try! InsertParagraphCommand(withParagraph: paragraph, inDocument: document).execute()
        
        let image = Image(withImage: UIImage(named: "Notes")!, name: "Notes")
        image.width = 300
        image.height = 200
        
        try! InsertImageCommand(withImage: image, inDocument: document).execute()
        
        let deleteItemCommand: CommandProtocol = DeleteItemCommand(withPosition: 1, inDocument: document)
        XCTAssertNoThrow(try deleteItemCommand.execute())
        XCTAssertEqual(document.itemsCount, 1)
        XCTAssertEqual(document.getItemAtIndex(0).paragraph!.text, "Paragraph")
    }
    
    
    func testCantDeleteItemWithWrongPosition() {
        let document = Document()
        let paragraph = Paragraph()
        paragraph.text = "Paragraph"
        try! InsertParagraphCommand(withParagraph: paragraph, inDocument: document).execute()
        
        var deleteItemCommand: CommandProtocol = DeleteItemCommand(withPosition: 2, inDocument: document)
        XCTAssertThrowsError(try deleteItemCommand.execute())
        
        deleteItemCommand = DeleteItemCommand(withPosition: -1, inDocument: document)
        XCTAssertThrowsError(try deleteItemCommand.execute())
    }
    
    
    // MARK: - ResizeItemCommand -
    func testResizeImage() {
        let document = Document()
        let image = Image(withImage: UIImage(named: "Notes")!, name: "Notes")
        image.width = 300
        image.height = 200
        
        try! InsertImageCommand(withImage: image, inDocument: document).execute()
        
        let resizeImageCommand: CommandProtocol = ResizeImageCommand(withPosition: 0, width: 50, height: 100, inDocument: document)
        XCTAssertNoThrow(try resizeImageCommand.execute())
        let resizedImage = document.getItemAtIndex(0).image!
        
        XCTAssertEqual(resizedImage.width, 50)
        XCTAssertEqual(resizedImage.height, 100)
        XCTAssertEqual(resizedImage.name, "Notes")
    }
    
    
    func testCantResizeParagraph() {
        let document = Document()
        let paragraph = Paragraph()
        paragraph.text = "Paragraph"
        
        try! InsertParagraphCommand(withParagraph: paragraph, inDocument: document).execute()
        let resizeImageCommand: CommandProtocol = ResizeImageCommand(withPosition: 0, width: 50, height: 100, inDocument: document)
        XCTAssertThrowsError(try resizeImageCommand.execute())
    }
    
    
    func testCantResizeItemWithWrongPosition() {
        let document = Document()
        let paragraph = Paragraph()
        paragraph.text = "Paragraph"
        
        try! InsertParagraphCommand(withParagraph: paragraph, inDocument: document).execute()
        var resizeImageCommand: CommandProtocol = ResizeImageCommand(withPosition: 2, width: 50, height: 100, inDocument: document)
        XCTAssertThrowsError(try resizeImageCommand.execute())
        
        resizeImageCommand = ResizeImageCommand(withPosition: -1, width: 50, height: 100, inDocument: document)
        XCTAssertThrowsError(try resizeImageCommand.execute())
    }
    
    
    // MARK: - setParagraph -
    func testSetTitle() {
        let document = Document()
        let setTitleCommand: CommandProtocol = SetTitleCommand(withTitle: "Title", inDocument: document)
        
        XCTAssertNoThrow(try setTitleCommand.execute())
        XCTAssertEqual(document.title, "Title")
    }
    
    
    func testCanChangeBackTitle() {
        let document = Document()
        let setTitleCommand: CommandProtocol = SetTitleCommand(withTitle: "Title", inDocument: document)
        
        XCTAssertNoThrow(try setTitleCommand.execute())
        XCTAssertEqual(document.title, "Title")
        
        XCTAssertNoThrow(try setTitleCommand.unexecute())
        XCTAssertEqual(document.title, "")
    }
    
    
    // MARK: - ReplaceText -
    func testReplaceText() {
        let document = Document()
        let paragraph = Paragraph()
        paragraph.text = "Paragraph"
        
        let replaceParagraph = Paragraph()
        replaceParagraph.text = "Replace Paragraph"
        
        try! InsertParagraphCommand(withParagraph: paragraph, inDocument: document).execute()
        
        let replaceTextCommand: CommandProtocol = ReplaceTextCommand(withParagraph: replaceParagraph, position: 0, inDocument: document)
        XCTAssertNoThrow(try replaceTextCommand.execute())
        XCTAssertEqual(document.itemsCount, 1)
        XCTAssertEqual(document.getItemAtIndex(0).paragraph!.text, "Replace Paragraph")
    }
    
    
    func testCantReplaceTextToImage() {
        let document = Document()
        let image = Image(withImage: UIImage(named: "Notes")!, name: "Notes")
        image.width = 300
        image.height = 200
        
        let replaceParagraph = Paragraph()
        replaceParagraph.text = "Replace Paragraph"
        
        try! InsertImageCommand(withImage: image, inDocument: document).execute()
        
        let replaceTextCommand: CommandProtocol = ReplaceTextCommand(withParagraph: replaceParagraph, position: 0, inDocument: document)
        XCTAssertThrowsError(try replaceTextCommand.execute())
    }
    
    
    func testCantReplaceTextToWrongPosition() {
        let document = Document()
        let paragraph = Paragraph()
        paragraph.text = "Paragraph"
        
        let replaceParagraph = Paragraph()
        replaceParagraph.text = "Replace Paragraph"
        
        try! InsertParagraphCommand(withParagraph: paragraph, inDocument: document).execute()
        
        var replaceTextCommand: CommandProtocol = ReplaceTextCommand(withParagraph: replaceParagraph, position: 1, inDocument: document)
        XCTAssertThrowsError(try replaceTextCommand.execute())
        
        replaceTextCommand = ReplaceTextCommand(withParagraph: replaceParagraph, position: -1, inDocument: document)
        XCTAssertThrowsError(try replaceTextCommand.execute())
    }
    
    
    func testCanUnexecuteReplaceText() {
        let document = Document()
        let paragraph = Paragraph()
        paragraph.text = "Paragraph"
        
        let replaceParagraph = Paragraph()
        replaceParagraph.text = "Replace Paragraph"
        
        try! InsertParagraphCommand(withParagraph: paragraph, inDocument: document).execute()
        
        let replaceTextCommand: CommandProtocol = ReplaceTextCommand(withParagraph: replaceParagraph, position: 0, inDocument: document)
        XCTAssertNoThrow(try replaceTextCommand.execute())
        XCTAssertEqual(document.itemsCount, 1)
        XCTAssertEqual(document.getItemAtIndex(0).paragraph!.text, "Replace Paragraph")
        
        XCTAssertNoThrow(try replaceTextCommand.unexecute())
        XCTAssertEqual(document.itemsCount, 1)
        XCTAssertEqual(document.getItemAtIndex(0).paragraph!.text, "Paragraph")
    }
    
    
    func testUndoSetTitleAfterTwoParagraph() {
        let document = Document()
        
        let paragraph = Paragraph()
        paragraph.text = "Paragraph"
        
        let secondParagraph = Paragraph()
        secondParagraph.text = "SecondParagraph"
        
        let insertParagraph1 = InsertParagraphCommand(withParagraph: paragraph, inDocument: document)
        let insertParagraph2 = InsertParagraphCommand(withParagraph: secondParagraph, inDocument: document)
        let setTitle = SetTitleCommand(withTitle: "Title", inDocument: document)
        
        try! insertParagraph1.execute()
        try! insertParagraph2.execute()
        try! setTitle.execute()
        
        XCTAssertEqual(document.itemsCount, 2)
        XCTAssertEqual(document.title, "Title")
        
        XCTAssertEqual(document.getItemAtIndex(0).paragraph!.text, "Paragraph")
        XCTAssertEqual(document.getItemAtIndex(1).paragraph!.text, "SecondParagraph")
        
        try! setTitle.unexecute()
        XCTAssertEqual(document.itemsCount, 2)
        XCTAssertEqual(document.title, "")
        
        XCTAssertEqual(document.getItemAtIndex(0).paragraph!.text, "Paragraph")
        XCTAssertEqual(document.getItemAtIndex(1).paragraph!.text, "SecondParagraph")
    }
    
    
    func testUndoSetParagraphAfterSetTitle() {
        let document = Document()
        
        let paragraph = Paragraph()
        paragraph.text = "Paragraph"
        
        let setTitle = SetTitleCommand(withTitle: "Title", inDocument: document)
        let insertParagraph = InsertParagraphCommand(withParagraph: paragraph, inDocument: document)
        
        try! setTitle.execute()
        try! insertParagraph.execute()
        
        XCTAssertEqual(document.itemsCount, 1)
        XCTAssertEqual(document.title, "Title")
        XCTAssertEqual(document.getItemAtIndex(0).paragraph!.text, "Paragraph")
        
        try! insertParagraph.unexecute()
        XCTAssertEqual(document.itemsCount, 0)
        XCTAssertEqual(document.title, "Title")
        
        try! setTitle.unexecute()
        XCTAssertEqual(document.itemsCount, 0)
        XCTAssertEqual(document.title, "")
    }
}
