//
//  OOD_LR5_Interactor.swift
//  OOD_LR5Tests
//
//  Created by Marty on 24/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import XCTest
@testable import OOD_LR5

class OOD_LR5_Interactor: XCTestCase {
    func testCantUndoEmptyDocument() {
        let presenter = EditorPresenter()
        let interactor = EditorInteractor(presenter: presenter)
        
        XCTAssertFalse(interactor.canUndo)
    }
    
    func testCantRedoEmptyDocument() {
        let presenter = EditorPresenter()
        let interactor = EditorInteractor(presenter: presenter)
        
        XCTAssertFalse(interactor.canRedo)
    }
    
    func testCanUndoOnlyOnceWithOneCommand() {
        let presenter = EditorPresenter()
        let interactor = EditorInteractor(presenter: presenter)
        
        interactor.performCommand("InsertParagraph end hello world")
        XCTAssertTrue(interactor.canUndo)
        interactor.undo()
        XCTAssertFalse(interactor.canUndo)
        XCTAssertTrue(interactor.canRedo)
    }
    
    func testCantRedoCommandAfterPerformCommand() {
        let presenter = EditorPresenter()
        let interactor = EditorInteractor(presenter: presenter)
        
        interactor.performCommand("InsertParagraph end hello world")
        interactor.performCommand("InsertParagraph end hello world")
        interactor.undo()
        interactor.undo()
        interactor.performCommand("InsertParagraph end hello world")
        XCTAssertFalse(interactor.canRedo)
        
        interactor.undo()
        XCTAssertTrue(interactor.canRedo)
        
        interactor.redo()
        XCTAssertFalse(interactor.canRedo)
    }
    
    func testDeepOfUndo() {
        let presenter = EditorPresenter()
        let interactor = EditorInteractor(presenter: presenter)
        
        for _ in 0..<20 {
            interactor.performCommand("InsertParagraph end hello world")
        }
        
        for _ in 0..<10 {
            XCTAssertTrue(interactor.canUndo)
            interactor.undo()
        }
        XCTAssertFalse(interactor.canUndo)
    }
    
    func testDeepOfUndoIsEqualToDeepOfRedo() {
        let presenter = EditorPresenter()
        let interactor = EditorInteractor(presenter: presenter)
        
        for _ in 0..<20 {
            interactor.performCommand("InsertParagraph end hello world")
        }
        
        for _ in 0..<10 {
            XCTAssertTrue(interactor.canUndo)
            interactor.undo()
        }
        XCTAssertFalse(interactor.canUndo)
        
        for _ in 0..<10 {
            XCTAssertTrue(interactor.canRedo)
            interactor.redo()
        }
        XCTAssertFalse(interactor.canRedo)
    }
    
    func testCantUnduAfterWrongCommand() {
        let presenter = EditorPresenter()
        let interactor = EditorInteractor(presenter: presenter)
        
        interactor.performCommand("InsertParagrah end hello world")
        XCTAssertFalse(interactor.canUndo)
        XCTAssertFalse(interactor.canRedo)
    }
}

