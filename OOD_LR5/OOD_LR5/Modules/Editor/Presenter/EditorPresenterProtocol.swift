//
//  EditorPresenterProtocol.swift
//  OOD_LR5
//
//  Created by Marty on 15/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol EditorPresenterProtocol: class {
    // View ->
    func userEnteredCommand(_ command: String)
    func userTappedUndoButton()
    func userTappedRedoButton()
    func userTappedListButton()
    func userTappedHelpButton()
    func userTappedSaveButton()
    
    // Interactor, View ->
    func reloadData()
    
    // Interactor ->
    func printItemList(_ list: String)
    func printHelpInfo(_ info: String)
    func printMessage(_ message: String)
}
