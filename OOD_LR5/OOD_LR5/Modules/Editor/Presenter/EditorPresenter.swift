//
//  EditorPresenter.swift
//  OOD_LR5
//
//  Created by Marty on 15/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class EditorPresenter: EditorPresenterProtocol {
    // MARK: - VIPER -
    private weak var view : EditorViewControllerProtocol?
    private var interactor: EditorInteractorProtocol?
    private var router    : EditorRouterProtocol?
    
    func setup(view: EditorViewControllerProtocol, router: EditorRouterProtocol, interactor: EditorInteractorProtocol) {
        self.view       = view
        self.router     = router
        self.interactor = interactor
    }
    
    
    // MARK: - EditorPresenterProtocol -
    func userEnteredCommand(_ command: String) {
        interactor?.performCommand(command)
    }
    
    func userTappedUndoButton() {
        interactor?.undo()
        reloadData()
    }
    
    func userTappedRedoButton() {
        interactor?.redo()
        reloadData()
    }
    
    func userTappedListButton() {
        interactor?.list()
    }
    
    func userTappedHelpButton() {
        interactor?.help()
    }
    
    func userTappedSaveButton() {
        interactor?.save()
    }
    
    func reloadData() {
        view?.isUndoButtonEnabled = interactor?.canUndo ?? false
        view?.isRedoButtonEndbled = interactor?.canRedo ?? false
    }
    
    func printItemList(_ list: String) {
        view?.mainTextViewContent = list
    }
    
    func printHelpInfo(_ info: String) {
        view?.mainTextViewContent = info
    }
    
    func printMessage(_ message: String) {
        view?.mainTextViewContent = message
    }
}
