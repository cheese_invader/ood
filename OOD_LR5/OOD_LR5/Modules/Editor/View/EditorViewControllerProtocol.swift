//
//  EditorViewControllerProtocol.swift
//  OOD_LR5
//
//  Created by Marty on 15/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol EditorViewControllerProtocol: class {
    func setup(presenter: EditorPresenterProtocol)
    
    var isUndoButtonEnabled: Bool { get set }
    var isRedoButtonEndbled: Bool { get set }
    var mainTextViewContent    : String { get set }
    var commandTextFieldContent: String { get set }
}

