//
//  EditorViewController.swift
//  OOD_LR5
//
//  Created by Marty on 15/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit
import WebKit

class EditorViewController: UIViewController, EditorViewControllerProtocol {
    // MARK: - VIPER -
    private var presenter: EditorPresenterProtocol?
    
    
    // MARK: - EditorViewControllerProtocol -
    func setup(presenter: EditorPresenterProtocol) {
        self.presenter = presenter
    }
    
    var isUndoButtonEnabled: Bool {
        get {
            return undoButton.isEnabled
        }
        
        set {
            undoButton.isEnabled = newValue
        }
    }
    
    var isRedoButtonEndbled: Bool {
        get {
            return redoButton.isEnabled
        }
        
        set {
            redoButton.isEnabled = newValue
        }
    }
    
    var mainTextViewContent: String {
        get {
            return mainTextView.text
        }
        set {
            mainTextView.text = newValue
        }
    }
    
    var commandTextFieldContent: String {
        get {
            return commandTextField.text ?? ""
        }
        set {
            commandTextField.text = newValue
        }
    }
    
    
    // MARK: - Outlet -
    @IBOutlet weak var undoButton: UIBarButtonItem!
    @IBOutlet weak var redoButton: UIBarButtonItem!
    
    @IBOutlet weak var commandTextField: UITextField!
    @IBOutlet weak var mainTextView: UITextView!
    @IBOutlet weak var webView: WKWebView!
    
    
    // MARK: - Action -
    @IBAction func undoButtonWasTapped(_ sender: UIBarButtonItem) {
        presenter?.userTappedUndoButton()
    }
    
    @IBAction func redoButtonWasTapped(_ sender: UIBarButtonItem) {
        presenter?.userTappedRedoButton()
    }
    
    @IBAction func listButtonWasTapped(_ sender: UIBarButtonItem) {
        webView.isHidden = true
        presenter?.userTappedListButton()
    }
    
    @IBAction func helpButtonWasTapped(_ sender: UIBarButtonItem) {
        webView.isHidden = true
        presenter?.userTappedHelpButton()
    }
    
    @IBAction func saveButtonWasAdded(_ sender: UIBarButtonItem) {
        presenter?.userTappedSaveButton()
        webView.isHidden = false
        
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileURL = documentsDirectory.appendingPathComponent("index.html")
        
        if fileURL.isFileURL {
            webView.loadFileURL(fileURL, allowingReadAccessTo: documentsDirectory)
        } else {
            let request = URLRequest(url: fileURL)
            webView.load(request)
        }
    }
    
    @IBAction func mainTextFieldPrimaryActionWasTriggered(_ sender: UITextField) {
        presenter?.userEnteredCommand(commandTextField.text ?? "")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)

        presenter?.reloadData()
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
