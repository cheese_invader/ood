//
//  EditorRouter.swift
//  OOD_LR5
//
//  Created by Marty on 15/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit


private let storyboardName = "Editor"
private let storyboardID   = "EditorStoryboard"


class EditorRouter: EditorRouterProtocol {
    private static let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
    private weak var navigation: NavigationViewController?
    
    
    // MARK: - VIPER -
    private weak var presenter: EditorPresenterProtocol?
    
    init(presenter: EditorPresenterProtocol, navigationController: NavigationViewController) {
        self.presenter  = presenter
        self.navigation = navigationController
    }
    
    
    // MARK: - EditorRouterProtocol -
    static func assemble(embadedIn navigationController: NavigationViewController) -> UIViewController {
        let editorVC = EditorRouter.storyboard.instantiateViewController(withIdentifier: storyboardID) as! EditorViewController
        
        let presenter = EditorPresenter()
        let interactor = EditorInteractor(presenter: presenter)
        let router = EditorRouter(presenter: presenter, navigationController: navigationController)
        
        presenter.setup(view: editorVC, router: router, interactor: interactor)
        editorVC.setup(presenter: presenter)
        
        return editorVC
    }
    
    var isNavbarHidden: Bool {
        get {
            return navigation?.isNavigationBarHidden ?? true
        }
        set {
            navigation?.setNavigationBarHidden(newValue, animated: true)
        }
    }

}
