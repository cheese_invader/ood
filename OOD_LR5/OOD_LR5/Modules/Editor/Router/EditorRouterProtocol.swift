//
//  EditorRouterProtocol.swift
//  OOD_LR5
//
//  Created by Marty on 15/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

protocol EditorRouterProtocol {
    static func assemble(embadedIn navigationController: NavigationViewController) -> UIViewController
    
    var isNavbarHidden: Bool { get set }
}
