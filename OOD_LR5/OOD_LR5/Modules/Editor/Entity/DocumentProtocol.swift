//
//  DocumentProtocol.swift
//  OOD_LR5
//
//  Created by Marty on 22/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

protocol DocumentProtocol {
    func setDelegate(_ delegate: DocumentDelegate)
    
    var itemsCount: Int { get }
    var title: String { get set }
    var canUndo: Bool { get }
    var canRedo: Bool { get }
    
    func insertParagraph(_ paragraph: String, atPosition position: Int?)
    func insertImage(_ image: UIImage, name: String, widh: Int, height: Int, atPosition position: Int?)
    
    func getItemAtIndex(_ index: Int) -> DocumentItemProtocol
    func insertItem(_ item: DocumentItemProtocol, atIndex index: Int)
    func removeItemAtIndex(_ index: Int)
    
    func undo()
    func redo()
    
    func save()
}


