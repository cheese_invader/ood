//
//  Menu.swift
//  OOD_LR5
//
//  Created by Marty on 22/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

typealias CommandHandler = (String) -> ()

class Menu {
    private var     handlerByShortcut = [String: CommandHandler]()
    private var descriptionByShortcut = [String: String]()
    private let document: DocumentProtocol
    
    
    init(document: DocumentProtocol) {
        self.document = document
    }
    
    func addShortcut(_ shortcut: String, withDescription description: String, andHandler handler: @escaping CommandHandler) {
        handlerByShortcut[shortcut]     = handler
        descriptionByShortcut[shortcut] = description
    }
    
    func excecuteCommand(_ command: String) {
        guard let firstWhitespace = command.firstIndex(of: " ") else {
            return
        }
        let shortcut = String(command[..<firstWhitespace])
        let tail = String(command[command.index(after: firstWhitespace)...])
        
        handlerByShortcut[shortcut]?(tail)
    }
    
    var list: String {
        var result = "Title: \(document.title)\n"
        
        for i in 0..<document.itemsCount {
            result.append(document.getItemAtIndex(i).description + "\n")
        }
        return result
    }
    
    var help: String {
        var result = ""
        
        for command in handlerByShortcut.keys {
            result.append(command + ": " + descriptionByShortcut[command]! + "\n\n")
        }
        return result
    }
}
