//
//  DocumentItemProtocol.swift
//  OOD_LR5
//
//  Created by Marty on 22/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol DocumentItemProtocol {
    var contentType: DocumentItemContent { get }
    var image: ImageProtocol? { get }
    var paragraph: ParagraphProtocol? { get }
    var description: String { get }
}
