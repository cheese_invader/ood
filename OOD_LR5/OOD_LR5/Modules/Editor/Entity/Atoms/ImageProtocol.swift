//
//  ImageProtocol.swift
//  OOD_LR5
//
//  Created by Marty on 22/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

protocol ImageProtocol {
    var width: Int { get }
    var height: Int { get }
    var image: UIImage { get set }
    var name: String { get }
    
    func resize(width: Int, height: Int)
}
