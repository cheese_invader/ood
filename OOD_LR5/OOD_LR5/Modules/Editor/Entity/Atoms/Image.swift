//
//  Image.swift
//  OOD_LR5
//
//  Created by Marty on 22/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

private let maxWidth  = 10_000
private let maxHeight = 10_000

class Image: ImageProtocol {
    init(withImage image: UIImage, name: String) {
        self.image = image
        self.name = name
        self.width  = Int(image.size.width)  <= maxWidth  ? Int(image.size.width)  : maxWidth
        self.height = Int(image.size.height) <= maxHeight ? Int(image.size.height) : maxHeight
    }
    
    var width: Int
    var height: Int
    var image: UIImage
    var name: String
    
    func resize(width: Int, height: Int) {
        guard 1...maxWidth ~= width && 1...maxHeight ~= height else {
            return
        }
        self.width = width
        self.height = height
    }
}

