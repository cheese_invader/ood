//
//  DocumentItem.swift
//  OOD_LR5
//
//  Created by Marty on 22/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

class DocumentItem: DocumentItemProtocol {
    init(withParagraph paragraph: ParagraphProtocol) {
        contentType = .paragraph
        self.paragraph = paragraph
    }
    
    init(withImage image: ImageProtocol) {
        contentType = .image
        self.image = image
    }
    
    // MARK: - DocumentItemProtocol -
    public private(set) var contentType: DocumentItemContent
    public private(set) var paragraph: ParagraphProtocol?
    public private(set) var image: ImageProtocol?
    
    var description: String {
        switch contentType {
        case .image:
            return "Image \(image?.name ?? "Unnamed") \(image?.width ?? 0) x \(image?.height ?? 0)\n"
        case .paragraph:
            return "Paragraph \"\(paragraph?.text ?? "")\"\n"
        }
    }
}

