//
//  Document.swift
//  OOD_LR5
//
//  Created by Marty on 22/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit


private let historySize = 6


class Document: DocumentProtocol {
    private weak var delegate: DocumentDelegate?
    private var items = [DocumentItemProtocol]()
    
    func setDelegate(_ delegate: DocumentDelegate) {
        self.delegate = delegate
    }
    
    var itemsCount: Int {
        return items.count
    }
    
    var title = ""
    
    var canUndo: Bool {
        return delegate?.canUndo ?? false
    }
    
    var canRedo: Bool {
        return delegate?.canRedo ?? false
    }
    
    func insertParagraph(_ paragraph: String, atPosition position: Int?) {
        var paragraphObj: ParagraphProtocol = Paragraph()
        paragraphObj.text = paragraph
        
        let docItem: DocumentItemProtocol = DocumentItem(withParagraph: paragraphObj)
        items.insert(docItem, at: position ?? self.items.count)
    }
    
    func insertImage(_ image: UIImage, name: String, widh: Int, height: Int, atPosition position: Int?) {
        let imageObj: ImageProtocol = Image(withImage: image, name: name)
        imageObj.resize(width: widh, height: height)
        
        let docItem: DocumentItemProtocol = DocumentItem(withImage: imageObj)
        items.insert(docItem, at: position ?? self.items.count)
    }
    
    func getItemAtIndex(_ index: Int) -> DocumentItemProtocol {
        return items[index]
    }
    
    func insertItem(_ item: DocumentItemProtocol, atIndex index: Int) {
        items.insert(item, at: index)
    }
    
    func removeItemAtIndex(_ index: Int) {
        items.remove(at: index)
    }
    
    func undo() {
        delegate?.undo()
    }
    
    func redo() {
        delegate?.redo()
    }
    
    func save() {
        
    }
}

