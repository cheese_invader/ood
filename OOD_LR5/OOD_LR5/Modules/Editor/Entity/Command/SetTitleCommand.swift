//
//  SetTitleCommand.swift
//  OOD_LR5
//
//  Created by Marty on 24/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class SetTitleCommand: CommandProtocol {
    private var document: DocumentProtocol
    private var title: String
    private var previousTitle: String
    
    
    init(withTitle title: String, inDocument document: DocumentProtocol) {
        self.document = document
        self.title = title
        self.previousTitle = document.title
    }
    
    
    // MARK: - CommandProtocol -
    func execute() throws {
        document.title = title
    }
    
    func unexecute() throws {
        document.title = previousTitle
    }
}

