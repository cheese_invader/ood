//
//  ResizeImageCommand.swift
//  OOD_LR5
//
//  Created by Marty on 24/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class ResizeImageCommand: CommandProtocol {
    private var document: DocumentProtocol
    private var position: Int
    private var width: Int
    private var previousWidth: Int!
    private var height: Int
    private var previousHeight: Int!
    
    init(withPosition position: Int, width: Int, height: Int, inDocument document: DocumentProtocol) {
        self.document = document
        self.position = position
        self.width  = width
        self.height = height
    }
    
    // MARK: - CommandProtocol -
    func execute() throws {
        if position >= document.itemsCount || position < 0 {
            throw CommandError.indexIsOutOfRange
        }
        
        let currentImage = document.getItemAtIndex(position)
        if currentImage.contentType != .image {
            throw CommandError.wrongTypeForCommand
        }
        self.previousWidth = currentImage.image?.width ?? 0
        self.previousHeight = currentImage.image?.height ?? 0
        currentImage.image?.resize(width: width, height: height)
    }
    
    func unexecute() throws {
        if position >= document.itemsCount {
            throw CommandError.indexIsOutOfRange
        }
        
        let currentImage = document.getItemAtIndex(position)
        if currentImage.contentType != .image {
            throw CommandError.wrongTypeForCommand
        }
        
        currentImage.image?.resize(width: previousWidth, height: previousHeight)
    }
}

