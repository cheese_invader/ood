//
//  InsertImageCommand.swift
//  OOD_LR5
//
//  Created by Marty on 22/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class InsertImageCommand: CommandProtocol {
    private var document: DocumentProtocol
    private var image: ImageProtocol
    private var position: Int?
    
    convenience init(withImage image: ImageProtocol, andPosition position: Int, inDocument document: DocumentProtocol) {
        self.init(withImage: image, inDocument: document)
        self.position = position
    }
    
    init(withImage image: ImageProtocol, inDocument document: DocumentProtocol) {
        self.image = image
        self.document = document
    }
    
    // MARK: - CommandProtocol -
    func execute() throws {
        if let position = self.position, (position > document.itemsCount || position < 0) {
            throw CommandError.indexIsOutOfRange
        }
        
        document.insertImage(image.image, name: image.name, widh: image.width, height: image.height, atPosition: position)
    }
    
    func unexecute() throws {
        if let position = self.position {
            if position >= document.itemsCount {
                throw CommandError.indexIsOutOfRange
            }
            if document.getItemAtIndex(position).contentType != .image {
                throw CommandError.wrongTypeForCommand
            }
        } else {
            if document.itemsCount == 0 {
                throw CommandError.indexIsOutOfRange
            }
        }
        document.removeItemAtIndex(position ?? document.itemsCount - 1)
    }

}
