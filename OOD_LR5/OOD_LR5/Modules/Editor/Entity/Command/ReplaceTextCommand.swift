//
//  ReplaceTextCommand.swift
//  OOD_LR5
//
//  Created by Marty on 22/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

class ReplaceTextCommand: CommandProtocol {
    private var document: DocumentProtocol
    private var paragraph: ParagraphProtocol
    private var previousParagraph: String!
    private var position: Int
    
    init(withParagraph paragraph: ParagraphProtocol, position: Int, inDocument document: DocumentProtocol) {
        self.document = document
        self.paragraph = paragraph
        self.position = position
    }
    
    // MARK: - CommandProtocol -
    func execute() throws {
        if position >= document.itemsCount || position < 0 {
            throw CommandError.indexIsOutOfRange
        }
        
        let currentParagraph = document.getItemAtIndex(position)
        if currentParagraph.contentType != .paragraph {
            throw CommandError.wrongTypeForCommand
        }
        previousParagraph = currentParagraph.paragraph?.text ?? ""
        
        document.removeItemAtIndex(position)
        document.insertParagraph(paragraph.text, atPosition: position)
    }
    
    func unexecute() throws {
        document.removeItemAtIndex(position)
        document.insertParagraph(previousParagraph, atPosition: position)
    }
}
