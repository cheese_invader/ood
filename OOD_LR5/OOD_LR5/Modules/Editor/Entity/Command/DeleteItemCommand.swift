//
//  DeleteItemCommand.swift
//  OOD_LR5
//
//  Created by Marty on 22/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class DeleteItemCommand: CommandProtocol {
    private var document: DocumentProtocol
    private var position: Int
    private var deletedItem: DocumentItemProtocol!
    
    init(withPosition position: Int, inDocument document: DocumentProtocol) {
        self.document = document
        self.position = position
    }
    
    // MARK: - CommandProtocol -
    func execute() throws {
        if position >= document.itemsCount || position < 0 {
            throw CommandError.indexIsOutOfRange
        }
        deletedItem = document.getItemAtIndex(position)
        document.removeItemAtIndex(position)
    }
    
    func unexecute() throws {
        if position > document.itemsCount {
            throw CommandError.indexIsOutOfRange
        }
        document.insertItem(deletedItem, atIndex: position)
    }
}

