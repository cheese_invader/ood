//
//  InsertParagraphCommand.swift
//  OOD_LR5
//
//  Created by Marty on 22/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class InsertParagraphCommand: CommandProtocol {
    private var document: DocumentProtocol
    private var paragraph: ParagraphProtocol
    private var position: Int?
    
    init(withParagraph paragraph: ParagraphProtocol, inDocument document: DocumentProtocol) {
        self.paragraph = paragraph
        self.document = document
    }
    
    convenience init(withParagraph paragraph: ParagraphProtocol, atPosition position: Int, inDocument document: DocumentProtocol) {
        self.init(withParagraph: paragraph, inDocument: document)
        self.position = position
    }
    
    
    // NARK: - CommandProtocol -
    func execute() throws {
        if let position = self.position, (position > document.itemsCount || position < 0) {
            throw CommandError.indexIsOutOfRange
        }
        document.insertParagraph(paragraph.text, atPosition: position)
    }
    
    func unexecute() throws {
        if let position = self.position {
            if position >= document.itemsCount {
                throw CommandError.indexIsOutOfRange
            }
            if document.getItemAtIndex(position).contentType != .paragraph {
                throw CommandError.wrongTypeForCommand
            }
        } else {
            if document.itemsCount == 0 {
                throw CommandError.indexIsOutOfRange
            }
        }
        document.removeItemAtIndex(position ?? document.itemsCount - 1)
    }
}

