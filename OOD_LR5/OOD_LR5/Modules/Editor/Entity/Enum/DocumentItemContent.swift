//
//  DocumentItemContent.swift
//  OOD_LR5
//
//  Created by Marty on 22/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

enum DocumentItemContent {
    case paragraph
    case image
}

