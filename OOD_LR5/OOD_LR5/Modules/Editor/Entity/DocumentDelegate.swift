//
//  DocumentDelegate.swift
//  OOD_LR5
//
//  Created by Marty on 22/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol DocumentDelegate: class {
    func undo()
    func redo()
    
    var canUndo: Bool { get }
    var canRedo: Bool { get }
}

