//
//  EditorInteractorProtocol.swift
//  OOD_LR5
//
//  Created by Marty on 15/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol EditorInteractorProtocol: class {
    func performCommand(_ command: String)
    var canUndo: Bool { get }
    var canRedo: Bool { get }
    
    func undo()
    func redo()
    func list()
    func help()
    func save()
}
