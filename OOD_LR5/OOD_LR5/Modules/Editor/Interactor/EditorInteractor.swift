//
//  EditorInteractor.swift
//  OOD_LR5
//
//  Created by Marty on 15/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import Foundation
import UIKit

private let historySize = 10


class EditorInteractor: EditorInteractorProtocol, DocumentDelegate {
    // MARK: - VIPER -
    private weak var presenter: EditorPresenterProtocol?
    
    init(presenter: EditorPresenterProtocol) {
        self.presenter = presenter
        self.document = Document()
        self.menu = Menu(document: self.document)
        
        setupMenu()
    }
    
    
    private var history = [CommandProtocol]()
    private var historyPointer = -1
    
    private let document: DocumentProtocol!
    private let menu: Menu!
    
    // MARK: - DocumentDelegate -
    func undo() {
        try? history[historyPointer].unexecute()
        historyPointer -= 1
        presenter?.reloadData()
    }
    
    func redo() {
        historyPointer += 1
        try? history[historyPointer].execute()
        presenter?.reloadData()
    }
    
    var canUndo: Bool {
        return historyPointer != -1
    }
    
    var canRedo: Bool {
        return historyPointer != history.count - 1
    }
    
    
    // MARK: - EditorInteractorProtocol -
    func performCommand(_ command: String) {
        menu.excecuteCommand(command)
        presenter?.reloadData()
    }
    
    func list() {
        presenter?.printItemList(menu.list)
    }
    
    func help() {
        presenter?.printHelpInfo(menu.help)
    }
    
    func save() {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        for i in 0..<document.itemsCount where document.getItemAtIndex(i).contentType == .image {
            if let image = document.getItemAtIndex(i).image {
                let fileURL = documentsDirectory.appendingPathComponent(image.name)
                if let data = image.image.jpegData(compressionQuality: 1.0) {
                    do {
                        try data.write(to: fileURL)
                    } catch {}
                }
            }
        }
        let fileURL = documentsDirectory.appendingPathComponent("index.html")
        if let data = composeHtml().data(using: .utf8) {
            do {
                try data.write(to: fileURL)
            } catch {}
        }
    }
    
    private func composeHtml() -> String {
        return """
        <!DOCTYPE html>
        <html lang="en" dir="ltr">
        <head>
        <meta charset="utf-8">
        <title>LR5</title>
        </head>
        <body>
        <h1>\(document.title)</h1>
        \(paragraphsAndImages())
        </body>
        </html>
        """
    }
    
    private func paragraphsAndImages() -> String {
        var result = ""
        for i in 0..<document.itemsCount {
            let element = document.getItemAtIndex(i)
            switch element.contentType {
            case .paragraph:
                result.append("<p>\(i) \(element.description)</p>\n")
            case .image:
                guard let fileName = element.image?.name else {
                    continue
                }
                let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
                let fileURL = documentsDirectoryURL.appendingPathComponent(fileName)
                result.append("<img src=\"\(fileURL.absoluteString)\" height=\"\(element.image?.width ?? 1000)\" width=\"\(element.image?.height ?? 1000)\">")
            }
        }
        return result
    }
    
    func setCommandToHistory(_ command: CommandProtocol) {
        history.append(command)
        
        if history.count > historySize {
            history.removeFirst()
            if historyPointer != -1 {
                historyPointer -= 1
            }
        }
        historyPointer += 1
        
        history.removeLast(history.count - historyPointer - 1)
        
        presenter?.reloadData()
    }
    
    func setupMenu() {
        menu.addShortcut("InsertParagraph", withDescription: "Вставляет в указанную позицию документа параграф с указанным текстом. В качестве позиции вставки можно указать либо порядковый номер блока, либо end для вставки параграфа в конец.") { (params) in
            guard let whitespacePosition = params.firstIndex(of: " ") else {
                self.presenter?.printMessage("Wrong InsertParagraph command")
                return
            }
            let position = String(params[..<whitespacePosition])
            let paragraph = Paragraph()
            paragraph.text = String(params[params.index(after: whitespacePosition)...])
            
            
            if let intPosition = Int(position) {
                let command = InsertParagraphCommand(withParagraph: paragraph, atPosition: intPosition, inDocument: self.document)
                try? command.execute()
                self.setCommandToHistory(command)
            } else if position == "end" {
                let command = InsertParagraphCommand(withParagraph: paragraph, inDocument: self.document)
                try? command.execute()
                self.setCommandToHistory(command)
            } else {
                self.presenter?.printMessage("Wrong InsertParagraph position")
                return
            }
        }
        
        menu.addShortcut("InsertImage", withDescription: "Вставляет в указанную позицию документа изображение, с указанным названием. При вставке указывается ширина и высота, что позволяет разместить изображение с указанным масштабом.") { (params) in
            let paramsComponents = params.components(separatedBy: .whitespaces)
            guard paramsComponents.count == 4 else {
                self.presenter?.printMessage("Wrong params count for InsertImage command")
                return
            }
            
            guard let width = Int(paramsComponents[1]), let height = Int(paramsComponents[2]) else {
                self.presenter?.printMessage("Wrong params for InsertImage command")
                return
            }
            
            guard let image = UIImage(named: paramsComponents[3]) else {
                self.presenter?.printMessage("Wrong image name for InsertImage command")
                return
            }
            
            let imageItem = Image(withImage: image, name: paramsComponents[3])
            imageItem.width = width
            imageItem.height = height
            
            
            if let intPosition = Int(paramsComponents[0]) {
                let command = InsertImageCommand(withImage: imageItem, andPosition: intPosition, inDocument: self.document)
                try? command.execute()
                self.setCommandToHistory(command)
            } else if paramsComponents[0] == "end" {
                let command = InsertImageCommand(withImage: imageItem, inDocument: self.document)
                try? command.execute()
                self.setCommandToHistory(command)
            } else {
                self.presenter?.printMessage("Wrong InsertImage command position")
                return
            }
        }
        
        menu.addShortcut("SetTitle", withDescription: "Изменяет заголовок документа") { (params) in
            let command = SetTitleCommand(withTitle: params, inDocument: self.document)
            try? command.execute()
            self.setCommandToHistory(command)
        }
        
        menu.addShortcut("ReplaceText", withDescription: "Заменяет текст в параграфе, находящемся в указанной позиции документа. Если в данной позиции не находится параграф, выдается сообщение об ошибке, а команда игнорируется.") { (params) in
            guard let whitespacePosition = params.firstIndex(of: " ") else {
                self.presenter?.printMessage("Wrong ReplaceText command")
                return
            }
            let position = String(params[..<whitespacePosition])
            let paragraph = Paragraph()
            paragraph.text = String(params[params.index(after: whitespacePosition)...])
            
            
            if let intPosition = Int(position) {
                let command = ReplaceTextCommand(withParagraph: paragraph, position: intPosition, inDocument: self.document)
                try? command.execute()
                self.setCommandToHistory(command)
            } else {
                self.presenter?.printMessage("Wrong ReplaceText position")
                return
            }
        }
        
        menu.addShortcut("ResizeImage", withDescription: "Изменяет размер изображения, находящегося в указанной позиции документа. Если в данной позиции не находится  изображение, выдается сообщение об ошибке") { (params) in
            let paramsComponents = params.components(separatedBy: .whitespaces)
            guard paramsComponents.count == 3 else {
                self.presenter?.printMessage("Wrong params count for ResizeImage command")
                return
            }
            
            guard let position = Int(paramsComponents[0]), let width = Int(paramsComponents[1]), let height = Int(paramsComponents[2]) else {
                self.presenter?.printMessage("Wrong params for ResizeImage command")
                return
            }
            
            let command = ResizeImageCommand(withPosition: position, width: width, height: height, inDocument: self.document)
            try? command.execute()
            self.setCommandToHistory(command)
        }
        
        menu.addShortcut("DeleteItem", withDescription: "Удаляет элемент документа, находящийся в указанной позиции. Если указана недопустимая позиция документа, команда игнорируется, а пользователю выводится сообщение об ошибке.") { (params) in
            guard let position = Int(params) else {
                self.presenter?.printMessage("Wrong position for DeleteItem command")
                return
            }
            
            let command = DeleteItemCommand(withPosition: position, inDocument: self.document)
            try? command.execute()
            self.setCommandToHistory(command)
        }
    }
}



