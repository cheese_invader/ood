//
//  NavigationViewController.swift
//  OOD_LR5
//
//  Created by Marty on 15/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

class NavigationViewController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let editorVC = EditorRouter.assemble(embadedIn: self)
        pushViewController(editorVC, animated: false)
    }
}
