//
//  OOD_LR4ShapesTest.swift
//  OOD_LR4Tests
//
//  Created by Marty on 04/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import XCTest
@testable import OOD_LR4

private struct Line: Equatable {
    var from: CGPoint
    var to  : CGPoint
    
    static func ==(lhs: Line, rhs: Line) -> Bool {
        return abs(lhs.from.x - rhs.from.x) < 0.01 &&
               abs(lhs.from.y - rhs.from.y) < 0.01 &&
               abs(lhs.to.x   - rhs.to.x)   < 0.01 &&
               abs(lhs.to.y   - rhs.to.y)   < 0.01
    }
}

private struct Ellipse: Equatable {
    var x     : CGFloat
    var y     : CGFloat
    var width : CGFloat
    var height: CGFloat
}

private class CanvasTest: CanvasProtocol {
    var lines    = [Line]()
    var ellipses = [Ellipse]()
    var colors   = [Color]()
    
    func setColor(_ color: Color) {
        colors.append(color)
    }
    
    func drawLine(from: CGPoint, to: CGPoint) {
        lines.append(Line(from: from, to: to))
    }
    
    func drawEllipse(x: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat) {
        ellipses.append(Ellipse(x: x, y: y, width: width, height: height))
    }
}

class OOD_LR4ShapesTest: XCTestCase {
    func testRectangleInCanvas() {
        let canvas = CanvasTest()
        
        let leftTop     = CGPoint(x: 10, y: 11)
        let rightTop    = CGPoint(x: 12, y: 11)
        let rightBottom = CGPoint(x: 12, y: 13)
        let leftBottom  = CGPoint(x: 10, y: 13)
        
        let line1 = Line(from: leftTop, to: rightTop)
        let line2 = Line(from: rightTop, to: rightBottom)
        let line3 = Line(from: rightBottom, to: leftBottom)
        let line4 = Line(from: leftBottom, to: leftTop)
        
        let rect = Rectangle(leftTop: leftTop, rightBottom: rightBottom, color: .red)

        rect.drawInCanvas(canvas)
        
        XCTAssertEqual(canvas.lines, [line1, line2, line3, line4])
        XCTAssertEqual(canvas.ellipses, [])
        XCTAssertEqual(canvas.colors, [.red])
    }
    
    func testTriangleInCanvas() {
        let canvas = CanvasTest()
        
        let vertex1 = CGPoint(x: 10, y: 11)
        let vertex2 = CGPoint(x: 13, y: 20)
        let vertex3 = CGPoint(x: 5,  y: 20)
        
        let line1 = Line(from: vertex1, to: vertex2)
        let line2 = Line(from: vertex2, to: vertex3)
        let line3 = Line(from: vertex3, to: vertex1)
        
        let triangle = Triangle(vertex1: vertex1, vertex2: vertex2, vertex3: vertex3, color: .blue)
        triangle.drawInCanvas(canvas)
        
        XCTAssertEqual(canvas.lines, [line1, line2, line3])
        XCTAssertEqual(canvas.ellipses, [])
        XCTAssertEqual(canvas.colors, [.blue])
    }
    
    func testEllipseInCanvas() {
        let canvas = CanvasTest()
        
        let testEllipse = Ellipse(x: 25, y: 50, width: 150, height: 300)
        
        let drawingEllipse = OOD_LR4.Ellipse(center: CGPoint(x: testEllipse.x + testEllipse.width / 2, y: testEllipse.y + testEllipse.height / 2), horizontalRadius: testEllipse.width / 2, verticalRadius: testEllipse.height / 2, color: .pink)
        drawingEllipse.drawInCanvas(canvas)
        
        XCTAssertEqual(canvas.lines, [])
        XCTAssertEqual(canvas.ellipses, [testEllipse])
        XCTAssertEqual(canvas.colors, [.pink])
    }
    
    func testRegularPolygonInCanvas() {
        let canvas = CanvasTest()
        let polygon = RegularPolygon(vertexCount: 4, center: CGPoint(x: 1000, y: 1000), radius: 500, color: .green)
        polygon.drawInCanvas(canvas)
        
        let right  = CGPoint(x: 1500, y: 1000)
        let top    = CGPoint(x: 1000, y: 1500)
        let left   = CGPoint(x: 500,  y: 1000)
        let bottom = CGPoint(x: 1000, y: 500)
        
        let line1 = Line(from: right, to: top)
        let line2 = Line(from: top, to: left)
        let line3 = Line(from: left, to: bottom)
        let line4 = Line(from: bottom, to: right)
        
        XCTAssertEqual(canvas.lines, [line1, line2, line3, line4])
        XCTAssertEqual(canvas.ellipses, [])
        XCTAssertEqual(canvas.colors, [.green])
    }
}
