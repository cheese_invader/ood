//
//  OOD_LR4FactoryTest.swift
//  OOD_LR4Tests
//
//  Created by Marty on 04/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import XCTest
@testable import OOD_LR4

class OOD_LR4FactoryTest: XCTestCase {
    // MARK: - Rect -
    func testFactoryRectangle() {
        let shapeFactory: ShapeFactoryProtocol = ShapeFactory()
        let shape = shapeFactory.createShape("red rectangle 10 11 150.4 200.1")
        XCTAssertNotNil(shape)
        
        let rectOpt = shape! as? Rectangle
        XCTAssertNotNil(rectOpt)
        
        let rect = rectOpt!
        XCTAssertEqual(rect.leftTop.x,        10, accuracy: 0.001)
        XCTAssertEqual(rect.leftTop.y,        11, accuracy: 0.001)
        XCTAssertEqual(rect.rightBottom.x, 150.4, accuracy: 0.001)
        XCTAssertEqual(rect.rightBottom.y, 200.1, accuracy: 0.001)
        XCTAssertEqual(rect.color, .red)
    }
    
    func testFactoryRectangleWithoutParams() {
        let shapeFactory: ShapeFactoryProtocol = ShapeFactory()
        let shape = shapeFactory.createShape("red rectangle")
        XCTAssertNil(shape)
    }
    
    func testFactoryRectangleWithExtraParams() {
        let shapeFactory: ShapeFactoryProtocol = ShapeFactory()
        let shape = shapeFactory.createShape("red rectangle 10 11 150.4 200.1 2")
        XCTAssertNil(shape)
    }
    
    func testFactoryRectangleWithWrongSpelling() {
        let shapeFactory: ShapeFactoryProtocol = ShapeFactory()
        let shape = shapeFactory.createShape("red ellipse 10 11 150.4 200.1")
        XCTAssertNotNil(shape)
        
        let rectOpt = shape! as? Rectangle
        XCTAssertNil(rectOpt)
    }
    
    func testFactoryNotRectangle() {
        let shapeFactory: ShapeFactoryProtocol = ShapeFactory()
        let shape = shapeFactory.createShape("red rectengle 10 11 150.4 200.1")
        XCTAssertNil(shape)
    }
    
    // MARK: - Ellipse -
    func testFactoryEllipse() {
        let shapeFactory: ShapeFactoryProtocol = ShapeFactory()
        let shape = shapeFactory.createShape("red ellipse 10 11 150.4 200.1")
        XCTAssertNotNil(shape)
        
        let ellipseOpt = shape! as? Ellipse
        XCTAssertNotNil(ellipseOpt)
        
        let ellipse = ellipseOpt!
        XCTAssertEqual(ellipse.center.x,         10,    accuracy: 0.001)
        XCTAssertEqual(ellipse.center.y,         11,    accuracy: 0.001)
        XCTAssertEqual(ellipse.horizontalRadius, 150.4, accuracy: 0.001)
        XCTAssertEqual(ellipse.verticalRadius,   200.1, accuracy: 0.001)
        XCTAssertEqual(ellipse.color, .red)
    }
    
    func testFactoryEllipseWithoutParams() {
        let shapeFactory: ShapeFactoryProtocol = ShapeFactory()
        let shape = shapeFactory.createShape("red ellipse")
        XCTAssertNil(shape)
    }
    
    func testFactoryEllipseWithExtraParams() {
        let shapeFactory: ShapeFactoryProtocol = ShapeFactory()
        let shape = shapeFactory.createShape("red ellipse 10 11 150.4 200.1 2")
        XCTAssertNil(shape)
    }
    
    func testFactoryEllipseWithWrongSpelling() {
        let shapeFactory: ShapeFactoryProtocol = ShapeFactory()
        let shape = shapeFactory.createShape("red elipse 10 11 150.4 200.1")
        XCTAssertNil(shape)
    }
    
    func testFactoryNotEllipse() {
        let shapeFactory: ShapeFactoryProtocol = ShapeFactory()
        let shape = shapeFactory.createShape("red rectangle 10 11 150.4 200.1")
        XCTAssertNotNil(shape)
        
        let ellipseOpt = shape! as? Ellipse
        XCTAssertNil(ellipseOpt)
    }
    
    // MARK: - Triangle -
    func testFactoryTriangle() {
        let shapeFactory: ShapeFactoryProtocol = ShapeFactory()
        let shape = shapeFactory.createShape("red triangle 10 11 12.3 13.5 5 -11.2")
        XCTAssertNotNil(shape)
        
        let triangleOpt = shape! as? Triangle
        XCTAssertNotNil(triangleOpt)
        
        let triangle = triangleOpt!
        XCTAssertEqual(triangle.vertex1.x, 10,    accuracy: 0.001)
        XCTAssertEqual(triangle.vertex1.y, 11,    accuracy: 0.001)
        XCTAssertEqual(triangle.vertex2.x, 12.3,  accuracy: 0.001)
        XCTAssertEqual(triangle.vertex2.y, 13.5,  accuracy: 0.001)
        XCTAssertEqual(triangle.vertex3.x, 5,     accuracy: 0.001)
        XCTAssertEqual(triangle.vertex3.y, -11.2, accuracy: 0.001)
        XCTAssertEqual(triangle.color, .red)
    }
    
    func testFactoryTriangleWithoutParams() {
        let shapeFactory: ShapeFactoryProtocol = ShapeFactory()
        let shape = shapeFactory.createShape("red triangle")
        XCTAssertNil(shape)
    }
    
    func testFactoryTriangleWithExtraParams() {
        let shapeFactory: ShapeFactoryProtocol = ShapeFactory()
        let shape = shapeFactory.createShape("red triangle 10 11 12.3 13.5 5 -11.2 7")
        XCTAssertNil(shape)
    }
    
    func testFactoryTriangleWithWrongSpelling() {
        let shapeFactory: ShapeFactoryProtocol = ShapeFactory()
        let shape = shapeFactory.createShape("red tiangle 10 11 12.3 13.5 5 -11.2")
        XCTAssertNil(shape)
    }
    
    func testFactoryNotTriangle() {
        let shapeFactory: ShapeFactoryProtocol = ShapeFactory()
        let shape = shapeFactory.createShape("red rectangle 10 11 150.4 200.1")
        XCTAssertNotNil(shape)
        
        let ellipseOpt = shape! as? Triangle
        XCTAssertNil(ellipseOpt)
    }
    
    // MARK: - RegularPolygon -
    func testFactoryRegularPolygon() {
        let shapeFactory: ShapeFactoryProtocol = ShapeFactory()
        let shape = shapeFactory.createShape("red regularPolygon 6 11 12.3 4")
        XCTAssertNotNil(shape)
        
        let polygonOpt = shape! as? RegularPolygon
        XCTAssertNotNil(polygonOpt)
        
        let polygon = polygonOpt!
        XCTAssertEqual(polygon.vertexCount, 6)
        XCTAssertEqual(polygon.center.x, 11,   accuracy: 0.001)
        XCTAssertEqual(polygon.center.y, 12.3, accuracy: 0.001)
        XCTAssertEqual(polygon.radius,   4,    accuracy: 0.001)
        XCTAssertEqual(polygon.color, .red)
    }
    
    func testFactoryRegularPolygonWithoutParams() {
        let shapeFactory: ShapeFactoryProtocol = ShapeFactory()
        let shape = shapeFactory.createShape("red regularPolygon")
        XCTAssertNil(shape)
    }
    
    func testFactoryRegularPolygonWithExtraParams() {
        let shapeFactory: ShapeFactoryProtocol = ShapeFactory()
        let shape = shapeFactory.createShape("red regularPolygon 10 11 12.3 13.5 5 -11.2 7")
        XCTAssertNil(shape)
    }
    
    func testFactoryRegularPolygonWithWrongSpelling() {
        let shapeFactory: ShapeFactoryProtocol = ShapeFactory()
        let shape = shapeFactory.createShape("red regularPolygon 10 11 12.3 13.5 5 -11.2")
        XCTAssertNil(shape)
    }
    
    func testFactoryNotRegularPolygon() {
        let shapeFactory: ShapeFactoryProtocol = ShapeFactory()
        let shape = shapeFactory.createShape("red rectangle 10 11 150.4 200.1")
        XCTAssertNotNil(shape)
        
        let ellipseOpt = shape! as? RegularPolygon
        XCTAssertNil(ellipseOpt)
    }
}
