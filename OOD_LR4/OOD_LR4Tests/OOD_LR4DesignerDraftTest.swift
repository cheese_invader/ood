//
//  OOD_LR4DesignerDraftTest.swift
//  OOD_LR4Tests
//
//  Created by Marty on 04/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import XCTest
@testable import OOD_LR4

class OOD_LR4DesignerDraftTest: XCTestCase {
    // MARK: - Designer & Draft -
    func testDesignerAndDraft() {
        let instructions = ["red rectangle 10 11 150.4 200.1",
                            "red ellipse 10 11 150.4 200.1",
                            "red triangle 10 11 12.3 13.5 5 -11.2",
                            "red regularPolygon 6 11 12.3 4"]
        
        
        let designer = Designer(withFactory: ShapeFactory())
        let draft = designer.createDraftFromInstructions(instructions)
        
        // Rectangle
        var shape = draft.nextShape
        XCTAssertNotNil(shape)
        let rect = shape! as? Rectangle
        XCTAssertNotNil(rect)
        
        XCTAssertEqual(rect!.leftTop.x,     10,    accuracy: 0.001)
        XCTAssertEqual(rect!.leftTop.y,     11,    accuracy: 0.001)
        XCTAssertEqual(rect!.rightBottom.x, 150.4, accuracy: 0.001)
        XCTAssertEqual(rect!.rightBottom.y, 200.1, accuracy: 0.001)
        XCTAssertEqual(rect!.color, .red)
        
        // ellipse
        shape = draft.nextShape
        XCTAssertNotNil(shape)
        let ellipse = shape! as? Ellipse
        XCTAssertNotNil(ellipse)
        
        XCTAssertEqual(ellipse!.center.x,         10,    accuracy: 0.001)
        XCTAssertEqual(ellipse!.center.y,         11,    accuracy: 0.001)
        XCTAssertEqual(ellipse!.horizontalRadius, 150.4, accuracy: 0.001)
        XCTAssertEqual(ellipse!.verticalRadius,   200.1, accuracy: 0.001)
        XCTAssertEqual(ellipse!.color, .red)
        
        // triangle
        shape = draft.nextShape
        XCTAssertNotNil(shape)
        let triangle = shape! as? Triangle
        XCTAssertNotNil(triangle)
        
        XCTAssertEqual(triangle!.vertex1.x, 10,    accuracy: 0.001)
        XCTAssertEqual(triangle!.vertex1.y, 11,    accuracy: 0.001)
        XCTAssertEqual(triangle!.vertex2.x, 12.3,  accuracy: 0.001)
        XCTAssertEqual(triangle!.vertex2.y, 13.5,  accuracy: 0.001)
        XCTAssertEqual(triangle!.vertex3.x, 5,     accuracy: 0.001)
        XCTAssertEqual(triangle!.vertex3.y, -11.2, accuracy: 0.001)
        XCTAssertEqual(triangle!.color, .red)
        
        // regular polygon
        shape = draft.nextShape
        XCTAssertNotNil(shape)
        let polygon = shape! as? RegularPolygon
        XCTAssertNotNil(polygon)
        
        XCTAssertEqual(polygon!.vertexCount, 6)
        XCTAssertEqual(polygon!.center.x, 11,   accuracy: 0.001)
        XCTAssertEqual(polygon!.center.y, 12.3, accuracy: 0.001)
        XCTAssertEqual(polygon!.radius,   4,    accuracy: 0.001)
        XCTAssertEqual(polygon!.color, .red)
        
        // end
        XCTAssertTrue(draft.isEnd)
        XCTAssertNil(draft.nextShape)
    }
    
    func testIgnoreWrongCommandFromDraft() {
        // ract & triangle are spelled wrong
        let instructions = ["red rectangl 10 11 150.4 200.1",
                            "red ellipse 10 11 150.4 200.1",
                            "red treangle 10 11 12.3 13.5 5 -11.2",
                            "red regularPolygon 6 11 12.3 4"]
        
        
        let designer = Designer(withFactory: ShapeFactory())
        let draft = designer.createDraftFromInstructions(instructions)
        
        // Rectangle
        var shape = draft.nextShape
        XCTAssertNotNil(shape)
        // Ellipse is second instruction
        let ellipse = shape! as? Ellipse
        XCTAssertNotNil(ellipse)
        
        shape = draft.nextShape
        XCTAssertNotNil(shape)
        // Ellipse is fourth instruction
        let polygon = shape! as? RegularPolygon
        XCTAssertNotNil(polygon)
    }
}
