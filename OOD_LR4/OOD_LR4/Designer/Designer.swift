//
//  Designer.swift
//  OOD_LR4
//
//  Created by Marty on 02/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class Designer: DesignerProtocol {
    private let factory: ShapeFactoryProtocol
    
    init(withFactory factory: ShapeFactoryProtocol) {
        self.factory = factory
    }
    
    func createDraftFromInstructions(_ instructions: [String]) -> PictureDraft {
        var shapes = [ShapeProtocol]()
        
        for instruction in instructions {
            if let shape = factory.createShape(instruction) {
                shapes.append(shape)
            }
        }
        
        return PictureDraft(shapes: shapes)
    }
}
