//
//  DesignerProtocol.swift
//  OOD_LR4
//
//  Created by Marty on 02/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol DesignerProtocol {
    func createDraftFromInstructions(_ instructions: [String]) -> PictureDraft
}
