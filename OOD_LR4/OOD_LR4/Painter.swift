//
//  Painter.swift
//  OOD_LR4
//
//  Created by Marty on 02/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class Painter {
    func drawPictureFromDraft(_ draft: PictureDraft, inCanvas canvas: CanvasProtocol) {
        while !draft.isEnd {
            if let shape = draft.nextShape {
                shape.drawInCanvas(canvas)
            }
        }
    }
}
