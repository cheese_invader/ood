//
//  PictuerDraft.swift
//  OOD_LR4
//
//  Created by Marty on 02/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class PictureDraft {
    private var pointer = 0
    private var shapes: [ShapeProtocol]
    
    init(shapes: [ShapeProtocol]) {
        self.shapes = shapes
    }
    
    var isEnd: Bool {
        return pointer == shapes.count
    }
    
    var nextShape: ShapeProtocol? {
        guard pointer != shapes.count else {
            return nil
        }
        pointer += 1
        return shapes[pointer - 1]
    }
    
    func reset() {
        pointer = 0
    }
}
