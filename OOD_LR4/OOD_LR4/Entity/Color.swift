//
//  Color.swift
//  OOD_LR4
//
//  Created by Marty on 02/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

enum Color {
    case green
    case red
    case blue
    case yellow
    case pink
    case black
    
    static let defaultColor: Color = .black
    
    var cgColor: CGColor {
        switch self {
        case .green : return UIColor.green  .cgColor
        case .red   : return UIColor.red    .cgColor
        case .blue  : return UIColor.blue   .cgColor
        case .yellow: return UIColor.yellow .cgColor
        case .pink  : return UIColor.magenta.cgColor
        case .black : return UIColor.black  .cgColor
        }
    }
    
    var toString: String {
        switch self {
        case .green : return "green"
        case .red   : return "red"
        case .blue  : return "blue"
        case .yellow: return "yellow"
        case .pink  : return "pink"
        case .black : return "black"
        }
    }
    
    static func fromRawValue(_ value: String) -> Color? {
        switch value {
        case "green":
            return .green
        case "red":
            return .red
        case "blue":
            return .blue
        case "yellow":
            return .yellow
        case "pink":
            return .pink
        case "black":
            return .black
        default:
            return nil
        }
    }
}
