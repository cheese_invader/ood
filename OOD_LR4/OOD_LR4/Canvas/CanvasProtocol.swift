//
//  CanvasProtocol.swift
//  OOD_LR4
//
//  Created by Marty on 02/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import CoreGraphics

protocol CanvasProtocol {
    func setColor(_ color: Color)
    func drawLine(from: CGPoint, to: CGPoint)
    func drawEllipse(x: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat)
}
