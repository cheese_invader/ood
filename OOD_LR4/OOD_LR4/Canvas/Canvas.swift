//
//  Canvas.swift
//  OOD_LR4
//
//  Created by Marty on 02/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

private let lineWidth: CGFloat = 3

class Canvas: UIView, CanvasProtocol {
    private var color = Color.defaultColor
    
    func setColor(_ color: Color) {
        self.color = color
    }
    
    func drawLine(from: CGPoint, to: CGPoint) {
        let line = CAShapeLayer()
        let linePath = UIBezierPath()
        
        linePath.move(to: from)
        linePath.addLine(to: to)
        line.path = linePath.cgPath
        line.strokeColor = color.cgColor
        line.lineWidth = lineWidth
        line.lineJoin = CAShapeLayerLineJoin.round
        
        self.layer.addSublayer(line)
    }
    
    func drawEllipse(x: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat) {
        let ovalPath = UIBezierPath(ovalIn: CGRect(x: x, y: y, width: width, height: height))
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = ovalPath.cgPath
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineWidth = lineWidth
        
        self.layer.addSublayer(shapeLayer)
    }
}
