//
//  ShapeFactory.swift
//  OOD_LR4
//
//  Created by Marty on 02/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

class ShapeFactory: ShapeFactoryProtocol {    
    func createShape(_ shapeDescription: String) -> ShapeProtocol? {
        let words = shapeDescription.components(separatedBy: .whitespaces)
        
        guard words.count > 2, let shape = Shape.init(rawValue: words[1]), let color = Color.fromRawValue(words[0]) else {
            return nil
        }
        
        var descriptionComponents = [CGFloat]()
        
        for component in words[2...] {
            guard let doubleComponent = Double(component) else {
                return nil
            }
            descriptionComponents.append(CGFloat(doubleComponent))
        }
        
        return routeShape(shape, withColor: color, andDescription: descriptionComponents)
    }
    
    private func routeShape(_ shape: Shape, withColor color: Color, andDescription description: [CGFloat]) -> ShapeProtocol? {
        switch shape {
        case .rectangle:
            guard description.count == 4 else {
                return nil
            }
            return Rectangle(leftTop: CGPoint(x: description[0], y: description[1]), rightBottom: CGPoint(x: description[2], y: description[3]), color: color)
        case .triangle:
            guard description.count == 6 else {
                return nil
            }
            return Triangle(vertex1: CGPoint(x: description[0], y: description[1]), vertex2: CGPoint(x: description[2], y: description[3]), vertex3: CGPoint(x: description[4], y: description[5]), color: color)
        case .ellipse:
            guard description.count == 4 else {
                return nil
            }
            return Ellipse(center: CGPoint(x: description[0], y: description[1]), horizontalRadius: description[2], verticalRadius: description[3], color: color)
        case .regularPolygon:
            guard description.count == 4 else {
                return nil
            }
            return RegularPolygon(vertexCount: Int(description[0]), center: CGPoint(x: description[1], y: description[2]), radius: description[3], color: color)
        }
    }
}
