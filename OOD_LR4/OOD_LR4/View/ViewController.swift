//
//  ViewController.swift
//  OOD_LR4
//
//  Created by Marty on 02/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    private var instructions = [String]()
    private let shapeFactory: ShapeFactoryProtocol = ShapeFactory()
    private var color = Color.defaultColor
    private var draft: PictureDraft?
    
    
    // MARK: - Outlet -
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var canvas: Canvas!
    @IBOutlet var colorButtons: [UIButton]!
    
    
    // MARK: - Action -
    @IBAction func addButtonWasTapped(_ sender: UIButton) {
        guard let textDescrioption = descriptionTextField.text else {
            return
        }
        let instruction = color.toString + " " + textDescrioption
        instructions.append(instruction)
        
        guard let shape = shapeFactory.createShape(instruction) else {
            return
        }
        shape.drawInCanvas(canvas)
    }
    
    @IBAction func saveButtonWasTapped(_ sender: UIButton) {
        saveDraft()
    }
    
    @IBAction func loadButtonWasTapped(_ sender: UIButton) {
        loadDraft()
    }
    
    @IBAction func colorWasSelect(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let self = self else {
                return
            }
            for button in self.colorButtons where button !== sender {
                button.center.y = sender.center.y
                button.isEnabled = true
            }
            sender.isEnabled = false
            sender.center.y -= 20
        }
    
        switch sender.tag {
        case 0:
            color = .green
        case 1:
            color = .red
        case 2:
            color = .blue
        case 3:
            color = .yellow
        case 4:
            color = .pink
        case 5:
            color = .black
        default:
            return
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)

    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    private func saveDraft() {
        let designer = Designer(withFactory: ShapeFactory())
        let draft = designer.createDraftFromInstructions(instructions)

        self.draft = draft
//        do {
//            let data = try NSKeyedArchiver.archivedData(withRootObject: draft, requiringSecureCoding: false)
//            UserDefaults.standard.set(data, forKey: "draft")
//        } catch {
//            print("Couldn't write file")
//        }
    }
    
    private func loadDraft() {
        if let draft = draft {
            let painter = Painter()
            painter.drawPictureFromDraft(draft, inCanvas: canvas)
        }
        
//        if let draft = UserDefaults.standard.object(forKey: "draft") as? Data {
//            do {
//                if let decodedData = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(draft) as? PictuerDraft {
//                    let painter = Painter()
//                    painter.drawPictureFromDraft(decodedData, inCanvas: canvas)
//                }
//            } catch {
//                print("Couldn't read file.")
//            }
//        }
    }
    
}

