//
//  RegularPolygon.swift
//  OOD_LR4
//
//  Created by Marty on 02/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

class RegularPolygon: ShapeProtocol {
    public private(set) var vertexCount: Int
    public private(set) var center: CGPoint
    public private(set) var radius: CGFloat
    
    
    // MARK: - init -
    init(vertexCount: Int, center: CGPoint, radius: CGFloat, color: Color = Color.defaultColor) {
        self.vertexCount = vertexCount > 2 ? vertexCount : 3
        self.center      = center
        self.radius      = radius
        self.color       = color
    }
    
    
    // MARK: - ShapeProtocol -
    var color: Color
    
    func drawInCanvas(_ canvas: CanvasProtocol) {
        canvas.setColor(color)
        var oldPoint = getPointByVertexNumber(0)
        
        for i in 1...vertexCount {
            let nextPoint = getPointByVertexNumber(i)
            canvas.drawLine(from: oldPoint, to: nextPoint)
            oldPoint = nextPoint
        }
    }
}


// Helpers
extension RegularPolygon {
    private func getPointByVertexNumber(_ vertexNumber: Int) -> CGPoint {
        let angle = 2 * CGFloat.pi * CGFloat(vertexNumber) / CGFloat(vertexCount)
        
        var point = CGPoint()
        point.x = radius * cos(angle) + center.x
        point.y = radius * sin(angle) + center.y
        
        return point
    }
}
