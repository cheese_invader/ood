//
//  Ellipse.swift
//  OOD_LR4
//
//  Created by Marty on 02/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

class Ellipse: ShapeProtocol {
    public private(set) var center          : CGPoint
    public private(set) var horizontalRadius: CGFloat
    public private(set) var   verticalRadius: CGFloat
    
    
    // MARK: - init -
    init(center: CGPoint, horizontalRadius: CGFloat, verticalRadius: CGFloat, color: Color = Color.defaultColor) {
        self.center           = center
        self.horizontalRadius = horizontalRadius
        self  .verticalRadius = verticalRadius
        self.color            = color
    }
    
    
    // MARK: - ShapeProtocol -
    var color: Color
    
    func drawInCanvas(_ canvas: CanvasProtocol) {
        canvas.setColor(color)
        canvas.drawEllipse(x: center.x - horizontalRadius,
                           y: center.y -   verticalRadius,
                           width : horizontalRadius * 2,
                           height:   verticalRadius * 2)
    }
}
