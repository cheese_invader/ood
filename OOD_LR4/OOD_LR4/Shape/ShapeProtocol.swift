//
//  ShapeProtocol.swift
//  OOD_LR4
//
//  Created by Marty on 02/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol ShapeProtocol {
    func drawInCanvas(_ canvas: CanvasProtocol)
    var color: Color { get }
}
