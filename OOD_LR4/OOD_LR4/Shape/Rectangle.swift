//
//  Rectangle.swift
//  OOD_LR4
//
//  Created by Marty on 02/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

class Rectangle: ShapeProtocol {
    public private(set) var leftTop    : CGPoint
    public private(set) var rightBottom: CGPoint
    
    
    // MARK: - init -
    init(leftTop: CGPoint, rightBottom: CGPoint, color: Color = Color.defaultColor) {
        self.leftTop     = leftTop
        self.rightBottom = rightBottom
        self.color       = color
    }
    
    
    // MARK: - ShapeProtocol -
    var color: Color
    
    func drawInCanvas(_ canvas: CanvasProtocol) {
        canvas.setColor(color)
        let rightTop   = CGPoint(x: rightBottom.x, y: leftTop.y)
        let leftBottom = CGPoint(x: leftTop.x, y: rightBottom.y)
        
        canvas.drawLine(from: leftTop, to: rightTop)
        canvas.drawLine(from: rightTop, to: rightBottom)
        canvas.drawLine(from: rightBottom, to: leftBottom)
        canvas.drawLine(from: leftBottom, to: leftTop)
    }
}
