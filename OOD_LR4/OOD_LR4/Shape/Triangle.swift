//
//  Triangle.swift
//  OOD_LR4
//
//  Created by Marty on 02/03/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

class Triangle: ShapeProtocol {
    public private(set) var vertex1: CGPoint
    public private(set) var vertex2: CGPoint
    public private(set) var vertex3: CGPoint
    
    
    // MARK: - init -
    init(vertex1: CGPoint, vertex2: CGPoint, vertex3: CGPoint, color: Color = Color.defaultColor) {
        self.vertex1 = vertex1
        self.vertex2 = vertex2
        self.vertex3 = vertex3
        self.color   = color
    }
    
    
    // MARK: - ShapeProtocol -
    var color: Color
    
    func drawInCanvas(_ canvas: CanvasProtocol) {
        canvas.setColor(color)
        canvas.drawLine(from: vertex1, to: vertex2)
        canvas.drawLine(from: vertex2, to: vertex3)
        canvas.drawLine(from: vertex3, to: vertex1)
    }
}
