//
//  OOD_LR8Tests.swift
//  OOD_LR8Tests
//
//  Created by Marty on 22/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import XCTest
@testable import OOD_LR8

private class MockStateContext: StateContextProtocol {
    init(logger: LoggerProtocol) {
        self.ballsCount = 0
        self.logger = logger
        self.state = SoldOutState(gumballMachine: self, logger: logger)
    }
    
    var ballsCount: Int
    
    var state: StateProtocol!
    
    let logger: LoggerProtocol
    
    func releaseBall() {
        logger.addLog("Releasing ball")
    }
    
    func setSoldOutState() {
        logger.addLog("-> Sold Out")
    }
    
    func setNoQuarterState() {
        logger.addLog("-> No Quarter")
    }
    
    func setSoldState() {
        logger.addLog("-> Sold")
    }
    
    func setHasQuarterState() {
        logger.addLog("-> Has Quarter")
    }
}


private class MockGumballMachine {    
    var ballsCount: Int {
        get {
            return context.ballsCount
        }
    }
    
    private let context: MockStateContext
    
    init(context: MockStateContext) {
        self.context = context
    }
    
    func insertQuarter() {
        context.state.insertQuarter()
    }
    
    func ejectQuarter() {
        context.state.ejectQuarter()
    }
    
    func turnCrank() {
        context.state.turnCrank()
        context.state.dispense()
    }
    
    func toString() -> String {
        return """
        GumballMachine, Inc.
        Copyright © 2019 Marty. All rights reserved.
        If you have any problems see you in court
        
        Inventory: \(context.ballsCount != 0 ? String(context.ballsCount) : "no") gumball\(context.ballsCount != 1 ? "s" : "")
        Machine is \(context.state.toString())
        """
    }
}


class OOD_LR8Tests: XCTestCase {
    // MARK: - Sold out state -
    
    func testSoldOutState_UserCantInsertQuarter() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        let machine = MockGumballMachine(context: context)
        
        machine.insertQuarter()
        XCTAssertEqual(logger.getLastLog()!, "You can't insert a quarter, the machine is sold out\n")
    }
    
    func testSoldOutState_MachineDoesntTransitInNewStateWhileInsertingQuarter() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        let machine = MockGumballMachine(context: context)
        
        machine.insertQuarter()
        XCTAssertEqual(logger.logsCount, 1)
    }
    
    func testSoldOutState_UserCAntEjectQuarter() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        let machine = MockGumballMachine(context: context)
        
        machine.ejectQuarter()
        XCTAssertEqual(logger.getLastLog(), "You can't eject, you haven't inserted a quarter yet\n")
    }
    
    func testSoldOutState_MachineDoesntTransferInNewStateWhileEjectionQuarter() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        let machine = MockGumballMachine(context: context)
        
        machine.ejectQuarter()
        XCTAssertEqual(logger.getLastLog(), "You can't eject, you haven't inserted a quarter yet\n")
        XCTAssertEqual(logger.logsCount, 1)
    }
    
    func testSoldOutState_TurningCrankDoesntBringAnyChanges() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        let machine = MockGumballMachine(context: context)
        
        machine.turnCrank()
        XCTAssertEqual(logger.getLogAtIndex(0), "You turned but there's no gumballs\n")
        XCTAssertEqual(logger.getLastLog(), "No gumball dispensed\n")
    }

    func testSoldOutState_TurningCrankDoesntTransferInNewState() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        let machine = MockGumballMachine(context: context)
        
        machine.turnCrank()
        XCTAssertEqual(logger.getLogAtIndex(0), "You turned but there's no gumballs\n")
        XCTAssertEqual(logger.getLastLog(), "No gumball dispensed\n")
    }
    
    
    // MARK: - No quarter state -
    
    func testNoQuarterState_UserCanInsertQuarter() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        context.ballsCount = 3
        
        let machine = MockGumballMachine(context: context)
        context.state = NoQuarterState(gumballMachine: context, logger: logger)
        
        machine.insertQuarter()
        
        XCTAssertEqual(logger.getLastLog(), "You inserted a quarter\n")
    }
    
    func testNoQuarterState_MachineGetsHasQuarterStateAfterInsertionQuarter() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        context.ballsCount = 1
        context.state = NoQuarterState(gumballMachine: context, logger: logger)
        
        let machine = MockGumballMachine(context: context)
        
        machine.insertQuarter()

        
        XCTAssertEqual(logger.getLogAtIndex(0), "-> Has Quarter")
    }
    
    func testNoQuarterState_UserCantGetGumballTurningCrank() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        
        let machine = MockGumballMachine(context: context)
        context.ballsCount = 3
        context.state = NoQuarterState(gumballMachine: context, logger: logger)
        
        machine.turnCrank()
        
        XCTAssertEqual(logger.getLastLog(), "You need to pay first\n")
    }
    
    func testNoQuarterState_UserCantEjectQuarter() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        
        let machine = MockGumballMachine(context: context)
        context.ballsCount = 3
        context.state = NoQuarterState(gumballMachine: context, logger: logger)
        
        machine.ejectQuarter()
        XCTAssertEqual(logger.getLastLog(), "You haven't inserted a quarter\n")
    }
    
    func testNoQuarterState_TurninkCrankDoesntBringAnyChanges() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        
        let machine = MockGumballMachine(context: context)
        context.ballsCount = 3
        context.state = NoQuarterState(gumballMachine: context, logger: logger)
        
        machine.turnCrank()
        XCTAssertEqual(logger.getLogAtIndex(0), "You turned but there's no quarter\n")
    }
    
    
    // MARK: - Has quarter state -
    
    func testHasQuarterState_UserCantInsertOneMoreQuarter() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        context.ballsCount = 3
        
        let machine = MockGumballMachine(context: context)
        context.state = HasQuarterState(gumballMachine: context, logger: logger)
        
        machine.insertQuarter()
        
        XCTAssertEqual(logger.getLastLog(), "You can't insert another quarter\n")
    }
    
    func testHasQuarterState_UserCanEjectQuarter() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        context.ballsCount = 3
        
        let machine = MockGumballMachine(context: context)
        context.state = HasQuarterState(gumballMachine: context, logger: logger)
        
        machine.ejectQuarter()
        
        XCTAssertEqual(logger.getLastLog(), "Quarter returned\n")
    }
    
    func testHasQuarterState_TurningCrankBringSoldState() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        context.ballsCount = 3
        
        let machine = MockGumballMachine(context: context)
        context.state = HasQuarterState(gumballMachine: context, logger: logger)
        
        machine.turnCrank()
        
        XCTAssertEqual(logger.getLogAtIndex(0), "-> Sold")
    }
    
    
    // MARK: - Sold state -
    
    func testSoldState_UserCantInsertQuarter() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        context.ballsCount = 3
        
        let machine = MockGumballMachine(context: context)
        context.state = SoldState(gumballMachine: context, logger: logger)
        
        machine.insertQuarter()
        
        XCTAssertEqual(logger.getLastLog(), "Please wait, we're already giving you a gumball\n")
    }
    
    func testSoldState_UserCantEjectQuarter() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        context.ballsCount = 3
        
        let machine = MockGumballMachine(context: context)
        context.state = SoldState(gumballMachine: context, logger: logger)
        
        machine.ejectQuarter()
        
        XCTAssertEqual(logger.getLastLog(), "Sorry you already turned the crank\n")
    }
    
    func testSoldState_TurningCrankDoesntBringAnyChanges() {
        let logger = Logger()
        let context = MockStateContext(logger: logger)
        context.ballsCount = 3
        
        let machine = MockGumballMachine(context: context)
        context.state = SoldState(gumballMachine: context, logger: logger)
        
        machine.turnCrank()
        
        XCTAssertEqual(logger.getLogAtIndex(0), "Turning twice doesn't get you another gumball\n")
    }
    
    
    // MARK: - Global machine testing -
    
    func testMachine_CantInsertQuarterInEmptyMachine() {
        let logger = Logger()
        let machine = GumballMachine(ballsCount: 0, logger: logger)
        
        machine.insertQuarter()
        
        XCTAssertEqual(logger.getLastLog(), "You can't insert a quarter, the machine is sold out\n")
    }
    
    func testMachine_CantEjectQuarterInEmptyMachine() {
        let logger = Logger()
        let machine = GumballMachine(ballsCount: 0, logger: logger)
        
        machine.ejectQuarter()
        
        XCTAssertEqual(logger.getLastLog(), "You can't eject, you haven't inserted a quarter yet\n")
    }
    
    func testMachine_CantTurnCrankInEmptyMachine() {
        let logger = Logger()
        let machine = GumballMachine(ballsCount: 0, logger: logger)
        
        machine.turnCrank()
        
        XCTAssertEqual(logger.getLogAtIndex(0), "You turned but there\'s no gumballs\n")
        XCTAssertEqual(logger.getLastLog(), "No gumball dispensed\n")
    }
    
    func testMachine_CanInsertQuarterInMachine() {
        let logger = Logger()
        let machine = GumballMachine(ballsCount: 1, logger: logger)
        
        machine.insertQuarter()
        
        XCTAssertEqual(logger.getLastLog(), "You inserted a quarter\n")
    }
    
    func testMachine_CantEjectQuarterWithoutInsertion() {
        let logger = Logger()
        let machine = GumballMachine(ballsCount: 1, logger: logger)
        
        machine.ejectQuarter()
        
        XCTAssertEqual(logger.getLastLog(), "You haven't inserted a quarter\n")
    }
    
    func testMachine_CantTurnCrankWithoutQuarter() {
        let logger = Logger()
        let machine = GumballMachine(ballsCount: 1, logger: logger)
        
        machine.turnCrank()
        
        XCTAssertEqual(logger.getLogAtIndex(0), "You turned but there's no quarter\n")
        XCTAssertEqual(logger.getLastLog(), "You need to pay first\n")
    }
    
    func testMachine_CanTurnCrankWithQuarter() {
        let logger = Logger()
        let machine = GumballMachine(ballsCount: 1, logger: logger)
        
        machine.insertQuarter()
        machine.turnCrank()
        
        XCTAssertEqual(logger.getLogAtIndex(1), "You turned...\n")
        XCTAssertEqual(logger.getLastLog(), "Get your gumball\n")
    }
    
    func testMachine_UserCanBuyGumballsUntilMachineIsNotEmpty() {
        let logger = Logger()
        let machine = GumballMachine(ballsCount: 1, logger: logger)
        
        machine.insertQuarter()
        machine.turnCrank()
        machine.insertQuarter()
        
        XCTAssertEqual(logger.getLogAtIndex(0), "You inserted a quarter\n")
        XCTAssertEqual(logger.getLogAtIndex(1), "You turned...\n")
        XCTAssertEqual(logger.getLogAtIndex(2), "Get your gumball\n")
        XCTAssertEqual(logger.getLogAtIndex(3), "You can't insert a quarter, the machine is sold out\n")
    }
}
