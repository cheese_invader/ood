//
//  SoldState.swift
//  OOD_LR8
//
//  Created by Marty on 18/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class SoldState: StateProtocol {
    init(gumballMachine: StateContextProtocol, logger: LoggerProtocol?) {
        self.gumballMachine = gumballMachine
        self.logger = logger
    }
    
    func insertQuarter() {
        let response = "Please wait, we're already giving you a gumball\n"
        logger?.addLog(response)
        print(response)
    }
    
    func ejectQuarter() {
        let response = "Sorry you already turned the crank\n"
        logger?.addLog(response)
        print(response)
    }
    
    func turnCrank() {
        let response = "Turning twice doesn't get you another gumball\n"
        logger?.addLog(response)
        print(response)
    }
    
    func dispense() {
        gumballMachine.releaseBall()
        if gumballMachine.ballsCount == 0 {
            gumballMachine.setSoldOutState()
        } else {
            gumballMachine.setNoQuarterState()
        }
        let response = "Get your gumball\n"
        logger?.addLog(response)
        print(response)
    }
    
    func toString() -> String {
        return "Delivering a gumball\n"
    }
    
    
    private var gumballMachine: StateContextProtocol
    private var logger: LoggerProtocol?
}
