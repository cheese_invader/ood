//
//  NoQuarterState.swift
//  OOD_LR8
//
//  Created by Marty on 18/04/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class NoQuarterState: StateProtocol {
    init(gumballMachine: StateContextProtocol, logger: LoggerProtocol?) {
        self.gumballMachine = gumballMachine
        self.logger = logger
    }
    
    func insertQuarter() {
        gumballMachine.setHasQuarterState()
        let response = "You inserted a quarter\n"
        logger?.addLog(response)
        print(response)
    }
    
    func ejectQuarter() {
        let response = "You haven't inserted a quarter\n"
        logger?.addLog(response)
        print(response)
    }
    
    func turnCrank() {
        let response = "You turned but there's no quarter\n"
        logger?.addLog(response)
        print(response)
    }
    
    func dispense() {
        let response = "You need to pay first\n"
        logger?.addLog(response)
        print(response)
    }
    
    func toString() -> String {
        return "Waiting for quarter\n"
    }
    
    
    private var gumballMachine: StateContextProtocol
    private var logger: LoggerProtocol?
}
