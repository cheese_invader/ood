//
//  NavigationViewControllerProtocol.swift
//  OOD_LR9
//
//  Created by Marty on 10/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//


import UIKit


protocol NavigationViewControllerProtocol: class {
    func push(_ viewController: UIViewController, animated: Bool)
}
