//
//  NavigationViewController.swift
//  OOD_LR9
//
//  Created by Marty on 10/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

class NavigationViewController: UINavigationController, NavigationViewControllerProtocol {

    override func viewDidLoad() {
        super.viewDidLoad()

        isNavigationBarHidden = true
        EditorConfigurator().configure(embeddedIn: self, animated: false)
    }
    
    // MARK: - NavigationViewControllerProtocol -
    func push(_ viewController: UIViewController, animated: Bool) {
        self.pushViewController(viewController, animated: animated)
    }
}
