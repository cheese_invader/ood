//
//  RedactorInteractor.swift
//  OOD_LR9
//
//  Created by Marty on 10/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

private let minimumShapeSize: CGFloat = 150

class EditorInteractor: EditorInteractorProtocol {
    // MARK: - VIPER -
    private weak var presenter: EditorPresenterForInteractorProtocol?
    
    init(presenter: EditorPresenterForInteractorProtocol) {
        self.presenter = presenter
    }
    
    
    private var currentTag = 1
    private var shapes = [Int: ShapeProtocol]()
    
    
    
    // MARK: - RedactorInteractorProtocol -
    func addNewTriangle() {
        guard let triangle = addNewShapeWithNextTag() else {
            return
        }
        presenter?.shapeWasAddedByInteractor(shape: triangle, ofType: .triangle)
    }
    
    func addNewRectangle() {
        guard let rectangle = addNewShapeWithNextTag() else {
            return
        }
        presenter?.shapeWasAddedByInteractor(shape: rectangle, ofType: .rectangle)
    }
    
    func addNewEllipse() {
        guard let ellipse = addNewShapeWithNextTag() else {
            return
        }
        presenter?.shapeWasAddedByInteractor(shape: ellipse, ofType: .ellipse)
    }
    
    func changePosition(_ movement: CGPoint, ofShapeWithTag tag: Int) {
        guard var shape = shapes[tag], let viewSize = presenter?.viewFrame else {
            return
        }
        
        var x = max(shape.frame.minX + movement.x, 0)
        var y = max(shape.frame.minY + movement.y, 0)
        
        if x + shape.frame.width > viewSize.width {
            x = viewSize.width - shape.frame.width
        }
        
        if y + shape.frame.height > viewSize.height {
            y = viewSize.height - shape.frame.height
        }
        
        shape.frame = CGRect(x: x, y: y, width: shape.frame.width, height: shape.frame.height)
    }
    
    func changeScale(_ scale: CGSize, ofShapeWithTag tag: Int) {
        guard var shape = shapes[tag], let viewSize = presenter?.viewFrame else {
            return
        }
        let newSize = CGSize(width: shape.frame.width * scale.width, height: shape.frame.height * scale.height)
        
        var x = max((shape.frame.minX - (newSize.width  - shape.frame.width)  / 2), 0)
        var y = max((shape.frame.minY - (newSize.height - shape.frame.height) / 2), 0)
        var width = newSize.width
        var height = newSize.height
        
        if x + width > viewSize.width {
            width = viewSize.width - x
        }
        if y + height > viewSize.height {
            height = viewSize.height - y
        }
        if width < minimumShapeSize {
            width = minimumShapeSize
        }
        if height < minimumShapeSize {
            height = minimumShapeSize
        }
        
        if x + width > viewSize.width {
            x = viewSize.width - width
        }
        if y + height > viewSize.height {
            y = viewSize.height - height
        }
        
        shape.frame = CGRect(x: x, y: y, width: width, height: height)
    }
    
    func removeShapeWithTag(_ tag: Int) {
        shapes.removeValue(forKey: tag)
    }
    
    func getShapeWithTag(_ tag: Int) -> ShapeProtocol? {
        return shapes[tag]
    }
    
    
    // MARK: - Helpers -
    
    private func addNewShapeWithNextTag() -> ShapeProtocol? {
        guard let shapeFrame = presenter?.shapeStartFrame else {
            return nil
        }
        
        let shape = Shape(tag: currentTag, frame: shapeFrame)
        shapes[currentTag] = shape
        currentTag += 1
        return shape
    }
}
