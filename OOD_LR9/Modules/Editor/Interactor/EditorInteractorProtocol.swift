//
//  RedactorInteractorProtocol.swift
//  OOD_LR9
//
//  Created by Marty on 10/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

protocol EditorInteractorProtocol: class {
    func addNewTriangle()
    func addNewRectangle()
    func addNewEllipse()
    
    func changePosition(_ center: CGPoint, ofShapeWithTag tag: Int)
    func changeScale(_ scale: CGSize, ofShapeWithTag tag: Int)
    func removeShapeWithTag(_ tag: Int)
    func getShapeWithTag(_ tag: Int) -> ShapeProtocol?
}
