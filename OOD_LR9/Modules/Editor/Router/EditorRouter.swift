//
//  RedactorRouter.swift
//  OOD_LR9
//
//  Created by Marty on 10/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

class EditorRouter: EditorRouterProtocol {
    // MARK: - VIPER -
    private weak var presenter: EditorPresenterForRouterProtocol?
    private weak var navigation: NavigationViewControllerProtocol?
    
    init(presenter: EditorPresenterForRouterProtocol, embeddedIn navigationController: NavigationViewControllerProtocol) {
        self.presenter  = presenter
        self.navigation = navigationController
    }
}
