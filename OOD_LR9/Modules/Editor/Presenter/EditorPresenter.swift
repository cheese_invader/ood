//
//  RedactorPresenter.swift
//  OOD_LR9
//
//  Created by Marty on 10/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit


private let shapeInitWidht : CGFloat = 200
private let shapeInitHeight: CGFloat = 200


class EditorPresenter: EditorPresenterForViewProtocol, EditorPresenterForInteractorProtocol, EditorPresenterForRouterProtocol {
    // MARK: - VIPER -
    private weak var view : EditorViewProtocol?
    private var interactor: EditorInteractorProtocol!
    private var router    : EditorRouterProtocol!
    
    func setup(view: EditorViewProtocol, interactor: EditorInteractorProtocol, router: EditorRouterProtocol) {
        self.view       = view
        self.interactor = interactor
        self.router     = router
    }
    
    
    // MARK: - RedactorPresenterProtocol -
    var viewFrame: CGRect {
        get {
            return view?.viewFrame ?? CGRect()
        }
    }
    
    var viewCenter: CGPoint {
        get {
            return view?.viewCenter ?? CGPoint()
        }
    }
    
    var shapeStartFrame: CGRect {
        get {
            return CGRect(x: (viewFrame.width  - shapeInitWidht ) / 2,
                          y: (viewFrame.height - shapeInitHeight) / 2,
                          width : shapeInitWidht,
                          height: shapeInitHeight)
        }
    }
    
    
    // -> View
    func addRectangleButtonWasTapped() {
        interactor?.addNewRectangle()
    }
    
    func addTriangleButtonWasTapped() {
        interactor?.addNewTriangle()
    }
    
    func addEllipseButtonWasTapped() {
        interactor?.addNewEllipse()
    }
    
    func deleteItemButtonWasTapped() {
        guard let lastSelectedShapeTag = view?.lastSelectedTag else {
            return
        }
        interactor?.removeShapeWithTag(lastSelectedShapeTag)
        view?.removeShapeWithTag(lastSelectedShapeTag)
    }
    
    
    func shapeWasMoved(tag: Int, movement: CGPoint) {
        interactor?.changePosition(movement, ofShapeWithTag: tag)
        updateFrameForShapeWithTag(tag)
    }
    
    func shapeWasScaled(tag: Int, scale: CGSize) {
        interactor?.changeScale(scale, ofShapeWithTag: tag)
        updateFrameForShapeWithTag(tag)
    }
    
    
    // -> Interactor
    func shapeWasAddedByInteractor(shape: ShapeProtocol, ofType shapeType: ShapeType) {
        guard let view = view?.createShapeOfType(shapeType) else {
            return
        }
        view.frame = shape.frame
        view.tag = shape.tag
        view.backgroundColor = .clear
        
        self.view?.drawShape(view)
    }
    
    
    private func updateFrameForShapeWithTag(_ tag: Int) {
        guard let newFrame = interactor.getShapeWithTag(tag) else {
            return
        }
        view?.changeFrameTo(newFrame.frame, forShapeWithTag: tag)
    }
}
