//
//  EditorPresenterForViewProtocol.swift
//  OOD_LR9
//
//  Created by Marty on 27/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import CoreGraphics

protocol EditorPresenterForViewProtocol: class {
    func addRectangleButtonWasTapped()
    func  addTriangleButtonWasTapped()
    func   addEllipseButtonWasTapped()
    func   deleteItemButtonWasTapped()
    
    func shapeWasMoved (tag: Int, movement: CGPoint)
    func shapeWasScaled(tag: Int, scale   : CGSize )
}
