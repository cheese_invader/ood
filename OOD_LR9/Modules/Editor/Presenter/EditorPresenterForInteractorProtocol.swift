//
//  EditorPresenterForInteractorProtocol.swift
//  OOD_LR9
//
//  Created by Marty on 27/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import CoreGraphics

protocol EditorPresenterForInteractorProtocol: class {
    var viewFrame      : CGRect  { get }
    var viewCenter     : CGPoint { get }
    var shapeStartFrame: CGRect  { get }
    
    func shapeWasAddedByInteractor(shape: ShapeProtocol, ofType shapeType: ShapeType)
}
