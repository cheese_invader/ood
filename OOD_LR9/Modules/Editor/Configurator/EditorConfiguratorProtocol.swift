//
//  EditorConfiguratorProtocol.swift
//  OOD_LR9
//
//  Created by Marty on 26/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

protocol EditorConfiguratorProtocol: class {
    func configure(embeddedIn navigationController: NavigationViewControllerProtocol, animated: Bool)
}
