//
//  EditorConfigurator.swift
//  OOD_LR9
//
//  Created by Marty on 26/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit


private let storyboardName = "Editor"
private let storyboardID   = "EditorStoryboard"


class EditorConfigurator: EditorConfiguratorProtocol {
    
    func configure(embeddedIn navigationController: NavigationViewControllerProtocol, animated: Bool) {
        let editorStoryboard = UIStoryboard(name: storyboardName, bundle: nil)
        
        let editorVC = editorStoryboard.instantiateViewController(withIdentifier: storyboardID) as! EditorViewController
        
        let presenter  = EditorPresenter()
        let interactor = EditorInteractor(presenter: presenter)
        let router     = EditorRouter(presenter: presenter, embeddedIn: navigationController)
        
        presenter.setup(view: editorVC, interactor: interactor, router: router)
        editorVC.setupWithPresenter(presenter)
        
        navigationController.push(editorVC, animated: animated)
    }
    
}
