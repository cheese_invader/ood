//
//  ShapeProtocol.swift
//  OOD_LR9
//
//  Created by Marty on 13/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

protocol ShapeProtocol {
    var tag: Int { get set }
    var frame: CGRect { get set }
}
