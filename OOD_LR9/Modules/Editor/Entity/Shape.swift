//
//  Shape.swift
//  OOD_LR9
//
//  Created by Marty on 26/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//


import CoreGraphics


class Shape: ShapeProtocol {
    init(tag: Int, frame: CGRect) {
        self.tag = tag
        self.frame = frame
    }
    
    var tag: Int
    var frame: CGRect
}
