//
//  EllipseView.swift
//  OOD_LR9
//
//  Created by Marty on 13/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

class EllipseView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        
        context.addEllipse(in: rect)
        
        context.setFillColor(UIColor.random())
        context.fillPath()
    }
}
