//
//  RedactorViewController.swift
//  OOD_LR9
//
//  Created by Marty on 10/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

class EditorViewController: UIViewController, UITabBarDelegate, EditorViewProtocol {
    // MARK: - VIPER -
    private var presenter: EditorPresenterForViewProtocol!
    
    func setupWithPresenter(_ presenter: EditorPresenterForViewProtocol) {
        self.presenter = presenter
    }
    
    
    private var shapeByTag = [Int: UIView]()
    
    private var dragShapeTag: Int?
    private var dragStartLocation: CGPoint?
    
    private var pinchShapeTag: Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        interactionTabBar.delegate = self
    }
    
    @objc func draggedView(_ sender: UIPanGestureRecognizer) {
        let location = sender.location(in: self.view)
        
        switch sender.state {
        case .began:
            guard let view = sender.view, view.tag != 0 else {
                return
            }
            dragShapeTag = view.tag
            dragStartLocation = location
        case .changed, .ended:
            moveShape(location)
        default:
            break
        }
    }
    
    @objc func pinchedView(_ sender: UIPinchGestureRecognizer) {
        switch sender.state {
        case .began:
            guard let view = sender.view, view.tag != 0 else {
                return
            }
            pinchShapeTag = view.tag
        case .changed, .ended:
            guard let tag = pinchShapeTag, let scale = sender.scaleByDirection() else {
                return
            }
            presenter?.shapeWasScaled(tag: tag, scale: scale)
        default:
            break
        }
    }
    
    private func moveShape(_ currentLocation: CGPoint) {
        guard let tag = dragShapeTag, let startPosition = dragStartLocation else {
            return
        }
        let movement = CGPoint(x: currentLocation.x - startPosition.x, y: currentLocation.y - startPosition.y)
        self.dragStartLocation = currentLocation
        presenter?.shapeWasMoved(tag: tag, movement: movement)
    }
    
    // MARK: - UITabBarDelegate -
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        UIView.animate(withDuration: 0.45) { [weak self] in
            guard let self = self else {
                return
            }
            self.interactionTabBar.selectedItem = nil
            switch item.tag {
            case 1:
                self.presenter?.addRectangleButtonWasTapped()
            case 2:
                self.presenter?.addTriangleButtonWasTapped()
            case 3:
                self.presenter?.addEllipseButtonWasTapped()
            case 4:
                self.presenter?.deleteItemButtonWasTapped()
            default:
                break
            }
        }
    }
    
    // MARK: - Outlet -
    @IBOutlet weak var interactionTabBar: UITabBar!
    
    
    // MARK: - RedactorViewControllerProtocol -
    var isDeleteItemButtonEnabled: Bool {
        get {
            return interactionTabBar.items?.last?.isEnabled ?? false
        }
        
        set {
            interactionTabBar.items?.last?.isEnabled = newValue
        }
    }
    
    var viewFrame: CGRect {
        get {
            return view.frame
        }
    }
    
    var viewCenter: CGPoint {
        get {
            return view.center
        }
    }
    
    var lastSelectedTag: Int? {
        get {
            return dragShapeTag
        }
    }
    
    func drawShape(_ shape: UIView) {
        shapeByTag[shape.tag] = shape
        
        let pan   = UIPanGestureRecognizer  (target: self, action: #selector(self.draggedView(_:)))
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchedView(_:)))
        view.addSubview(shape)
        shape.addGestureRecognizer(pan)
        shape.addGestureRecognizer(pinch)
    }
    
    func createShapeOfType(_ type: ShapeType) -> UIView {
        switch type {
        case .rectangle:
            return RectangleView(frame: CGRect())
        case .triangle:
            return TriangleView(frame: CGRect())
        case .ellipse:
            return EllipseView(frame: CGRect())
        }
    }
    
    
    
    func changeFrameTo(_ frame: CGRect, forShapeWithTag tag: Int) {
        guard let shape = shapeByTag[tag] else {
            return
        }
        shape.frame = frame
    }
    
    func removeShapeWithTag(_ tag: Int) {
        guard let shape = shapeByTag.removeValue(forKey: tag) else {
            return
        }
        shape.removeFromSuperview()
    }
}
