//
//  RedactorViewControllerProtocol.swift
//  OOD_LR9
//
//  Created by Marty on 10/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

protocol EditorViewProtocol: class {
    var isDeleteItemButtonEnabled: Bool    { get set }
    var viewFrame                : CGRect  { get }
    var viewCenter               : CGPoint { get }
    var lastSelectedTag          : Int?    { get }
    func drawShape(_ shape: UIView)
    func changeFrameTo(_ frame: CGRect, forShapeWithTag tag: Int)
    func removeShapeWithTag(_ tag: Int)
    func createShapeOfType(_ type: ShapeType) -> UIView
}
