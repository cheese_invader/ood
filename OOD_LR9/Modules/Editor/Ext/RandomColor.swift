//
//  RandomColor.swift
//  OOD_LR9
//
//  Created by Marty on 13/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static func random() -> CGColor {
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0).cgColor
    }
}
