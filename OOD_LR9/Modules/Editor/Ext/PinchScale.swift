//
//  PinchScale.swift
//  OOD_LR9
//
//  Created by Marty on 13/05/2019.
//  Copyright © 2019 Marty. All rights reserved.
//

import UIKit

extension UIPinchGestureRecognizer {
    func scaleByDirection() -> CGSize? {
        guard let view = view else {
            return nil
        }
        if numberOfTouches > 1 {
            let touch1 = self.location(ofTouch: 0, in: view)
            let touch2 = self.location(ofTouch: 1, in: view)
            let deltaX = abs(touch1.x - touch2.x)
            let deltaY = abs(touch1.y - touch2.y)
            let sum = deltaX + deltaY
            if sum > 0 {
                let scale = self.scale
                return CGSize(width: 1.0 + (scale - 1.0) * (deltaX / sum), height: 1.0 + (scale - 1.0) * (deltaY / sum))
            }
        }
        return nil
    }
}
